$(function(){
  var anchorsArray = ['Manifesto', 'About Us', 'Work','Testimonials', 'Expertise','Clients', 'Brands', 'Services', 'Contact'];
  $('#fullpage').fullpage({
    sectionsColor: ['#ffffff', '#000000', ' ', '#000000', '#000000', '#000000', '#000', '#000000', ''],
    anchors: anchorsArray,
    scrollingSpeed: 1000,
    scrollBar:true,
    navigation: true,
    afterRender: function(){
      $('.video').get(0).play();
      for (var i = 0; i < anchorsArray.length; i++) {
        $('.nav_aside ul').append( '<li><a href="#' + anchorsArray[i] + '"><span>' + anchorsArray[i] +'</span></a></li>');
      };

    },
    onLeave: function(index, nextIndex, direction){
      $('.video').get(0).play();
      $('.nav_aside ul').find('li').removeClass('active');
      $('.nav_aside ul').find('li').eq((nextIndex - 1)).addClass('active');

      if ( nextIndex == 2) {
        setTimeout(function(){
          $('.about-us').addClass('init');
        }, 1000);
      };
      if ( nextIndex == 4) {
        setTimeout(function(){
          $('.proven-experience').addClass('init');
        }, 1000);
      };
      if ( nextIndex == 4) {
       //  $('video').get(0).play();
      };
      if ( nextIndex == 6) {
        setTimeout(function(){
          $('.our-brands').addClass('init');
        }, 1000);
      };
      if ( nextIndex == 7) {
        setTimeout(function(){
          $('.rich-media-experts').addClass('init');
        }, 1000);
      };
    },
  });
  $('.site__nav li:first').addClass('active');


    /*--------------------------
           CAROUSEL HOME
    ---------------------------*/

  function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  var owl = $('.owl-home');
  owl.owlCarousel({
    items: 1,
    number: 1,
    margin: 0,
    loop: true,
    nav: false,
    autoplayHoverPause: false,
    autoplay: true,
    autoplayTimeout: 11000,
    callbacks: true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    startPosition: getRandomInt(0,3),
    onTranslate: function(e){
      $('.wrap_content img, .wrap_content h1, .wrap_content h2').removeClass('show');
    },
    onTranslated: function(e){
      $('.owl-item.active .wrap_content .logo-rich-media, .owl-item.active .wrap_content .img-responsive, .owl-item.active .wrap_content h1, .owl-item.active .wrap_content h2').addClass('show');
    },
    onInitialized: function(e){
      $('.owl-item.active .wrap_content .logo-rich-media, .owl-item.active .wrap_content .img-responsive, .owl-item.active .wrap_content h1, .owl-item.active .wrap_content h2').addClass('show');
      // setTimeout(function(){
      // }, 100);
    }
  });


  /*--------------------------
      CAROUSEL TESTIMONIAL
  ---------------------------*/

    var owlTestimonial = $('.owl-testiminial');
    owlTestimonial.owlCarousel({
      items: 1,
      number: 1,
      margin: 0,
      loop: false,
      nav: true,
      autoplayHoverPause: false,
      autoplay: false,
      autoplayTimeout: 8000,
      callbacks: true,
      mouseDrag: false,
      // onTranslate: function(e){
      //   $('.wrap_content img, .wrap_content h1, .wrap_content h2').removeClass('show');
      // },
      // onTranslated: function(e){
      //   $('.owl-item.active .wrap_content .logo-rich-media, .owl-item.active .wrap_content .img-responsive, .owl-item.active .wrap_content h1, .owl-item.active .wrap_content h2').addClass('show');
      // },
      // onInitialized: function(){
      //   setTimeout(function(){
      //     $('.owl-item.active .wrap_content .logo-rich-media, .owl-item.active .wrap_content .img-responsive, .owl-item.active .wrap_content h1, .owl-item.active .wrap_content h2').addClass('show');
      //   }, 1000);
      // }
    });

    /*--------------------------
           CAROUSEL NEWS
    ---------------------------*/

    var owlNews = $('.owl-news');
    owlNews.owlCarousel({
      items: 1,
      number: 1,
      margin: 0,
      loop: true,
      nav: true,
      dots: true,
      dragClass: 'owl-drag owl-theme',
      autoplayHoverPause: false,
      autoplay: true,
      autoplayTimeout: 8000,
      callbacks: true,
      animateOut: 'fadeOut',
      animateIn: 'fadeIn',
    });

    var owlText = $('.owl-text');
    owlText.owlCarousel({
      items: 1,
      number: 0,
      margin: 20,
      loop: true,
      nav: true,
      dots: false,
      autoplayTimeout: 5000,
      animateOut: 'fadeOut',
      animateIn: 'fadeIn'
    });

    var owlBg = $('.owl-bg');
    owlBg.owlCarousel({
      items: 1,
      // number: 0,
      margin: 0,
      loop: true,
      nav: false,
      dots: false,
      // rewind: true,
      // dragClass: 'owl-drag owl-theme',
      autoplayHoverPause: false,
      autoplay: true,
      autoplayTimeout: 10000,
      // callbacks: true,
      animateOut: 'fadeOut',
      animateIn: 'fadeIn',
      mouseDrag: false
    });

});
