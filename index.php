<?php
/*
 * STANDARD HOURS
 */
$webroot = "http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/";
$hourURL = $webroot . "php/hour.php";

$datetimeCST  = new DateTime("now", new DateTimeZone('America/Chicago'));
$cstHour = $datetimeCST->format("H");
$cstMinutes = $datetimeCST->format("i");

$datetimePST  = new DateTime("now", new DateTimeZone('America/Los_Angeles'));
$pstHour = $datetimePST->format("H");
$pstMinutes = $datetimePST->format("i");

$datetimeEST  = new DateTime("now", new DateTimeZone('America/New_York'));
$estHour = $datetimeEST->format("H");
$estMinutes = $datetimeEST->format("i");

$datetimeMST  = new DateTime("now", new DateTimeZone('America/Denver'));
$mstHour = $datetimeMST->format("H");
$mstMinutes = $datetimeMST->format("i");

$sent = false;
if($_POST){
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];
    if(filter_var($client, FILTER_VALIDATE_IP)) {
        $ip = $client;
    } elseif(filter_var($forward, FILTER_VALIDATE_IP)) {
        $ip = $forward;
    } else {
        $ip = $remote;
    }

    $data = array(
        'secret' => '6LeKhiAUAAAAAMiWmaRJFmE5Qrd7d4AAcDMtElND',
        'response' => $_POST['g-recaptcha-response'],
        'remoteip' => $ip
    );

    $curlValidate = curl_init("https://www.google.com/recaptcha/api/siteverify");
    curl_setopt($curlValidate, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curlValidate, CURLOPT_POST, true);
    curl_setopt($curlValidate, CURLOPT_POSTFIELDS, $data);
    $responseFromGoogle = curl_exec($curlValidate);
    $responseFromGoogle = json_decode($responseFromGoogle);
    curl_close($curlValidate);

    if($responseFromGoogle->success == 1){
          require 'php/sendgrid/vendor/autoload.php';

          $name = $_POST["name"];
          $email = $_POST["email"];
          $message = $_POST["message"];
          $subject = $name . ' - RICHMEDIAEXPERTS.COM';

          $from = new SendGrid\Email($name, $email);
          $to = new SendGrid\Email("RICH MEDIA EXPERTS", "contact@wamdigital.com");

          $html = "Name: $name <br/>
                   E-mail: $email<br/>
                   Message: $message <br/>";
          $content = new SendGrid\Content("text/html", $html);

          $mail  = new SendGrid\Mail($from, $subject, $to, $content);
          $sg         = new \SendGrid("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");
          $response   = $sg->client->mail()->send()->post($mail);

          if($response->statusCode() == 202){
              $sent = true;
          }
    }
}


?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
      <title>RICH MEDIA EXPERTS | Turn-key OLA HTML5 ads solutions</title>

      <meta name="description" content="We’re online advertising production specialists, serving award-winning agencies in USA and EUROPE. HTML5 banners, Dynamic Banners">
      <meta name="keywords" content="HTML5 banners, Rich media, Online Advertising, outsourcing, digital production, interactive production, integrated production, Transcreation, Nearshore digital production, Banner production, dynamic banners, programmatic ads, online advertising production, expandable banners, mobile banners, polite banners, video banners, retargeting, responsive banners, Dynamic responsive ads, HTML5 production, HTML5 Responsive Rich Media Banner, online ads, translocation, HTML5 Banner production, digital agency, digital production agency, programmatic ad production, Ad serves, doubleclick certified, rich media vendors, display advertising, responsive display advertisement">
      <meta name="author" content="RME">
      <meta name="robots" content="index, follow">
      <meta name="revisit-after" content="1 days">

      <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
      <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
      <link rel="manifest" href="/manifest.json">
      <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
      <meta name="theme-color" content="#ffffff">

      <link href="css/main.css?v=4.8" rel="stylesheet">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
      <link href="https://fonts.googleapis.com/css?family=Exo" rel="stylesheet">

      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
      <script src='https://www.google.com/recaptcha/api.js'></script>
      <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-103696907-1', 'auto');
        ga('send', 'pageview');
      </script>
   </head>
   <body>
      <div class="page-loading">
         <div class="loader">
         </div>
         <span class="text">Loading...</span>
      </div>
      <div id="fullpage">
        <section class="section carousel">
          <ul class="owl-carousel owl-home">
             <li>
              <figure>
                <div class="background" style="background-image: url('img/carousel/slider_2.jpg')"></div>
                <figcaption>
                  <div class="wrap_content">
                    <img src="img/carousel/text-slide2.svg" class="img-responsive">
                    <img src="img/carousel/logo-rich-media-2.svg" class="logo-rich-media">
                    <h1>We can handle the big fish</h1>
                    <h2>
                      We’ve produced global display advertising campaigns in over 14 languages and have over 50,000 banners under our belt.
                    </h2>
                  <div>
                </figcaption>
              </figure>
            </li>
            <li>
              <figure>
                <div class="background" style="background-image: url('img/carousel/slider_1.jpg')"></div>
                <figcaption>
                  <div class="wrap_content">
                    <img src="img/carousel/text-slide1.svg" class="img-responsive">
                    <img src="img/carousel/logo-rich-media-1.svg" class="logo-rich-media">
                    <h1>We are wise beyond our years</h1>
                    <h2>
                      We’ve been on the forefront of the online advertising space for the past 15-years working with clients throughout
                      <span>North America and around the world.</span>
                    </h2>
                  <div>
                </figcaption>
              </figure>
            </li>

            <li>
              <figure>
                <div class="background" style="background-image: url('img/carousel/slider_3.jpg')"></div>
                <figcaption>
                  <div class="wrap_content">
                    <img src="img/carousel/text-slide3.svg" class="img-responsive">
                    <img src="img/carousel/logo-rich-media-3.svg" class="logo-rich-media">
                    <h1>We can butt heads with the big ones</h1>
                    <h2>
                      Some of the hottest award winning agencies in the
                      <span>world trusts us with developing their brands’ online advertising campaigns.</span>
                    </h2>
                  <div>
                </figcaption>
              </figure>
            </li>
            <li>
              <figure>
                <div class="background" style="background-image: url('img/carousel/slider_4.jpg')"></div>
                <figcaption>
                  <div class="wrap_content">
                    <img src="img/carousel/text-slide4.svg" class="img-responsive">
                    <img src="img/carousel/logo-rich-media-4.svg" class="logo-rich-media">
                  <h1>All day and all night if we have to
                  </h1>
                  <h2>
                    We are used to tight deadlines and we are obsessed with delivering on time, every time.
                    </h2>
                  <div>
                </figcaption>
              </figure>
            </li>
          </ul>
        </section>
        <section class="section about-us">
          <div class="content">
            <h3>It’s in our nature.</h3>
            <p>
               Rich Media Experts, is your go-to HTML5 rich media banner production company. We work with both traditional and digital advertising agencies as well as digital production companies within North America and beyond.
            </p>
            <p>
              Our team consists of a combination of highly skilled accredited programmers and quality assurance professionals. Our client service team is un-matched and ensures that your project will be delivered on-time and on-budget.
            </p>
            <p>
              In our 15-years of experience, we’ve seen it all.  It’s this background, attention to detail and always going the extra mile that has gained us some very loyal clients and partners.
            </p>
          </div>
        </section>
        <section class="section news">
            <h2>Our Work</h2>
            <p>We are proud of our craft.</p>
            <div class="works">
               <div class="work">
                  <figcaption class="info_banner">
                     <div class="info">
                        <h3>audi</h3>
                        <p>video banner</p>
                        <a href="http://richmediaexperts.com/portfolio/" class="btn--cta">view more</a>
                     </div>
                  </figcaption>
               </div>
               <div class="work">
                  <figcaption class="info_banner">
                     <div class="info">
                        <h3>nike</h3>
                        <p>image sequence banner</p>
                        <a href="http://richmediaexperts.com/portfolio/" class="btn--cta">view more</a>
                     </div>
                  </figcaption>
               </div>
               <div class="work">
                  <figcaption class="info_banner">
                     <div class="info">
                        <h3>Ea Sports</h3>
                        <p>expandable video banner</p>
                        <a href="http://richmediaexperts.com/portfolio/" class="btn--cta">view more</a>
                     </div>
                  </figcaption>
               </div>
               <div class="work">
                  <figcaption class="info_banner">
                     <div class="info">
                        <h3>EA</h3>
                        <p>html5 banner</p>
                        <a href="http://richmediaexperts.com/portfolio/" class="btn--cta">view more</a>
                     </div>
                  </figcaption>
               </div>
            </div>
            <a href="http://richmediaexperts.com/portfolio/" class="btn--cta">view more</a>
        </section>
        <section class="section testimonials">
           <div class="wrap">
              <div class="top hidden-md-down">
                <div class="overlay"></div>
              </div>
             <div class="bottom">
             <h2 class="break-word">
              Words from our Clients
             </h2>
               <div class="content">
                 <div class="owl-carousel owl-text">
                  <div class="">
                     <p>
                       "When I met with Rich Media Experts, it seemed their attention to creative detail could make a great fit for our pixel perfect art directors, so we did a test project with them. Our team was blown away at the extra mile they took the creative. All the projects we work with them are managed perfectly and effortlessly."
                     </p>
                     <article class="break-word">
                       Eric Kozak
                       <span>Senior Digital Producer at Duncan / Channon</span>
                     </article>
                   </div>

                   <div class="">
                     <p>
                       "In our industry, it is very important that we work with production companies who understand and can deal with the constraints we sometimes find ourselves in—from tight budgets, to last minute edits, to moving deadlines. We know we can trust RME because not only are they very responsive to our needs, they’re just plain great at what they do."
                     </p>
                     <article class="break-word">
                       Julia Glass
                       <span>Account Director at Third Rail Creative</span>
                     </article>
                   </div>

                   <div>
                     <p>
                       "In today's business environment, every serious communication company needs a serious digital development partner. Rich Media Experts is just that, they have understood the importance of high quality crafted work, that is both creative and effective. With their input and standardized processes, we have delivered communication campaigns with outstanding results for our clients."
                     </p>
                     <article class="break-word">
                       Marian Bakit
                       <span>General Manager at IDEAS WCW</span>
                     </article>
                   </div>

                   <div class="">
                     <p>
                       "Working with Rich Media Experts has been great, we found a partner that advises and helps us with our day to day OLA work flow. I completely trust their work and capabilities for every project they do for us.
                     </p>
                     <article class="break-word">
                       Andrea Oreamuno
                       <span>Account Service Director at Orson</span>
                     </article>
                   </div>
                 </div>
               </div>
             </div>

           </div>
        </section>
        <section class="section proven-experience">
          <div class="content">
            <h2>Full service delivery</h2>
            <p>
              With our certified team and extensive knowledge in multiple banner technologies you just have to chose your favorite Adserver, we will handle the rest.
            </p>
            <P>
              We will deliver your campaign QA’ed and ready to be published.
            </P>
            <div class="content-certification">
              <div class="content-logos">
                <img src="img/doubleclick_qa.svg">
              </div>
              <div class="content-logos">
                <img src="img/studio_certified.svg">
              </div>
              <div class="content-logos">
                <img src="img/html5.svg">
              </div>
            </div>
            <p>
              We are certified in DoubleClick Studio platform, HTML5 programming, and Quality Assurance.
            </p>
            <h6>Technology Partners:</h6>
            <div class="technology-partners">
              <div class="partner-logo">
                <img src="img/doubleclick_logo.png">
              </div>
              <div class="partner-logo">
                <img src="img/sizmek_logo.png">
              </div>
              <div class="partner-logo">
                <img src="img/pointroll_logo.png">
              </div>
              <div class="partner-logo">
                <img src="img/flashtalking_logo.png">
              </div>
              <div class="partner-logo">
                <img src="img/atlas_logo.png">
              </div>
              <div class="partner-logo">
                <img src="img/sc_logo.png">
              </div>
            </div>
          </div>
        </section>
        <section class="section map">
          <!-- <video class="video-background" autoplay="autoplay" muted="muted" preload="auto" loop="loop">
            <source src="video/17026099.ogv" type="video/ogg">
            <source src="video/17026099.mp4" type="video/mp4">
          </video> -->
          <div class="video-background">
             <div id="player"></div>
          </div>
          <div class="mask-video"></div>
          <div class="content">
            <h2>Who we work for</h2>
            <div class="content-map">
              <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 119 57.816">
                <defs>
                  <clipPath id="clip-path" transform="translate(295.283 135.142)">
                    <rect class="cls-1" x="-295.283" y="-135.141" width="119.001" height="57.815"/>
                  </clipPath>
                </defs>
                <title>map</title>
                <g>
                  <g class="cls-2">
                    <g class="cls-2">
                      <g>
                        <path class="cls-3" d="M-288.677-127.386l-4.334-1.862,4.486,0.1Zm-4.108-1.813,4.069,1.748,0.143-1.653Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-288.722-127.388l0.153-1.763h0.023l3.69,0.556Zm0.193-1.712-0.143,1.651,3.619-1.127Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-290.629-124.16l-1.029-.3,2.8-.958Zm-0.883-.3,0.875,0.256,1.509-1.071Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-292.974-124.827l-1.514-.74,1.5-.86Zm-1.42-.743,1.375,0.672-0.016-1.453Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-291.529-124.427l-1.49-.415-0.017-1.611Zm-1.446-.449,1.333,0.372-1.349-1.814Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-291.593-124.438l-0.01-.013-1.465-1.97,4.153,1.068Zm-1.365-1.909,1.381,1.857,2.508-.857Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-288.974-125.327l-0.024-.007-4.107-1.056,4.434-1.062Zm-3.948-1.061,3.911,1.006,0.288-2.012Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-292.982-126.346l-2.122-.949,1.812-.363Zm-1.978-.933,1.915,0.856-0.28-1.183Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-293.029-126.358l-0.307-1.3h0.029l4.767,0.22Zm-0.251-1.25,0.284,1.2,4.143-.992Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-288.576-127.39l-4.76-.22,0.423-1.644Zm-4.7-.262,4.464,0.206-4.067-1.747Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-293.3-127.592l-1.321-1.1,1.75-.567Zm-1.231-1.081,1.206,1,0.392-1.52Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-286.409-124.992l-2.61-.339,0.3-2.136Zm-2.56-.377,2.445,0.318-2.159-2.318Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-286.458-124.982l-2.279-2.446,3.821-1.19Zm-2.2-2.425,2.185,2.346,1.478-3.487Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-284.238-124.434l-2.257-.573,1.55-3.655Zm-2.2-.6,2.142,0.543-0.672-4.011Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-282.709-122.762l-3.9-2.319,2.353,0.6Zm-3.615-2.2,3.389,2.015-1.344-1.5Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-280.532-118.248l-1.335-3.285,1.746,1.129v0.015Zm-1.241-3.172,1.226,3.017,0.377-1.98Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-277.858-112.683l-1.437-1.928,1.753,1.052Zm-1.268-1.775,1.255,1.683,0.275-.765Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-276.366-112.171l-1.526-.544,0.315-.877Zm-1.47-.571,1.329,0.474-1.054-1.238Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-275.745-109.51l-2.174-3.262,1.5,0.535Zm-2.064-3.176,1.961,2.942-0.607-2.46Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-272.859-107.267l-1.4-.788-0.053-1.5Zm-1.357-.814,1.226,0.69-1.272-2.008Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.709-105.844l-1.384-1.093L-269.4-108Zm-1.308-1.089,1.275,1.007,0.283-1.983Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266.183-103.2l-1.8-1.34,1.542,0.606Zm-1.508-1.178,1.418,1.057-0.2-.579Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266.235-103.213l-0.251-.717,1.019-.271Zm-0.194-.687,0.209,0.6,0.638-.821Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-267.84-104.427l-0.014-.02-1.06-1.43,1.54,1.02Zm-0.889-1.275,0.9,1.209,0.388-.358Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266.366-103.856l-1.512-.594,0.468-.432Zm-1.43-.609,1.245,0.489-0.86-.845Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-267.4-104.815l-1.477-.979,1.837-.566Zm-1.375-.964,1.346,0.892,0.329-1.409Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.752-105.847l0.308-2.153,1.35,1.333Zm0.339-2.06-0.283,1.983,1.526-.755Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-268.809-105.764h-0.012l-0.978-.107,1.746-.864Zm-0.835-.135,0.815,0.089,0.623-.8Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-268.882-105.745l0.742-.953,1.148,0.37Zm0.758-.9-0.637.819,1.623-.5Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-272.925-107.289l-0.018-.029-1.385-2.185,2.822,0.1Zm-1.321-2.167,1.322,2.086,1.338-1.99Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-271.5-109.357l-2.924-.1,2.258-.683Zm-2.657-.14,2.559,0.092-0.583-.69Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-271.025-106.907l-1.936-.407,0.018-.028,1.408-2.1Zm-1.863-.437,1.806,0.38-0.475-2.361Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-271.067-106.9l-0.308-1.524,1.482,0.414Zm-0.253-1.464,0.281,1.386,1.081-1Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-271.042-106.916l-0.026-.035,1.686-1.554-0.025.565Zm1.611-1.484-1.433,1.32,1.414-.885Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.453-107.9l0.046-1.049,1.12-.082Zm0.088-1.008-0.039.9,1-.969Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-268.109-106.615l-1.355-1.338,1.132-1.1Zm-1.292-1.338,1.238,1.222-0.2-2.225Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266.209-110.517l-0.943-1.886,1.251,1.378Zm-0.76-1.619,0.764,1.528,0.25-.411Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-267.058-112.24l-0.935-.529,1.535-.423Zm-0.82-.515,0.8,0.456,0.517-.821Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.888-110.94l-1.2-1.327,0.594-.943Zm-1.15-1.332,1.06,1.167-0.538-2Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.659-114.059l0.792-2.18,1.687-1.108Zm0.828-2.151-0.686,1.89,2.149-2.85Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.35-117.157l0.5-.987,1.515-.955,0.027,0.035Zm0.536-.956-0.384.753,1.689-1.577Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.351-119.052l0.386-1.509,0.561,1.316Zm0.4-1.372-0.336,1.314,0.825-.168Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.855-118.087l0.136-2.1,1.437,1.112Zm0.175-2.018-0.126,1.934,1.447-.912Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.309-119.043l-1.444-1.117,1.822-.36Zm-1.34-1.092,1.314,1.017,0.344-1.346Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.878-120.059l1.025-2.169,0.631,2.152Zm1.015-2.045-0.945,2,1.528-.016Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.361-120.961l-2.011-.187,1.942-1.806Zm-1.909-.222,1.863,0.174-0.063-1.847Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.409-120.941l-0.068-2,1.657,0.769Zm-0.022-1.929,0.063,1.841,1.464-1.133Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.746-122.09l-1.719-.8h0l-1.2-2Zm-1.688-.832,1.465,0.68-2.492-2.387Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.335-121.122l-0.657-3.3,2.579,1.517Zm-0.6-3.216,0.623,3.13,1.82-1.693Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.036-124.339l1.012-.995,0.5,0.586Zm1.009-.93-0.859.843,1.281-.346Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.386-122.841l-2.631-1.548,1.462-.4,0.009,0.015Zm-2.518-1.533,2.391,1.406-1.063-1.765Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.961-124.351l-1.227-1.124,2.213,0.156Zm-1.1-1.071,1.1,1.01,0.886-.87Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.785-124.245l0.477-1.789,0.021,0.006,2.228,0.574Zm0.508-1.735-0.442,1.658,2.526-1.121Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.488-126.5l2.5-1.2-0.052,1.075Zm2.448-1.124-2.231,1.069,2.184-.108Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.82-126.381l-1.268-.248,0.053-1.09Zm-1.222-.284,1.1,0.215L-267-127.61Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-267.077-127.56l0.5-1.4,1.06-.413Zm0.538-1.368-0.416,1.159,1.292-1.5Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-264.521-126.672l-1.108-2.694,1.892,0.87Zm-1.025-2.607,1.025,2.493,0.726-1.688Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.046-125.405l-2.291-.59,0.816-.764Zm-2.2-.613,2.042,0.526-1.315-1.207Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.993-126.067l-1.557-.651,0.787-1.831Zm-1.5-.674,1.429,0.6-0.706-2.278Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.109-125.4l-1.518-1.394,1.625,0.679Zm-1.3-1.256,1.272,1.167,0.09-.6Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.887-125.269l-2.265-.159,0.107-.708Zm-2.214-.2,1.937,0.136-1.846-.741Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.009-125.269l-2.126-.854,2.178-.087Zm-1.919-.818,1.878,0.754,0.047-.831Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.517-124.666l-0.531-.627V-125.3l0.057-1.016Zm-0.486-.642,0.387,0.457-0.345-1.2Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.025-126.022l-0.77-2.481,1.42-.256Zm-0.713-2.446,0.707,2.279,0.6-2.515Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.056-126.082l0.643-2.709,1.473,2.624Zm0.66-2.589-0.6,2.543,1.987-.08Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.977-126.142l-1.476-2.63,2.46,0.9Zm-1.379-2.548,1.38,2.458,0.919-1.618Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.717-128.439l-1.908-.876,0.962-.98Zm-1.833-.89,1.736,0.8-0.86-1.688Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.779-128.461l-0.007-.014-0.942-1.849,2.381,1.6Zm-0.832-1.731,0.857,1.682,1.292-.234Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266.583-128.911l0.243-2.032,0.782,1.633Zm0.269-1.876-0.216,1.808,0.912-.355Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.594-129.284l-0.776-1.62,1.741,0.638Zm-0.689-1.541,0.7,1.465,0.873-.888Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.566-132.327l2.062-2.216,0.189,1.443Zm2.03-2.117-1.873,2.017,2.05-.7Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.358-133.084l-0.188-1.431,3.886,0.238Zm-0.137-1.384,0.174,1.326,3.425-1.1Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.49-132.342l-1.72-1.939,3.744-.236Zm-1.627-1.9,1.628,1.836,1.915-2.059Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-271.242-133.212l0.139-.378,1.142-.4,0.018,0.04Zm0.173-.343-0.09.246,0.982-.556Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-270.669-125.664l-3.227-2.588,3.16-.521Zm-3.122-2.561,3.076,2.467-0.064-2.964Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-270.709-125.672l-0.069-3.156,1.433,2.3Zm-0.021-3,0.063,2.917,1.261-.788Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-282.765-126.892l0.832-2.307,2.493,0.34Zm0.861-2.258-0.777,2.158,3.109-1.841Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-284.277-124.4l-0.708-4.231,2.292,1.686-0.011.017Zm-0.647-4.131,0.671,4.008,1.5-2.411Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-282.665-126.872l-2.344-1.724,1.4-.258Zm-2.236-1.7,2.12,1.559-0.857-1.793Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-282.72-126.886l-0.937-1.96,1.773-.358Zm-0.873-1.928,0.868,1.814,0.773-2.145Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-280.135-120.36l-1.725-1.117,2.909-1.911Zm-1.644-1.117,1.623,1.05,1.114-2.848Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-281.825-121.447l-1.036-1.426,3.956-.492Zm-0.956-1.391,0.967,1.331,2.726-1.791Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-279.03-123.308l0.013-.032,1.192-2.98,0.023,0.016,3.926,2.745h-0.065Zm1.225-2.944-1.159,2.9,4.957-.241Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-274.017-123.555l3.339-2.192,0.539,2.142Zm3.311-2.121-3.161,2.075,3.672-.048Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-276.9-119.489l-3.274-.889,0.009-.023,1.166-2.982Zm-3.215-.919,3.127,0.85-2-3.721Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-276.948-119.483l-0.017-.03-2.068-3.839H-279l5.1-.249Zm-2.013-3.828,2.018,3.746,2.956-3.988Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-277.566-113.522l-0.014-.009-1.684-1.011,3.974-1.189Zm-1.59-1.006,1.584,0.95,2.122-2.059Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-276.427-112.175l-1.172-1.376,2.276-2.208Zm-1.112-1.373,1.093,1.283,1.03-3.342Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-273.526-111.583l-2.938-.621,1.091-3.542Zm-2.882-.654,2.806,0.593-1.764-3.977Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-275.228-110.483l-1.259-1.77,2.978,0.626Zm-1.158-1.7,1.169,1.643,1.6-1.058Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-274.277-109.437l-0.978-1.08,1.733-1.15Zm-0.911-1.072,0.893,0.987,0.69-2.037Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-274.321-109.446l0.748-2.209,1.444,1.546Zm0.766-2.125-0.7,2.058,2.043-.617Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-272.159-110.076l-1.45-1.551,1.993-.426Zm-1.363-1.524,1.342,1.436,0.5-1.831Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-273.569-111.57l-1.837-4.141,0.04,0.006,4.4,0.645Zm-1.764-4.086,1.774,4,2.515-3.37Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-273.617-111.58l2.656-3.557L-271.625-112h-0.014Zm2.571-3.37-2.465,3.3,1.849-.4Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-271.673-111.991l0.659-3.112,1.471,2.452Zm0.679-2.993-0.621,2.928,2.006-.62Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.58-112.627l-0.015-.025-1.45-2.417,3.5,0.32Zm-1.383-2.39,1.391,2.318,1.934-2.014Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.631-112.638l2.075-2.161-0.36,2.058Zm2.007-2.026-1.9,1.975,1.568-.094Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-267.963-112.727l0.36-2.057,1.132,1.646Zm0.385-1.943-0.329,1.882,1.364-.376Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266.509-113.12l-1.133-1.648,2.094,0.567Zm-1.029-1.574,1.033,1.5,0.877-.986Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.574-114.163l-2.075-.561,2.841-1.548Zm-1.957-.575,1.93,0.522,0.711-1.961Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-267.564-114.706l-3.482-.318,0.024-.032,2.8-3.618Zm-3.4-.355,3.345,0.306-0.627-3.812Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-267.609-114.7l-0.654-3.973,3.453,2.447Zm-0.593-3.876,0.626,3.807,2.683-1.462Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-268.267-118.583l1.56-4.278,1.594,3.053Zm1.567-4.169-1.493,4.092,3.018-1.171Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-264.823-116.179l-3.453-2.447,3.151-1.224Zm-3.361-2.436,3.309,2.345-0.289-3.518Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-264.868-116.186l-0.3-3.709,1.931,2.642Zm-0.248-3.557,0.286,3.479,1.525-1Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.271-117.217l-1.976-2.7,2.444,1.785Zm-1.773-2.5,1.766,2.417,0.419-.821Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.809-118.088l-2.391-1.746,2.527-.339Zm-2.276-1.717,2.237,1.635,0.127-1.952Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266.745-122.783l0.986-1.529,2.769,1.427Zm1-1.472-0.919,1.425,3.5-.1Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.147-119.776l-1.593-3.051,3.7-.1Zm-1.521-3.009,1.526,2.923,2.023-3.019Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.054-122.868l-2.749-1.417,2.7-1.2Zm-2.647-1.414,2.6,1.341-0.048-2.474Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.19-119.791l2.127-3.174,0.388,2.837Zm2.1-3.053-2.008,3,2.375-.319Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.1-122.841l-0.051-2.659,1.218,1.115-0.012.016Zm0-2.557,0.047,2.429,1.066-1.41Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.716-120.107l-0.391-2.859,1.82,1.8Zm-0.33-2.736,0.364,2.656,1.328-.98Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.285-121.1l-1.821-1.8,0.011-.015,1.146-1.516Zm-1.763-1.808,1.691,1.675-0.616-3.1Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.776-121.785l-4.271-1.813,3.9-.051Zm-4.061-1.772,4,1.7-0.349-1.747Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-268.227-118.565l-1.61-3.269,3.169-1.007Zm-1.548-3.243,1.542,3.131,1.493-4.094Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-273.92-126.027l-3.929-.236,0.961-2.007Zm-3.861-.276,3.719,0.223-2.809-2.122Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-273.919-123.535l-3.973-2.777,3.922,0.235Zm-3.82-2.724,3.774,2.638-0.048-2.414Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-273.964-123.537l-0.05-2.543,3.385,0.354Zm0-2.494,0.048,2.413,3.165-2.077Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-270.62-125.682l-3.4-.355,0.152-2.246Zm-3.348-.394,3.2,0.335-3.06-2.455Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-275.369-115.627l-1.606-3.914,3.5-.887Zm-1.546-3.884,1.545,3.766,1.819-4.619Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-277-119.487l0.036-.05,3.036-4.1,0.437,3.258Zm3.043-4.034-2.934,3.961,3.351-.85Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-270.967-115.015l-4.433-.65,1.883-4.784Zm-4.372-.686,4.3,0.63-2.47-5.267Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-273.533-120.363l-0.436-3.25,4.222,1.793Zm-0.382-3.179,0.418,3.118,3.632-1.4Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-271.009-115l-2.549-5.43,5.363,1.8-0.019.025Zm-2.465-5.355,2.474,5.271,2.732-3.525Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-268.187-118.583l-5.393-1.809,3.785-1.457,0.009,0.018Zm-5.263-1.812,5.176,1.736-1.543-3.135Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-280.361-116.167l-0.2-2.195,1.939,0.919Zm-0.153-2.122,0.19,2.04,1.613-1.186Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-278.621-117.392l-1.944-.921,0.409-2.149Zm-1.894-.946,1.8,0.852-1.42-2.839Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-279.2-114.477l-1.174-1.736,0.017-.013,1.719-1.264Zm-1.114-1.726,1.09,1.612,0.522-2.8Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-278.673-117.4l-1.514-3.028,3.281,0.891Zm-1.431-2.96,1.44,2.88,1.68-2.032Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-279.238-114.5l0.554-2.97,3.372,1.795-0.051.016Zm0.586-2.9-0.529,2.84,3.754-1.123Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-275.326-115.635l-3.376-1.8,1.764-2.134Zm-3.308-1.811,3.221,1.715-1.538-3.75Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-278.984-123.3l-3.425-1.324,4.631-1.7Zm-3.3-1.324,3.273,1.268,1.155-2.889Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-282.809-122.811l-1.5-1.671,2-.171Zm-1.409-1.635,1.388,1.545,0.457-1.7Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-284.309-124.439l1.6-2.565,0.39,2.4Zm1.572-2.442-1.488,2.391,1.852-.158Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-282.851-122.827l0.491-1.828,3.453,1.337Zm0.521-1.769-0.461,1.717,3.705-.461Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-277.809-126.238l-0.623-1.09,1.608-.968Zm-0.563-1.074,0.559,0.979,0.886-1.849Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-282.817-126.912l3.317-1.964,1.134,1.575Zm3.308-1.9-3.116,1.844,4.181-.365Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-282.361-124.59l-0.387-2.372,4.424-.387Zm-0.336-2.332,0.368,2.257,3.841-2.625Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-282.5-124.547l4.1-2.8,0.615,1.077-0.024.009Zm4.09-2.741-3.774,2.579,4.34-1.588Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.821-121.783l-0.375-1.873,2.164,0.494Zm-0.318-1.815,0.347,1.737,1.659-1.279Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-268.069-123.127l-2.1-.48,0.007-.044,2.263,0.134Zm-1.834-.464,1.808,0.413,0.128-.3Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.919-121.763l1.831-1.412,1.464,0.366Zm1.841-1.364-1.616,1.247,2.909-.924Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266.549-122.745l-1.564-.391,0.168-.391Zm-1.5-.421,1.193,0.3-1.065-.6Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-279.446-128.772l-1.6-1.234,0.555-.955Zm-1.541-1.245,1.422,1.1-0.927-1.947Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-278.39-127.261l-1.147-1.592,1.438-.822Zm-1.083-1.578,1.052,1.461,0.267-2.215Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-278.433-127.278l0.29-2.407,1.3,1.451Zm0.322-2.3-0.268,2.221,1.465-.882Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-273.891-125.952l-3.032-2.292,1.085-.465Zm-2.945-2.281,2.745,2.075-1.762-2.5Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-273.973-125.992l-1.924-2.724,2.077,0.46Zm-1.822-2.656,1.786,2.529,0.142-2.1Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-273.909-128.206l0.928-.91h0.011l2.351,0.366Zm0.943-.863-0.812.8,2.881-.475Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-273.809-128.209l-2.069-.458,1.374-2.086Zm-2-.487,1.935,0.428-0.65-2.379Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-273.855-128.2l-0.7-2.582,1.618,1.687Zm-0.617-2.426,0.64,2.342,0.828-.812Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-276.867-128.192l-1.3-1.452,1.742-.6Zm-1.221-1.432,1.2,1.338,0.41-1.889Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-276.909-128.2l0.452-2.082,0.642,1.614Zm0.464-1.931-0.4,1.857,0.975-.418Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-275.85-128.634l-0.633-1.589,2.017-.513Zm-0.573-1.559,0.582,1.464,1.275-1.936Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-279.5-128.785l-1.025-2.152,1.64,0.017Zm-0.955-2.107,0.945,1.984,0.566-1.968Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-279.542-128.8l0.62-2.151,0.826,1.325Zm0.635-2.044-0.562,1.952,1.311-.75Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-278.136-129.607l-0.828-1.327,2.585,0.724Zm-0.73-1.254,0.748,1.2,1.591-.546Z" transform="translate(295.283 135.142)"/>
                        <polygon class="cls-3" points="20.768 4.463 20.765 4.419 22.997 4.271 23 4.315 20.768 4.463"/>
                        <path class="cls-3" d="M-273.025-129.022l0.761-1.939,0.018,1.3Zm0.72-1.714-0.618,1.574,0.633-.515Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-270.651-128.708l-2.375-.37,0.757-.615Zm-2.27-.4,2.057,0.32-1.4-.853Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-272.289-129.613l-0.018-1.253,1.583-.33Zm0.027-1.217,0.015,1.111,1.389-1.4Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-270.735-128.708l-1.568-.954,1.534-1.551Zm-1.5-.963,1.452,0.884-0.032-2.32Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-270.778-128.621l-0.035-2.575,0.788,0.385Zm0.01-2.5,0.031,2.25,0.658-1.913Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266.541-128.892l-1.6-1.877,1.843-.12Zm-1.513-1.839,1.481,1.734,0.221-1.844Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-270.791-128.72l0.729-2.119,1.764,1.662Zm0.749-2.04-0.682,1.984,2.332-.429Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.375-126.483l-1.419-2.281,2.484-.456Zm-1.347-2.249,1.338,2.151,1-2.581Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-268.33-129.146l-1.779-1.681,2.036,0.051Zm-1.665-1.63,1.634,1.54,0.236-1.493Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.42-126.488l1.067-2.745,1.373,1.576Zm1.082-2.661-1,2.573,2.286-1.1Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266.5-128.914l-1.869-.257,0.258-1.629Zm-1.818-.3,1.709,0.235-1.474-1.725Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-267.009-127.622l-1.393-1.6,1.875,0.258Zm-1.282-1.539,1.266,1.454,0.438-1.219Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-270.8-131.114l-0.426-2.181,1.857,0.7Zm-0.368-2.112,0.4,2.02,1.326-1.373Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.373-132.55l-1.88-.707,1.311-.741Zm-1.776-.715,1.7,0.639-0.514-1.309Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-270.04-130.769l-0.788-.385,1.471-1.523Zm-0.714-.4,0.689,0.337,0.6-1.669Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.422-132.553l-0.563-1.433,2.9-.307Zm-0.5-1.4,0.52,1.325,2.161-1.609Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-270.1-130.727l0.68-1.9,0.68,0.75Zm0.7-1.817-0.6,1.677,1.2-1.015Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-268.06-130.727l-2.051-.052,1.344-1.137Zm-1.934-.093,1.855,0.047-0.639-1.076Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-268.769-131.847l-0.675-.744,2.372-1.766Zm-0.61-.738,0.6,0.667,1.52-2.249Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-268.109-130.727l-0.714-1.2,2.594,1.08Zm-0.608-1.109,0.632,1.064,1.665-.108Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-268.818-131.853l1.652-2.445,1.72,1.939Zm1.657-2.373-1.564,2.315,3.192-.479Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266.316-130.836l-2.537-1.056,3.4-.511Zm-2.374-1.036,2.354,0.98,0.806-1.454Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-294.4-122.327l-0.023-.038,2.837-2.127,1.017,0.3Zm2.823-2.116-2.634,1.975,3.521-1.715Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.209-104.144l-1.89-.557,1.091-.388,0.009,0.012Zm-1.747-.561,1.618,0.477-0.684-.809Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.228-104.268l0.731-1.3,0.019,0.011,1.514,0.861Zm0.748-1.237-0.663,1.176,2.053-.385Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.216-104.2l0.123-1.451,0.639,0.1Zm0.163-1.4-0.1,1.2,0.632-1.12Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.167-104.261l-1.188-.713,1.307-.69Zm-1.1-.711,1.06,0.636,0.107-1.251Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-264.773-103.608l0.45-1.4,0.024,0.014,1.162,0.7Zm0.475-1.332-0.4,1.255,1.467-.617Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-264.624-103.545l-0.926-.609,0.1-.37Zm-0.875-.628,0.648,0.426-0.574-.685Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.009-103.092l0.259-.571,1.56-.655,0.02,0.039Zm0.292-.537-0.19.419,1.5-.971Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-264.96-103.1l-0.627-1.128,0.878,0.576Zm-0.5-.993,0.5,0.9,0.2-.437Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.509-100.969l0.564-2.331,0.077,1.255Zm0.538-2.035-0.426,1.762,0.484-.813Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.135-99.18l-1.026-.453,0.715-1.548Zm-0.967-.475,0.911,0.4-0.276-1.777Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266.271-98.538l0.12-1.138,1.036,0.457Zm0.158-1.073-0.1.991,1.007-.593Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.839-97.509l-0.432-1.08,1.154-.68Zm-0.377-1.061,0.377,0.944,0.631-1.538Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-264.509-94.275l-0.143-2.6,0.831,1.831-0.009.01Zm-0.086-2.369,0.124,2.26,0.6-.669Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-264.461-94.2l-1.426-3.423,1.285,0.854Zm-1.33-3.306,1.272,3.052-0.126-2.291Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.535-93.7l-0.989-.627,0.69-.771Zm-0.921-.636,0.856,0.542L-263.859-95Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.574-81.064l-0.11-3.864,0.68,1.936v0.007Zm-0.058-3.583,0.094,3.305,0.488-1.649Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-264.371-79.576l0.691-5.215h0.044l0.1,3.591h0Zm0.7-4.937-0.622,4.692,0.716-1.388Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.755-77.535l-1.1-.649,1.923-.24Zm-0.971-.621,0.963,0.565,0.712-.774Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.815-77.535l0.815-.886,1.415,0.647Zm0.826-.833-0.714.777,1.953-.21Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.1-79.653l-2.268-.024,0.808-1.564Zm-2.2-.067,2.094,0.022-1.348-1.467Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.8-78.127l-0.562-1.59,2.269,0.024Zm-0.5-1.545,0.518,1.468,1.576-1.447Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.859-78.139l1.724-1.582,0.166,1.346Zm1.691-1.492-1.559,1.431,1.709-.214Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.139-79.627l-1.47-1.6,1.818-.072Zm-1.37-1.556,1.343,1.461,0.323-1.527Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.19-79.6l0.361-1.713,0.7,0.483Zm0.391-1.639-0.313,1.48,0.918-1.063Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.883-83.5L-261-83.619l1.167-.762Zm-0.991-.152,0.95,0.1,0.041-.75Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-251.032-96.471l-1.57-3.015,1.641,1.12v0.013Zm-1.45-2.88,1.413,2.712,0.063-1.7Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.958-100.341h-1.472l0.579-.8Zm-1.386-.044h1.271l-0.771-.687Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-256.367-100.287l-1.181-1.946,1.228-.156Zm-1.108-1.911,1.067,1.759,0.043-1.9Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-256.409-100.293l0.049-2.181,0.539,1.372Zm0.088-1.961-0.041,1.821,0.491-.675Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.485-102.178l-1.281-.6,1.16-.339Zm-1.156-.586,1.1,0.512-0.1-.8Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.531-102.191l-0.121-.933,1.378,0.773Zm-0.066-.852,0.1,0.8,1.081-.138Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-264.915-102.027l-0.071-1.156,0.03,0.009,3.13,0.911Zm-0.023-1.1,0.064,1.048,2.8-.214Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.9-102.237l-3.114-.907,1.836-1.185Zm-3.009-.922,2.912,0.848-1.2-1.956Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.944-102.227l-1.277-2.089,0.032-.006,2.2-.412Zm-1.206-2.058,1.2,1.958,0.892-2.35Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262-102.214l0.955-2.516,1.817,0.536Zm0.982-2.462-0.889,2.341,2.58-1.842Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.037-102.239l2.774-1.981,0.591,1.468H-258.7Zm2.754-1.913-2.578,1.841,3.127-.476Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.966-100.335l0.243-2.467,1.245,0.578Zm0.281-2.4-0.222,2.255,1.36-1.726Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-256.348-100.34l-2.633-.047,1.472-1.867Zm-2.543-.089,2.464,0.043-1.087-1.791Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.918-100.371l-0.03-.019-3.064-1.9,3.333-.509Zm-2.969-1.891,2.932,1.816,0.226-2.3Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.655-99.91l-0.009-.015-1.345-2.413,0.072,0.045,3.056,1.893Zm-1.233-2.3,1.254,2.249,1.644-.454Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.846-95l-0.806-1.775,2-1.025Zm-0.748-1.755,0.747,1.646,1.112-2.6Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-264.625-96.734l-1.281-.851,3.318-.19Zm-1.147-.815,1.15,0.764,1.828-.934Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.873-97.543l0.7-1.7,2.555,1.519Zm0.721-1.641-0.653,1.593,3.041-.174Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.584-93.714l-0.29-1.358h0.028l3.351,0.046Zm-0.235-1.313,0.267,1.251,2.847-1.208Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.555-94.982l-3.325-.046,1.184-2.763Zm-3.258-.089,3.168,0.044-2.041-2.677Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.6-92.906l-1.01-.845,0.033-.015,3.068-1.3Zm-0.927-.833,0.923,0.773,1.912-1.976Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.747-97.72l2.1-2.248,1.709,1.9Zm2.1-2.183-1.99,2.129,3.608-.328Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.6-94.963l-0.021-.028-2.116-2.775,3.791-.344Zm-2.053-2.766,2.046,2.684,1.584-3.014Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.96-98.027l-1.725-1.92,1.771-.489Zm-1.645-1.9,1.6,1.783,0.043-2.238Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.009-98.035l0.047-2.4h0.022l2.6,0.046Zm0.09-2.351-0.044,2.252,2.513-2.208Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.792-96.853l0.681-2.52,1.62-.07Zm0.715-2.477-0.623,2.308,2.106-2.372Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-251.009-96.527l-3.779-.391,2.253-2.537,0.015,0.028Zm-3.689-.426,3.612,0.373-1.458-2.8Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-252.109-95.29l-2.729-1.68,3.833,0.4Zm-2.541-1.616,2.533,1.559,1.024-1.191Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.2-93.341l-0.577-3.641,2.7,1.659Zm-0.518-3.553,0.548,3.464,2.017-1.885Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.252-90.9l0.048-2.587,1.064,2.563Zm0.088-2.375-0.043,2.33,1-.021Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-253.171-90.888l-1.046-2.52,1.93,0.023Zm-0.98-2.475,0.977,2.352,0.825-2.33Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-252.3-93.34l-1.941-.023,2.152-2.012Zm-1.831-.066,1.791,0.021,0.2-1.877Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.641-94.972l1.66-3.159,1.368,2.468Zm1.661-3.066-1.578,3,2.878-.657Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.509-91.568l-0.118-1.388h0.023l2.189-.139Zm-0.07-1.347,0.107,1.265,1.909-1.394Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.657-92.91l2.076-2.146,0.117,2.006Zm2.038-2.043-1.927,1.992,2.035-.13Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-255.885-93.133l-1.795-2.555,2.969-1.289Zm-1.728-2.536,1.712,2.437,1.12-3.667Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-255.437-90.148l-0.492-3.12,1.732,2.347Zm-0.42-2.949,0.453,2.876,1.143-.712Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-255.924-93.157l1.185-3.879,0.58,3.67Zm1.171-3.683-1.109,3.631,1.653-.195Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.209-90.861l-1.725-2.339,1.772-.21Zm-1.644-2.3,1.6,2.173,0.043-2.368Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-255.839-93.145l-2.375-.675,0.559-1.909Zm-2.32-.705,2.212,0.629-1.691-2.408Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.173-93.8l-2.493-1.208,0.061-.014,2.99-.683ZM-260.534-95l2.334,1.131,0.521-1.783Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.509-93.04l-0.116-2,0.033,0.016,2.463,1.193Zm-0.068-1.928,0.109,1.868,2.222-.739Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.995-89.434l-1.522-3.65,2.359-.784Zm-1.463-3.623,1.447,3.47,0.8-4.215Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.783-89.206l0.117-1.639,1.991,0.8Zm0.157-1.576-0.108,1.509,1.94-.776Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.626-89.982l-2.045-.819,0.168-.85Zm-1.995-.846,1.777,0.711-1.631-1.45Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.717-90l-1.807-1.606,2.062-1.507Zm-1.737-1.6,1.7,1.511,0.245-2.934Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.965-89.475l-0.045-.014-1.748-.545v-0.017l0.254-3.111Zm-1.746-.59,1.669,0.52-1.431-3.433Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.083-86.138H-258.4l1.078-3.376Zm-1.253-.044h1.2l-0.215-3.1Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.339-89.317l-0.876-4.549,2.351,0.668-0.008.023Zm-0.819-4.487,0.833,4.326,1.4-3.69Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.124-86.084l-0.233-3.367,1.4,1.263Zm-0.182-3.261,0.216,3.109,1.08-1.943Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-255.981-88.152l-1.42-1.278,2.178,0.308Zm-1.283-1.214,1.278,1.15,0.681-.873Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.371-89.357l1.487-3.911,0.488,3.1Zm1.469-3.74-1.391,3.66,1.848-.761Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-255.238-89.079l-2.178-.308,2.013-.829Zm-2.011-.329,1.959,0.277-0.149-1.022Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.309-89.373h-0.028l-1.7-.114,0.843-4.463Zm-1.668-.154,1.618,0.109-0.827-4.3Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.357-86.116l-1.308-1.486,2.376-1.857ZM-259.6-87.6l1.226,1.392,1-3.132Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.63-87.538l-1.145-2.547,1.8,0.561Zm-1.065-2.476,1.059,2.354,0.6-1.836Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.677-87.537l0.658-2H-259l1.729,0.116Zm0.689-1.95-0.6,1.826,2.2-1.719Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.032-86.627l0.256-2.66,1.377,1.47Zm0.291-2.559-0.238,2.459,1.514-1.1Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.422-87.781l-1.374-1.468,2.1-.839Zm-1.3-1.451,1.279,1.365,0.672-2.145Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.6-87.572l-1.864-.233,0.722-2.307Zm-1.806-.27,1.734,0.217-1.062-2.363Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.7-84.741l0.681-1.974,1.128,0.964-0.027.015Zm0.7-1.9-0.623,1.8,1.654-.923Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.909-85.714l-1.128-.964,1.645-1.2Zm-1.057-.961,1.031,0.88,0.472-1.975Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.946-83.6l-1.034-2.217,2.162,1.48Zm-0.926-2.09,0.945,2.025,1.03-.673Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.022-82.913l-0.7-2,0.9,1.157Zm-0.57-1.754,0.562,1.6,0.158-.67Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.844-83.7l-0.847-1.09,0.022-.013,1.79-1Zm-0.781-1.077,0.771,0.993,0.879-1.914Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.9-83.609l-1.98-.117,0.955-2.081Zm-1.913-.157,1.842,0.108-0.953-2.044Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.6-81.118l0.567-1.916,0.624,0.613-0.015.016Zm0.588-1.833-0.493,1.662,1.034-1.13Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.609-81.18l1.169-1.277,0.662,1.2Zm1.16-1.2-1.06,1.153,1.654-.065Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.4-82.343l-0.654-.642,0.2-.845ZM-263-83l0.512,0.5-0.356-1.166Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.455-82.382l-0.424-1.388h0.032l1.968,0.116Zm-0.363-1.34,0.386,1.265L-261-83.615Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.819-81.236l-0.672-1.224,1.368,0.48ZM-262.4-82.38l0.587,1.07,0.608-.651Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.144-81.94l-1.344-.472,1.58-1.273Zm-1.255-.487L-261.18-82l0.214-1.583Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.451-84.5l-1.3-1.626,1.706,0.741Zm-1.154-1.515,1.144,1.432,0.358-.779Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.035-85.327l-1.734-.754,1.418-.1Zm-1.551-.722,1.474,0.64-0.269-.723Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.956-85.727l0.511-2.139,1.813,1.813-0.043.006Zm0.536-2.052-0.478,2,2.173-.3Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.84-84.3l-2.146-1.47,2.333-.327Zm-2.027-1.442,1.991,1.363,0.173-1.666Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.658-86.018l-1.835-1.836,1.883,0.236Zm-1.713-1.776,1.672,1.672,0.043-1.458Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.883-84.312l0.188-1.814,1.28,1.6Zm0.221-1.7-0.172,1.65,1.336-.193Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.7-86.046l0.047-1.609,1.333,1.514Zm0.088-1.5-0.043,1.449,1.243-.085Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.939-100.687l-2.553-.4,0.591-.993Zm-2.482-.435,2.3,0.363-1.771-1.259Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.023-100.692l-0.018-.013-1.916-1.362,3.053-.233Zm-1.809-1.34,1.8,1.279,1.041-1.5Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.709-97.7l-0.347-3.057,0.033,0.01,2.416,0.8ZM-263-100.691l0.328,2.894,1.99-2.129Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.6-99.9l-2.465-.814,1.116-1.6Zm-2.4-.837,2.3,0.76-1.26-2.258Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.179-99.178l-0.3-1.954,0.03,0,2.482,0.39Zm-0.251-1.9,0.284,1.824,2.061-1.455Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.664-97.706l-2.539-1.509,0.028-.019,2.165-1.528Zm-2.458-1.512,2.4,1.429-0.328-2.894Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.709-95.627l1.832-2.9,1.16,1.6Zm1.835-2.82-1.721,2.723,2.81-1.22Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-255.876-98.427l-0.541-1.96h1.435Zm-0.483-1.916,0.493,1.788,0.815-1.788h-1.308Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-255.916-98.443l0.894-1.96,0.964,1.058-0.027.013Zm0.906-1.881-0.815,1.788,1.7-.823Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.737-96.887L-255.9-98.5l1.843-.894Zm-1.1-1.594,1.082,1.493,0.627-2.322Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.648-95.635l-1.368-2.467h0.032l3.157-.41Zm-1.3-2.432,1.3,2.345,1.732-2.74Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.049-98.053l2.674-2.35,0.533,1.933Zm2.65-2.27-2.513,2.207,3.014-.392Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-237.828-102.014l-0.3-1,1.331,0.814Zm-0.221-.9,0.252,0.851,0.882-.158Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-231.655-86.232l-1.952-.507V-86.75l-0.5-1.615Zm-1.917-.543,1.746,0.453-2.2-1.908Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-233.121-85.979l-0.517-.814,2,0.518Zm-0.42-.743,0.441,0.693,1.261-.252Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-231.744-86.252l-0.011-.009-2.409-2.09L-230-87.035Zm-2.23-1.993,2.237,1.941,1.612-.724Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-230.052-87l-0.018-.006-4.087-1.292,5.4-1.088Zm-3.929-1.289,3.907,1.236,1.236-2.271Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-234.609-88.99v-0.024l-0.347-2.047,6.368,0.359Zm-0.3-2.024,0.334,1.969,5.717-1.628Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-234.076-88.27l-0.554-.759,0.03-.009,5.867-1.67,0.015,0.042ZM-234.557-89l0.5,0.679,5.045-2.256Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-234.258-88.237l5.555-2.485-0.071,1.381Zm5.507-2.415-5.128,2.294,5.062-1.019Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-228.821-89.281l0.073-1.429,0.848,0.065Zm0.115-1.382-0.063,1.226,0.79-1.17Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.915-90.6l-0.843-.066,0.623-1.695Zm-0.782-.105,0.732,0.057-0.191-1.527Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-226.642-93.374l0.456-2.825,0.152,1.938v0.007Zm0.441-2.454-0.366,2.266,0.488-.711Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.014-105.616l-1.111-2.255v-0.006l0.065-2.4Zm-1.067-2.265,0.943,1.914-0.887-3.954Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-225.109-104.583l-1.986-1.194-1.005-4.478Zm-1.947-1.222,1.834,1.1-2.762-5.239Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.979-109.977l-2.507-3.3,1.433,0.107v0.014Zm-2.413-3.245,2.286,3-0.98-2.906Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-232.969-112.741l0.288-1.151,0.022,0.006,2.317,0.642Zm0.32-1.1-0.26,1.04,2.373-.455Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.962-110.027L-233-112.776l2.573-.494,0.009,0.011Zm-4.912-2.729,4.752,2.592-2.326-3.057Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-236.248-114.085l-0.229-1.717,0.572,0.839v0.011Zm-0.161-1.542,0.182,1.364,0.273-.7Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-236.209-114.127l-2.182-1.4,1.967-.215Zm-2.052-1.364,2,1.276-0.2-1.472Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-244.754-107.327l-0.512-1.538v-0.007l1.022-2.482Zm-0.465-1.536,0.446,1.339,0.446-3.506Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-244.168-104.191l-1.168-2.313,0.006-.01,0.578-.976Zm-1.118-2.311,1.023,2.024-0.512-2.887Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-244.329-111.166l1.406-2.142,0.016,0,4.339,1.4Zm1.424-2.09-1.334,2.035,5.47-.7Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-238.59-111.864l-4.361-1.407,1.336-1.688,0.018,0.018Zm-4.287-1.429,4.131,1.332-2.865-2.931Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-238.6-111.808l-3.065-3.137,1.6-.107,0.007,0.014Zm-2.967-3.1,2.824,2.89-1.346-2.989Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-238.655-111.827l-1.448-3.215,1.8-.494Zm-1.387-3.186,1.359,3.017,0.332-3.481Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-238.7-111.857l0.351-3.689,2.143,1.37Zm0.388-3.613-0.333,3.5,2.367-2.2Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-238.755-111.862l2.514-2.337,1.771,0.991Zm2.521-2.283-2.348,2.183,4-1.257Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-232.651-106.976l-0.312-5.831L-228-110.1Zm-0.264-5.754,0.3,5.674,4.526-3.04Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-232.733-106.974l4.714-3.165-0.063,2.282H-228.1Zm4.667-3.081-4.463,3,4.4-.837Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.036-105.759l-0.045-.01-5.659-1.248,4.649-.883Zm-5.485-1.255,5.4,1.192-1-2.029Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-236.849-102.182l-0.016-.01-1.261-.77,0.027-.02,5.622-4.176Zm-1.2-.784,1.189,0.727,4.073-4.635Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-236.168-101.213l-0.714-1,0.011-.013,4.223-4.805,0.036,0.026Zm-0.658-1,0.655,0.917,3.386-5.515Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-232.609-106.979l-0.034-.017-4.424-2.171,0.025-.022,4.12-3.626v0.046Zm-4.379-2.2,4.331,2.126-0.3-5.669Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-237.025-109.127l-0.019-.031-1.658-2.763,0.027-.009,4.2-1.319Zm-1.61-2.769,1.61,2.684,2.45-3.959Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-237.121-109.059l2.586-4.18,1.641,0.46-0.03.027Zm2.606-4.129-2.413,3.9,3.944-3.47Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-226.153-95.963l-0.052-.031-4.282-2.6,3.694,0.711v0.012Zm-4.1-2.539,4.021,2.439-0.593-1.78Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-233.54-99.44l-3.138-.355,0.5-1.492,0.024,0.017Zm-3.079-.393,2.911,0.329-2.45-1.713Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-235.386-97.794l-0.014-.022-1.292-2.025h0.046l3.072,0.348Zm-1.219-1.993,1.228,1.925,1.7-1.593Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-233.609-99.432l-0.03-.021-2.562-1.793,0.011-.017,3.592-5.852Zm-2.533-1.826,2.5,1.748,0.974-7.406Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-235.083-95.3l-0.321-2.534,1.832-1.713Zm-0.275-2.517,0.295,2.328,1.388-3.9Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-235.117-95.342l1.479-4.157,0.02,0.006,3.3,0.937Zm1.507-4.1-1.42,3.992,4.607-3.086Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-233.65-99.44l1-7.626,5.053,5.654-0.032.01Zm1.035-7.525L-233.6-99.5l5.927-1.93Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-230.364-98.527h-0.013l-3.322-.944,6.144-2Zm-3.184-.951,3.17,0.9,2.67-2.8Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.616-101.373l-0.032-.036-5.045-5.643,5.641,1.244v0.02Zm-4.953-5.607,4.921,5.506,0.547-4.3Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-226.781-97.834l-0.033-.007-3.6-.692,2.8-2.935,0.009,0.04Zm-3.546-.727,3.488,0.671-0.8-3.488Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-226.819-97.8l-0.013-.057-0.834-3.615,2.083,1.377Zm-0.779-3.574,0.8,3.45,1.161-2.156Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.664-101.346l0.57-4.482,0.029,0.018,1.931,1.161-0.015.02Zm0.6-4.409L-227.6-101.5l2.4-3.134Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-225.609-100.059l-0.016-.01-2.072-1.37,3.68-.354Zm-1.96-1.348,1.952,1.29,1.49-1.621Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.68-101.4l2.52-3.291,1.12,2.941Zm2.507-3.2-2.41,3.147,3.481-.335Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-231.909-93.636l0.014-.045,1.515-4.9,0.024,0.015,4.214,2.556Zm1.554-4.878-1.484,4.8,5.6-2.306Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-228.718-90.648l-0.024-.023-3.23-3.062,3.855,1.45-0.007.02Zm-3.065-2.967,3.047,2.889,0.563-1.531Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-228.141-92.245h-0.012l-3.86-1.452,5.47,0.213Zm-3.6-1.4,3.593,1.352,1.484-1.154Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-226.585-93.446h-0.02l-5.374-.21,5.814-2.393Zm-5.19-.246,5.152,0.2,0.4-2.487Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-235.818-92.721l0.144-1.044,3.972,0.072Zm0.182-1-0.13.942,3.713-.877Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-234.943-91l-0.881-1.762,0.027-.006,4-.946Zm-0.817-1.732,0.831,1.662,2.969-2.559Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-231.787-93.65l-3.9-.071,0.6-1.708,0.023,0.012Zm-3.837-.114,3.657,0.066-3.094-1.668Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-231.865-93.641l-0.023-.013-3.228-1.741,0.03-.02,4.754-3.185-0.018.06Zm-3.166-1.758,3.14,1.693,1.48-4.788Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-228.669-90.662l-6.323-.356,3.115-2.685,0.014,0.013Zm-6.212-.394,6.094,0.344-3.092-2.932Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-239.409-106.249l-4.925-4.96,5.688-.729Zm-4.831-4.928,4.8,4.833,0.744-5.543Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-242.295-102.59l-1.961-1.751,4.9-2.008Zm-1.879-1.737,1.874,1.673,2.81-3.591Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-242.346-102.6l2.942-3.759,0.28,3.479Zm2.907-3.643-2.81,3.591,3.078-.268Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-239.168-102.873v-0.021l-0.285-3.546,1.4,3.5Zm-0.22-3.281,0.26,3.234,1.01-.065Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-239.454-106.227l0.01-.076,0.762-5.678,1.684,2.807-0.01.012Zm0.8-5.623-0.735,5.477,2.338-2.805Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-239.354-106.26l-5.434-1.149v-0.02l0.485-3.812Zm-5.385-1.184,5.249,1.111-4.778-4.812Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-244.233-104.3l-0.558-3.153,5.444,1.152Zm-0.5-3.1,0.537,3.034,4.7-1.926Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-238.1-102.927l-0.01-.027-1.347-3.36,6.9-.73-0.065.048Zm-1.294-3.349,1.314,3.279,5.368-3.987Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-239.473-106.27l2.442-2.931,0.016,0.008,4.463,2.191Zm2.454-2.876-2.351,2.821,6.661-.7Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-225.553-91.75l1.035-2.107,0.406,0.666-0.012.012Zm1.039-2.015-0.882,1.8,1.229-1.229Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-224.137-93.149l-0.407-.665,0.013-.012,1.429-1.4Zm-0.351-.658,0.347,0.567,0.882-1.775Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-223.332-92.642l-0.836-.544,0.009-.018,1.008-2.029Zm-0.779-.56,0.74,0.482,0.16-2.294Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-225.249-90.373l-0.26-1.531,1.057,0.656Zm-0.2-1.442,0.224,1.345,0.7-.768Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-224.465-91.207l-1.044-.648,1.4-1.4Zm-0.974-.656,0.942,0.584,0.324-1.85Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-224.53-89.757l-0.735-.662,0.809-.883Zm-0.673-.665,0.634,0.57,0.063-1.33Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-224.576-89.719l0.074-1.559,0.668,0.334Zm0.115-1.489-0.063,1.318,0.628-1.036Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-224.518-91.16l0.363-2.071,0.836,0.545Zm0.4-2-0.322,1.832,1.061-1.35Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-223.852-90.9l-0.664-.332,1.217-1.549Zm-0.6-.347,0.569,0.284,0.473-1.611Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-224.072-101.707l-1.124-2.951,2.676-.6Zm-1.065-2.919,1.067,2.8,1.474-3.374Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-256.276-129.706l-1.171-.364-0.495-.854Zm-1.141-.4,0.9,0.281-1.283-.939Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-255.709-128.206l-1.342-.34,1.108-.509Zm-1.21-.352,1.147,0.29-0.2-.725Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.254-127.606l0.252-.973,1.332,0.336Zm0.284-.92-0.219.847,1.377-.554Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-256.622-126.691l-0.633-.962,1.566-.629Zm-0.566-.941,0.564,0.858,0.832-1.419Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.753-125.927l-1.9-.791,0.918-1.565Zm-1.838-.812,1.756,0.73-0.909-2.175Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-255.324-125.275l-1.382-1.516,1.947,0.81Zm-1.216-1.4,1.213,1.332,0.5-.621Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-253.883-124.916l-1.481-.379,0.569-.711Zm-1.4-.4,1.282,0.328-0.789-.944Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-253.939-124.914l-0.9-1.08,1.431,0.015Zm-0.808-1.035,0.8,0.954,0.466-.94Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-253.393-125.935l-1.417-.015-0.005-.014-0.994-2.363Zm-1.387-.059,1.279,0.014-2.177-2.164Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-250.04-128.3l1.629-1.612,1.234,0.89Zm1.634-1.554-1.488,1.472,2.614-.66Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-247.16-128.956l-1.282-.925,0.476-.494Zm-1.215-.931,1.077,0.777-0.677-1.192Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-247.233-129l-0.791-1.392,1.491,0.709Zm-0.683-1.292,0.693,1.22,0.614-.6Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-248.009-130.309l0.192-.441,1.114,0.192Zm0.219-.392-0.145.333,0.986-.188Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-246.537-129.627l-1.5-.716,1.231-.235Zm-1.363-.7,1.294,0.616-0.234-.819Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-246.8-130.527l-1.041-.18,0.942-1.256Zm-0.962-.211,0.914,0.158-0.086-1.261Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-246.844-130.521l-0.1-1.443L-246-130.99Zm-0.047-1.326,0.086,1.254,0.737-.408Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-245.991-130.921l-0.956-.984,0.547-.619Zm-0.9-.985,0.813,0.836-0.348-1.362Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-246.939-131.848l-0.018-1.926,0.571,1.3Zm0.028-1.711,0.015,1.6,0.458-.519Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-246.416-132.434l-0.579-1.316,0.925,0.821Zm-0.457-1.149,0.464,1.056,0.278-.4Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-246.071-132.871l-0.9-.8,0.754-.48Zm-0.826-.793,0.769,0.684-0.124-1.1Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-246.119-132.895l-0.141-1.241,2.961,0.237Zm-0.091-1.193,0.129,1.133,2.573-.917Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-246.933-133.641l-2.11-1.119,2.865,0.638Zm-1.8-1.005,1.8,0.954,0.644-.41Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.916-133.475l-0.99-.747,4.275-.542Zm-0.877-.717,0.884,0.665,2.933-1.15Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.818-133.456l-2-.451,0.968-.325Zm-1.833-.459,1.643,0.371-0.846-.639Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.679-131.558l-0.107-1.613,1.134-.03Zm-0.06-1.57,0.1,1.431,0.911-1.458Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.893-133.127l2.8-1.058-1.6,1.026Zm2.428-.869-2.168.818,0.933-.024Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.7-131.6l1.007-1.611,1.253,1.524Zm1.012-1.536-0.931,1.489,2.088-.081Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.625-132.4l-0.958-.233,1.916-.573Zm-0.789-.237,0.778,0.189,0.778-.654Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.959-131.99l-0.6-.678,0.971,0.236Zm-0.476-.6,0.475,0.535,0.29-.349Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.636-131.579l-1.029-.84,0.921-.776Zm-0.96-.841,0.909,0.743-0.1-1.428Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.561-131.574l-1.436-.438,0.363-.438Zm-1.36-.461,1.159,0.354-0.865-.708Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-249.289-133.476l-2.612-1.266,3.043,0.017Zm-2.418-1.221,2.393,1.16,0.394-1.144Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-249.333-133.482l0.432-1.252,2.046,1.085Zm0.457-1.189-0.394,1.144,2.258-.153Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-247.809-130.686l-1.558-2.889,2.477,1.663Zm-1.435-2.753,1.441,2.672,0.85-1.134Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-246.9-131.864l-2.473-1.66,2.456-.166Zm-2.341-1.625,2.3,1.542-0.016-1.7Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-250-128.277l0.483-2.7,1.142,1.089Zm0.512-2.607-0.444,2.478,1.493-1.477Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-247.762-130.7l-1.765-.21v-0.018l0.209-2.653Zm-1.718-.249,1.639,0.2-1.444-2.677Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-248.409-129.852l-1.191-1.136,1.667,0.641Zm-1-1.016,1,0.954,0.4-.416Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-247.958-130.311l-1.718-.66,1.907,0.226Zm-1.373-.575,1.349,0.519,0.148-.341Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-252.192-132.933l-2.8-.56,3.218-1.262Zm-2.643-.574,2.61,0.523,0.39-1.7Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-252.238-132.927l0.418-1.821,2.588,1.255Zm0.449-1.757-0.391,1.7,2.808-.529Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-249.485-130.887l-2.776-2.085,2.984-.562Zm-2.67-2.06,2.633,1.977,0.2-2.51Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-252.748-127.032l-0.5-2.477,1.879,2.029Zm-0.426-2.332,0.46,2.275,1.266-.412Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-251.409-127.462l-1.874-2.023,3.371,1.138Zm-1.724-1.926,1.732,1.87,1.384-.818Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-249.95-128.313l-3.324-1.122,3.8-1.529Zm-3.2-1.126,3.163,1.068,0.452-2.522Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.524-131.721l-0.418-1.806,2.8,0.561Zm-0.36-1.751,0.39,1.685,2.22-1.162Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.909-130.812l1.164-3.139,2.277,2.208Zm1.182-3.06-1.111,3,3.283-.889Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-256.393-129.737l-1.527-1.117,3.482-.942Zm-1.427-1.1,1.422,1.04,1.82-1.917Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.47-131.686l-2.334-2.263,1.91,0.43v0.014Zm-2.191-2.186,2.114,2.05-0.384-1.66Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.49-131.679l-1.256-1.529,2.425,0.352Zm-1.15-1.469,1.153,1.4,1.073-1.08Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.318-132.811l-2.436-.353,1.482-.953Zm-2.313-.38,2.215,0.321-0.867-1.188Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.836-130.8l-1.688-.9,1.169-1.178Zm-1.615-.916,1.548,0.83-0.476-1.91Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.364-132.8l-0.954-1.307,1.714-.15Zm-0.872-1.27,0.866,1.187,0.69-1.323Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.874-130.769l-0.518-2.082,0.013-.008,1.69-1.107Zm-0.468-2.062,0.476,1.912,1.09-2.937Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.426-132.776l0.773-1.482,0.97,0.341Zm0.794-1.428-0.677,1.3,1.527-1Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-253.209-129.383l-1.34-2.393,2.176,0.035Zm-1.264-2.348,1.255,2.241,0.782-2.208Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-252.384-131.7l-2.212-.036L-252.18-133Zm-2.038-.077,2,0.032,0.184-1.176Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-253.248-129.4l0.832-2.348,2.982,0.814Zm0.86-2.3-0.786,2.218,3.6-1.449Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-249.4-130.876l-3.029-.828,0.2-1.294Zm-2.979-.86,2.768,0.756-2.583-1.939Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.462-129.227l-0.071-2.617,1.357,2.423Zm-0.022-2.439,0.064,2.388,1.174-.177Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-253.454-125.905l-1.042-3.453,1.792,2.3Zm-0.932-3.24,0.947,3.136,0.681-1.051Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-252.69-126.973l-1.791-2.294,1.288-.2Zm-1.71-2.262,1.63,2.087-0.458-2.264Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.418-129.227l-2.019-.531,1.949-2.054Zm-1.936-.555,1.89,0.5-0.065-2.42Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-255.97-129l-0.472-.8,2.11,0.555Zm-0.38-.73,0.4,0.682,1.4-.209Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-253.4-125.881l-2.374-2.359,1.345-1.049Zm-2.307-2.355,2.215,2.2-0.96-3.18Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-255.754-128.2l-0.232-.842,1.624-.244Zm-0.176-.806,0.2,0.731,1.208-.942Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-246.34-127.135l-0.437-.475,1.224,0.086Zm-0.329-.423,0.339,0.368,0.611-.3Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-245.161-127.392l-0.6-.118,1.242-.2Zm-0.351-.114,0.344,0.068,0.369-.185Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-244.982-126.566l-0.208-.861,0.568-.283Zm-0.157-.837,0.163,0.673,0.28-.9Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-245.016-126.605l0.345-1.1,0.789,0.526Zm0.369-1.028-0.3.942,0.973-.491Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-245.729-126.374l-0.641-.8,0.76-.375Zm-0.572-.781,0.539,0.67,0.1-.986Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-245.778-126.327l0.122-1.2,0.526,0.1Zm0.161-1.15-0.1.954,0.515-.872Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-245.793-126.393l0.637-1.08,0.2,0.84Zm0.62-.964-0.526.891,0.694-.2Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-246.637-126.67l-0.116-.978,0.446,0.484Zm-0.056-.848,0.086,0.724,0.244-.366Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-245.681-126.383l-0.975-.338,0.319-.478Zm-0.907-.361,0.777,0.269-0.523-.65Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-247.282-127.117l-0.1-.287,0.767-.235Zm-0.045-.258,0.066,0.185,0.431-.337Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-246.6-126.688l-0.714-.464,0.6-.473Zm-0.638-.467,0.583,0.379-0.09-.766Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-238.638-117.515l0.478-1.025,1.5-.3Zm0.509-.986-0.4.862,1.666-1.111Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-234.084-116.151l-2.721-2.655,1.062-.243,0.008,0.014Zm-2.632-2.63,2.437,2.378-1.486-2.6Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-234.183-116.235l-0.018-.031-1.646-2.875,2.2,2.063Zm-1.479-2.672,1.481,2.587,0.474-.75Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-233.719-117.046l0.45-.832,1.35,0.562Zm0.47-.776-0.389.719,1.556-.233Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-232.752-116.181l-0.975-.908,1.772-.266Zm-0.878-.878,0.87,0.811,0.712-1.048Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-231.742-115.41l-1.044-.8,0.8-1.177Zm-0.984-.809,0.927,0.71-0.217-1.756Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.586-115.56l-2.986-.109L-230.9-117.3l0.041,0.021Zm-2.95-.152,2.758,0.1-3.06-1.611Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-228.409-111.7l-0.7-1.461,1.029-.187Zm-0.63-1.428,0.613,1.29,0.295-1.455Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-228.445-111.733l0.34-1.679,0.408,1.361Zm0.348-1.5-0.289,1.424,0.636-.269Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-225.242-105.573l-1.07-3.785,4.256,2.138Zm-1-3.7,1.027,3.637,3.062-1.582Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-222.151-107.145l1.253-3.977,1.34,1.513-0.015.015Zm1.272-3.889-1.179,3.739,2.439-2.316Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-223.7-111.746h-0.018l-4.15-.3,4.561-1.048Zm-3.873-.328,3.841,0.282,0.362-1.247Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-220.869-111.05l-3.01-.736,3.077-.111Zm-2.691-.7,2.651,0.648,0.059-.746Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-223.749-111.746l0.393-1.354,2.621,1.245Zm0.421-1.292-0.362,1.246,2.773-.1Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-217.437-110.9l-3.476-.156,0.067-.847Zm-3.428-.2,3.066,0.137-3.007-.884Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-216.193-109.009l-1.514-2,2.115,1.313Zm-1.336-1.841,1.338,1.771,0.531-.61Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-213.909-103.62l-0.02-.047-2.346-5.51,3.126,3.82v0.012Zm-2.2-5.29,2.2,5.178,0.708-1.618Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-212.9-106.911l-3.326-2.128,0.6-.688Zm-3.259-2.138,2.964,1.9-2.43-2.51Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-213.155-105.3l-0.035-.043-3.106-3.8,3.272,2.094v0.013Zm-2.932-3.654,2.9,3.54,0.12-1.609Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-208.277-106.955l-1.087-3.037,1.885,2.527Zm-0.946-2.775,0.968,2.708,0.712-.455Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-205.256-100.954l-1.773-3.446,1.236,1.237Zm-1.6-3.215,1.475,2.866-0.447-1.838Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-196.4-114.527l2.772-1.634-0.667,1.571Zm2.679-1.528-2.509,1.479,1.9-.057Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-194.376-114.513l0.69-1.624,0.6,0.338Zm0.711-1.561-0.581,1.369,1.083-1.084Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-193.109-115.757l-0.6-.338,0.6-1.873v2.211Zm-0.544-.359,0.5,0.284v-1.852Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192-119.409l-0.834-.174v-2.44Zm-0.79-.21,0.725,0.151-0.729-2.271Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.389-117.773l-0.839-.175,0.334-1.362Zm-0.785-.209,0.72,0.15-0.434-1.318Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.433-117.766l-0.5-1.507,1.493,0.753Zm-0.42-1.42,0.443,1.348,0.892-.674Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-205.237-101.013l-1.25-1.252-0.522-2.191Zm-1.21-1.274,1.041,1.043-1.476-2.869Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-206.941-104.227l-1.408-2.883,1.743,1.833Zm-1.243-2.645,1.234,2.527,0.294-.921Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-206.3-106.047l-2.012-.939,0.8-.514,0.012,0.015Zm-1.921-.945,1.762,0.822-1.058-1.272Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-206.621-105.227l-0.026-.028-1.716-1.8,2.01,0.938Zm-1.549-1.693,1.529,1.607,0.235-.784Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-204.831-104.085l-0.014-.018-1.6-2.047,2.978,0.913Zm-1.488-1.982,1.495,1.919,1.272-1.071Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-203.485-105.2l-0.024-.009-2.95-.9,2.978-.717v1.63Zm-2.811-.908,2.767,0.848v-1.514Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-206.38-106.075l-1.172-1.41,2.994-.673Zm-1.092-1.383,1.093,1.315,1.7-1.942Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-206.442-106.07l1.824-2.085,1.15,1.368Zm1.823-2.017-1.7,1.941,2.771-.667Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-200.776-116.339l1.459-1.011,1.347,0.449Zm1.466-.962-1.266.877,2.435-.487Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-197.658-114.54l-0.423-2.385,0.891-.134v0.031Zm-0.372-2.348,0.373,2.1,0.413-2.22Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-197.685-114.633l0.465-2.5,0.576,2.236Zm0.472-2.3-0.416,2.237,0.931-.237Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-198.052-116.882l-1.335-.445,3.472-1Zm-1.187-.442,1.18,0.393,1.889-1.278Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-198.148-116.87l2.262-1.531-1.32,1.39Zm1.95-1.267-1.764,1.2,0.735-.111Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-195.12-118.14l-0.95-.11,0.006-.024,1.2-4.781,0.044,0.007Zm-0.9-.148,0.853,0.1,0.278-4.625Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-195.168-118.08v-0.086l0.3-4.921,2.085,1.224-0.012.02Zm0.34-4.933-0.286,4.764,2.269-3.6Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-186.485-125.539l-0.483-.459,2-.437Zm-0.392-.434,0.4,0.379,1.252-.739Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-185.354-125.373l-1.192-.18,1.484-.877Zm-1.063-.205,1.031,0.156,0.252-.914Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-188.176-123.885l1.249-2.135,0.477,0.454-0.016.016Zm1.259-2.065-1.057,1.806,1.461-1.422Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-187.756-121.581l-0.346-2.468,2.229,0.909Zm-0.292-2.4,0.325,2.313,1.765-1.461Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-185.885-123.1l-2.23-.909,1.646-1.6Zm-2.151-.925,2.089,0.852-0.548-2.357Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-185.919-123.049l-0.59-2.552,1.233,0.462Zm-0.532-2.478,0.538,2.312,0.578-1.893Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-185.277-125.091l-1.409-.528,1.334,0.2Zm-1-.422,0.939,0.352-0.05-.218Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-185.3-125.013l-0.09-.386h0l0.273-.986,0.043,0.01Zm-0.044-.385,0.037,0.159,0.122-.734Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-185.334-125.1l0.219-1.313,2.887,0.766Zm0.254-1.258-0.2,1.2,2.849-.5Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-182.388-125.61l1.292-1.686,1.117,0.985Zm1.3-1.622-1.182,1.542,2.2-.641Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-181.176-127.227l1.81-1.541-0.134,1.161Zm1.753-1.435-1.588,1.352,1.47-.333Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-179.545-127.6l0.132-1.143,3.033,1.121Zm0.169-1.082-0.12,1.038,2.874-.02Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-182.3-125.62l-2.832-.751,0.018-.025,2.084-2.978Zm-2.76-.777,2.7,0.717-0.7-3.584Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-182.342-125.6l-0.01-.048-0.727-3.743,2.015,2.123-0.012.015Zm-0.665-3.651,0.689,3.547,1.2-1.561Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-181.1-127.233l-2.01-2.118,3.76,0.627Zm-1.887-2.053,1.89,1.992,1.647-1.4Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-186.963-125.954l1.607-3.148,0.282,2.736Zm1.578-3-1.5,2.932,1.759-.384Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-185.109-126.327l-0.281-2.722,2.4-.3Zm-0.233-2.684,0.264,2.562,1.993-2.847Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-188.954-124.838l-0.408-5.008,2.471,3.868Zm-0.35-4.835,0.388,4.763,1.963-1.083Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-186.92-125.941l-0.02-.032-2.439-3.817,4.042,0.749Zm-2.367-3.787,2.363,3.7,1.521-2.98Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-185.371-129h0l-3.963-.733,0.006-.044,6.29,0.44v0.044Zm-3.576-.707,3.577,0.662,2.1-.264Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-204.833-130.612l-0.012-.015-1.289-1.632,3.772,0.281Zm-1.2-1.6,1.215,1.539,2.31-1.276Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-211.842-129.036l-0.795-1.893,6.688-1.354Zm-0.734-1.86,0.756,1.8,5.6-3.085Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-211.977-129.012l5.9-3.25,0.012,0.015,1.28,1.621-0.035.008Zm5.886-3.193-5.593,3.082,6.818-1.531Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-211.792-129.027l-2.824-.977,2.02-.934Zm-2.706-.983,2.627,0.91-0.747-1.779Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-215.394-128.536l0.827-1.5,2.83,0.979Zm0.848-1.448-0.766,1.392,3.388-.484Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-215.37-128.489l-0.155-2.064,1,0.533Zm-0.1-1.987,0.138,1.836,0.75-1.362Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-216.765-128.727l-0.5-1.555,1.818-.263Zm-0.446-1.519,0.461,1.422,1.2-1.662Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-215.329-128.538l-1.468-.219,1.314-1.819Zm-1.39-.252,1.342,0.2-0.14-1.862Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-224.651-127.841l-1.466-.074-4.445-1.656,0.014-.042Zm-1.454-.116,1.1,0.054-4.9-1.472Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-236.891-124.272l-0.99-1.717,1.893,0.792Zm-0.887-1.626,0.9,1.554,0.817-.837Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-236.009-125.158l-1.861-.779,2.467-2.12Zm-1.778-.792,1.746,0.731,0.568-2.72Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-236.058-125.15l0.6-2.883,2.432,1.42Zm0.633-2.814L-236-125.228l2.879-1.389Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-234.363-123.663l-1.7-1.531,0.029-.014,3.011-1.452Zm-1.625-1.52,1.609,1.446,1.263-2.831Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-233.065-126.585L-235.493-128l5.006-1.633Zm-2.321-1.407,2.311,1.35,2.454-2.9Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-232.333-125.543l-0.766-1.072,0.011-.013,2.616-3.1Zm-0.71-1.07,0.7,0.982,1.7-3.828Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-229.409-125.162l-2.955-.407,0.012-.027,1.808-4.063Zm-2.891-.443,2.832,0.39-1.088-4.31Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-229.456-125.149l-1.13-4.478,4.52,1.685Zm-1.067-4.407,1.092,4.329,3.278-2.7Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-235.309-123.143l-0.766-2.12,1.744,1.568Zm-0.665-1.97,0.689,1.906,0.879-.5Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-235.073-119.427l-0.666-3.286,2.9-.193-0.023.037Zm-0.613-3.245,0.634,3.13,2.128-3.314Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-238.169-118.493l0.327-3.485,1.111,3.2Zm0.352-3.279-0.3,3.224,1.33-.262Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-236.775-118.768l-0.007-.018-1.118-3.223,2.183,3Zm-0.986-2.975,1.014,2.924,0.955-.219Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-235.759-118.994l-2.2-3.024,2.935,2.535Zm-1.94-2.74,1.951,2.679,0.65-.433Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-235.028-119.427l-2.844-2.457,2.176-.84Zm-2.76-2.442,2.691,2.324-0.632-3.119Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-238.58-117.534l-2.273-3.046,2.736,2.053Zm-2.055-2.827,2.046,2.741,0.417-.894Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-238.466-117.527l-4.35-1.031,2.781,0.015Zm-3.964-.985,3.726,0.883-1.344-.87Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-239.925-115.973l-0.017-.016-2.756-2.586,4.153,0.985Zm-2.622-2.521,2.619,2.457,1.3-1.527Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-241.681-115.39l-0.007-.021-0.98-3.2,2.783,2.612Zm-0.9-3.072,0.924,3.016,1.684-.568Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-238.109-118.466l-2.672-2,1.767-1.508Zm-2.6-2.006,2.528,1.9-0.856-3.323Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-238.134-118.393l-0.92-3.569,1.248,0.065ZM-239-121.915l0.842,3.266,0.3-3.206Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-238.96-121.879l-1.961-1.048,1-.423Zm-1.859-1.043L-239.09-122l-0.845-1.3Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-240.885-122.893l-0.509-1.884,1.507,1.462Zm-0.425-1.741,0.455,1.68,0.889-.375Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-241.191-121.027l0.3-1.935,1.914,1.022Zm0.338-1.867-0.283,1.8,2.059-.848Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-242.961-121.727l0.513-1.247,0.757,1.069Zm0.523-1.156-0.452,1.1,1.121-.158Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-240.841-122.9l-0.534-.093v-1.883Zm-0.49-.13,0.43,0.075-0.43-1.589v1.514Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-241.686-121.818l-0.788-1.113,0.487-.3Zm-0.725-1.1,0.637,0.9-0.243-1.144Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-241.735-121.835l-0.3-1.393,0.707,0.206Zm-0.238-1.33,0.249,1.165,0.343-.993Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.778-123.327l-0.025-.027-1.672-1.842,3.414-2.813Zm-1.633-1.864,1.616,1.78,1.637-4.46Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-220.509-124.549l0.9-4.86,0.022,0.005,2.868,0.616-0.026.028Zm0.934-4.807-0.862,4.66,3.633-4.065Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-220.456-124.605l-0.03-.032,3.719-4.16,1.468,0.219Zm3.706-4.145-3.545,3.965,4.887-3.765Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-220.3-130.765h-0.008l-1.7-.1,5.1-1.258,0.013,0.042Zm-1.407-.127,1.4,0.083,3.077-1.188Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-222.134-129.327l0.261-1.572,1.628,0.1Zm0.3-1.526-0.236,1.421,1.708-1.334Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-220.454-124.583l-0.029-.021-4.4-3.277,5.319-1.532-0.006.036Zm-4.328-3.28,4.3,3.2,0.868-4.69Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-220.54-124.6l5.187-4,3.806,3.465Zm5.185-3.939-5.044,3.887,8.745-.517Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-211.575-125.094l-0.04-.036-3.788-3.449,3.593-.514v0.024Zm-3.729-3.455,3.678,3.35-0.226-3.843Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-211.62-125.1v-0.043l-0.232-3.94h0.018l7.1-1.593Zm-0.188-3.948,0.227,3.859,6.666-5.407Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-226.285-109.285l-0.011-.021-1.489-2.825,2.869,1.955Zm-1.378-2.71,1.394,2.647,1.273-.829Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-224.952-110.146l-2.852-1.945,4.127,0.3Zm-2.692-1.889,2.683,1.829,1.2-1.544Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-222-107.142l-4.322-2.172,1.368-.891,0.013,0.014Zm-4.234-2.177,4.023,2.021-2.75-2.85Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-225.009-110.142l1.284-1.653,2.936,0.718Zm1.3-1.6-1.191,1.534,3.916-.868Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-222.1-107.178l-2.9-3.011,4.143-.918-0.011.036Zm-2.818-2.985,2.8,2.9,1.194-3.786Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.734-112.017l-0.394-1.314,0.014-.009,3.117-2.049Zm-0.342-1.3,0.361,1.2,2.505-3.086Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-228.132-113.274l0.432-2.336,2.66,0.3-0.051.034Zm0.468-2.287-0.406,2.193,2.9-1.909Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-225.071-115.266l-2.633-.3-0.107-7.941Zm-2.589-.339,2.525,0.288-2.628-7.9Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.781-112.027l0.04-.05,2.638-3.25,0.018,0.021,1.782,2.25Zm2.678-3.23-2.563,3.158,4.286-.984Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-217.6-110.9l-3.274-.962,0.031-.027,3.372-2.927Zm-3.184-.982,3.142,0.924,0.123-3.758Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-220.822-111.848l-0.013-.006-2.569-1.22,6-1.743Zm-2.458-1.216,2.451,1.164,3.247-2.818Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-217.64-110.9v-0.022l0.127-3.878,4.353,3.137Zm0.169-3.821-0.123,3.769,4.324-.742Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-215.624-109.667l-2.055-1.276,4.542-.778Zm-1.933-1.252,1.929,1.2,2.335-1.929Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-213.023-106.974l-2.635-2.722,2.464-2.035Zm-2.57-2.718,2.522,2.605-0.164-4.553Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-213.067-106.983l-0.171-4.751,0.037,0.031,2.809,2.281-0.019.017Zm-0.124-4.655,0.164,4.559,2.567-2.341Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-210.43-109.4l-0.01-.008-2.927-2.377,4.129,1.921Zm-2.633-2.2,2.641,2.144,1.072-.417Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-207.509-107.427l-1.821-2.442,0.02-.013,3.515-2.193Zm-1.757-2.43,1.744,2.338,1.641-4.451Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-207.546-107.441l1.713-4.649,1.243,3.985Zm1.71-4.512-1.641,4.451,2.831-.636Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-204.626-108.071l-0.014-.043-1.248-4,2.22,2.633Zm-1.155-3.852,1.17,3.752,0.887-1.311Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-203.709-109.462l-0.007-.008-2.225-2.64,4.224,2.56Zm-2.025-2.471,2.045,2.426,1.825-.08Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-201.784-109.54l-4.124-2.5h0.073l6.658-.3ZM-205.761-112l3.969,2.406,2.509-2.694Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-205.9-112l5.231-4.424,1.468,4.126h-0.03Zm5.211-4.349-5.084,4.3,6.51-.29Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-200.709-116.336l0.653-5.484,0.762,4.5Zm0.661-5.175-0.6,5.083,1.311-.908Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-205.85-111.98l-0.006-.037-1.118-6.305,0.035,0.01,6.307,1.92Zm-1.068-6.279,1.1,6.2,5.1-4.31Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-200.658-116.353l-0.025-.008-6.323-1.924,6.985-3.418-0.005.04Zm-6.228-1.942,6.19,1.884,0.622-5.217Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-213.2-111.647l-0.029-.021-4.33-3.12,4.851-.15v0.027Zm-4.228-3.1,4.2,3.023,0.473-3.167Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-209.23-109.808l-4.008-1.865v-0.016l0.491-3.285,0.031,0.047Zm-3.96-1.891,3.833,1.783-3.361-4.94Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-209.3-109.827l-0.012-.018-3.48-5.114,7.006,2.938Zm-3.372-5.034,3.385,4.974,3.4-2.126Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-205.809-111.985l-6.976-2.927,5.852-3.414Zm-6.878-2.933,6.82,2.861-1.1-6.2Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-210.8-122.347l-0.007-.025-0.825-2.791,0.024-.005,10.515-2.022Zm-0.776-2.782,0.8,2.719,9.373-4.676Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-211.682-125.108l6.856-5.561,0.014,0.014,3.615,3.531Zm6.853-5.5-6.689,5.426,10.23-1.968Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-206.942-118.254l-0.019-.021-3.858-4.11,0.027-.013,9.612-4.8-0.044.068Zm-3.8-4.118,3.8,4.046,5.645-8.756Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-207.009-118.236l5.774-8.959,0.012,0.052,1.2,5.489-0.016.008Zm5.754-8.846-5.631,8.737,6.811-3.333Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-201.2-127.069l-3.662-3.576,2.441-1.349,0.007,0.028Zm-3.589-3.566,3.509,3.427-1.17-4.72Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-201.254-127.094l-1.214-4.9,0.036,0.01,4.543,1.246-0.024.027Zm-1.155-4.833,1.176,4.746,3.261-3.528Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-201.33-127.077l3.4-3.674,0.015,0.007,2.37,1.153Zm3.4-3.62-3.231,3.5,5.5-2.392Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-194.82-123.007l-0.04-.025-6.429-4.11,5.705-2.482v0.03Zm-6.376-4.128,6.321,4.041-0.746-6.466Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-194.86-122.973l-0.769-6.66,3.138,1.814Zm-0.715-6.578,0.741,6.423,2.285-4.673Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.5-127.771l-3.152-1.823,3.11-1.479Zm-3.057-1.819,3.012,1.742L-192.584-131Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-194.909-122.98l2.4-4.912,0.217,3.008Zm2.37-4.746-2.253,4.606,2.456-1.785Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.542-127.771l-0.043-3.3,3.3,1.307Zm0-3.235,0.041,3.157,3.116-1.906Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-188.873-124.852l-3.458-.021-0.215-2.986Zm-3.417-.065L-189-124.9l-3.5-2.864Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-188.909-124.827l-0.041-.033-3.609-2.955,0.027-.016,3.217-1.969Zm-3.574-2.983,3.521,2.883-0.391-4.8Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-200.063-121.639l-1.23-5.629,3.041,5.154Zm-1.128-5.37,1.161,5.316,1.711-.448Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-198.3-122.1l-0.008-.014-3-5.091,6.515,4.166Zm-2.88-4.968,2.9,4.917,3.374-.9Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-199.316-117.209l-0.756-4.474,1.815-.475Zm-0.706-4.441,0.711,4.207,1-4.654Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-199.342-117.295l0.007-.036,1.04-4.857,2.286,3.932Zm1.066-4.772-1.008,4.71,3.209-.926Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-196.034-118.211l-2.285-3.93,3.5-.939-0.009.036Zm-2.218-3.9,2.2,3.786,1.172-4.691Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.821-123.335l0.014-.037,1.694-4.616,0.023,0.042,2.09,3.821Zm1.716-4.547-1.647,4.488,3.685-.762Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-225.093-115.192l-2.723-8.189,0.025-.005,3.783-.782v0.03Zm-2.665-8.156,2.645,7.956,1.054-8.721Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-223.98-124l-2.167-3.962,1.335,0.066Zm-2.091-3.914,1.985,3.628-0.762-3.568Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-224.051-124.116v-0.02l-0.809-3.789,4.452,3.317Zm-0.746-3.7,0.78,3.655,3.491-.472Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-233.257-117.818l-1.847-1.676,4.554-1.718-0.05.063Zm-1.764-1.66,1.759,1.6,2.577-3.232Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-235.117-119.441l2.23-3.473,2.315,1.758Zm2.241-3.409-2.133,3.323,4.346-1.639Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-231.988-117.3l-1.307-.545,0.018-.023,2.725-3.417Zm-1.236-.563,1.21,0.5,1.33-3.689Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-230.575-121.1l-2.335-1.773,0.009-.017,1.136-1.973Zm-2.278-1.785,2.194,1.666-1.117-3.535Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-230.848-117.239l-1.184-.066,0.01-.028,1.437-3.983Zm-1.122-.107,1.081,0.06,0.24-3.723Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-230.617-121.087l-1.182-3.741,2.387-.387-0.01.033Zm-1.125-3.706,1.124,3.555,1.144-3.923Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.629-115.533l-3.263-1.717v-0.015l0.257-3.978Zm-3.217-1.743,3.111,1.638-2.865-5.445Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-230.658-121.1l1.205-4.129,1.7,1.871Zm1.225-4.041-1.144,3.922,2.758-2.145Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.658-115.494l-2.987-5.675,2.88-2.24Zm-2.931-5.663,2.884,5.48-0.1-7.643Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-225.131-115.245l1.082-8.949,0.032,0.04,4.272,5.113-0.02.014Zm1.113-8.842-1.057,8.748,5.265-3.71Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-223.339-113.027l-1.8-2.266,5.43-3.826Zm-1.732-2.257,1.727,2.179,3.5-5.859Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-219.746-118.974l-0.048-.057-4.282-5.126,3.626-.49Zm-4.245-5.15,4.183,5.007-0.68-5.481Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-223.389-113.033l3.613-6.057,2.317,4.336-0.027.008Zm3.611-5.967-3.516,5.894,5.77-1.675Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-219.8-119.027v-0.019l-0.7-5.633,0.042,0.036,6.672,5.51Zm-0.644-5.547,0.683,5.5,5.854-.1Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-213.834-119.114l-0.024-.019-6.669-5.508,8.959-.53Zm-6.578-5.49,6.559,5.417,2.22-5.936Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-234.355-131.587l-1.79-1.472,2.087-.267Zm-1.683-1.441,1.653,1.359,0.273-1.606Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-234.4-131.569l0.3-1.77,0.922,0.646Zm0.333-1.694-0.268,1.574,1.087-1Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-233.2-132.651l-0.95-.666,1.037-.1Zm-0.827-.633,0.791,0.555,0.073-.638Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-207.7-132.415l0.958-1.155,1.818,1.012Zm0.969-1.1-0.871,1.05,2.523-.13Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-207.651-132.407l-1.279-1.267,2.237,0.112Zm-1.166-1.217,1.162,1.152,0.872-1.05Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-207.568-132.387l-1.573-.546,0.258-.756Zm-1.517-.573,1.348,0.468-1.127-1.116Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-231.9-132.027l-1.342-.652,0.087-.766Zm-1.295-.678,1.116,0.542-1.043-1.179Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-231.987-132.063l-1.2-1.356,2.577,0.231Zm-1.094-1.3,1.1,1.241,1.26-1.029Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-217.5-114.727l-0.015-.029-2.3-4.309,6.018-.106-0.031.037Zm-2.245-4.3,2.253,4.217,3.6-4.32Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-217.54-114.744l3.707-4.453,0.011,0.041,1.118,4.263Zm3.686-4.359-3.59,4.312,4.683-.145Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-212.746-114.881l-0.008-.028-1.119-4.267,7,0.872Zm-1.068-4.243,1.1,4.176,5.705-3.328Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-213.9-119.056l0.038-.1,2.267-6.058,0.84,2.843-0.009.009Zm2.3-6.021-2.182,5.833,2.977-3.141Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-206.888-118.26l-7-.873,3.107-3.277,0.016,0.017Zm-6.91-.906,6.794,0.847-3.779-4.027Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-241.631-115.374l-0.89-.471v-0.012l-0.132-2.851Zm-0.847-.5,0.774,0.41-0.889-2.9Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-238.494-117.492l-1.571-1.018,0.1-.957Zm-1.524-1.04,1.342,0.87-1.256-1.688Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-237.884-121.827l1.381-2.22,0.823,1.369Zm1.38-2.135-1.273,2.047,2.032-.785Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-187.7-121.511l-0.845-1.5v-0.01l0.479-1.067Zm-0.8-1.507,0.72,1.281-0.309-2.2Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-195.033-86.217l-0.906-4.833,3.294,2.156-0.017.019Zm-0.844-4.74,0.871,4.644,2.294-2.573Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-193.609-85.013l-0.028-.024-1.417-1.226,2.415-2.708Zm-1.382-1.254,1.356,1.173,0.911-3.715Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-191.3-84.676l-2.347-.359v-0.023l0.96-3.912Zm-2.293-.4,2.229,0.341-1.312-4.079Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.357-84.492h-0.006l-1.426-.607,2.59,0.4ZM-193.45-85l1.1,0.467,0.888-.163Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-191.338-84.655l-1.381-4.29,0.054,0.037,2.927,2.046Zm-1.3-4.179,1.314,4.083,1.523-2.1Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-191.384-84.667l1.663-2.293-0.488,2.006Zm1.567-2.086-1.461,2.015,1.033-.252Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-189.758-86.822l-0.024-.017-2.963-2.071,3.761-.119-0.01.03Zm-2.853-2.048,2.831,1.978,0.734-2.091Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.72-88.866l2.011-3.065,0.018,0.03,1.713,2.916Zm2.009-2.982-1.926,2.935,3.584-.114Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.709-88.808l0.153-2.515h0.014l1.881-.6Zm0.2-2.482-0.141,2.319,1.889-2.879Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.7-88.887l-0.633-4.775,0.042-.01,0.782,2.363-0.147,2.421H-192.7Zm-0.559-4.549,0.573,4.317,0.132-2.185Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.651-88.845L-195.943-91l0.019-.02,2.628-2.694Zm-3.223-2.162,3.167,2.073-0.621-4.683Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-195.982-90.9l1.435-3.121,1.277,0.341-0.026.027Zm1.459-3.069-1.312,2.856,2.481-2.544Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-194.509-93.951l-2.225-1.455,2.17-.5Zm-2.114-1.435,2.068,1.352-0.052-1.815Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-196.783-87.387l-0.468-7.361L-195.885-91V-91Zm-0.4-7.059,0.431,6.772L-195.926-91Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-194.421-93.943l-2.834-.639,0.568-.844Zm-2.762-.668,2.534,0.571-2.026-1.325Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-195.909-90.946l-1.344-3.682,2.75,0.62Zm-1.275-3.621,1.279,3.5,1.34-2.915Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-197.956-91.32l-0.288-3.262,1.053-.038-0.007.028Zm-0.24-3.22,0.259,2.94,0.689-2.974Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-196.735-87.338l-1.234-4.121v0l0.761-3.284,0.009,0.15Zm-1.188-4.122,1.12,3.739-0.428-6.724Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-197.915-91.358l-1.36-2.451,1.074-.793Zm-1.3-2.439,1.241,2.236-0.261-2.959Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-197.891-91.407l-1.949-1.014,0.59-1.434Zm-1.893-1.034L-198-91.513l-1.243-2.24Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-196.7-87.378l-3.172-5.109,1.945,1.012v0.009Zm-3.05-5,2.913,4.691-1.127-3.762Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-198.887-86.665v-0.027l-0.96-5.845,3.114,5.017Zm-0.885-5.66,0.92,5.6,2.05-.815Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-202.176-89.063l-0.189-1.838v-0.006l0.453-.755-0.223,2.6h-0.041Zm-0.144-1.828,0.164,1.591,0.187-2.176Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-202.183-88.984l0.223-2.6,2.209-.894Zm0.265-2.569-0.207,2.408,2.253-3.237Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-200.091-86.592L-200.1-86.6l-2.189-2.587,3.48,2.51Zm-1.93-2.35,1.95,2.3,1.139-.076Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-198.839-86.646l-0.044-.031-3.3-2.383,0.012-.017,2.374-3.411,0.009,0.054Zm-3.285-2.424,3.224,2.326-0.925-5.629Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-199.98-86.527l-1.762-1.04v-0.008l-0.46-1.581Zm-1.725-1.069,1.523,0.9-1.922-2.272Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-200.9-86.038l-1.065-.346,0.23-1.267Zm-1.015-.376,0.927,0.3-0.726-1.4Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-200.952-86.045l-0.827-1.6,1.74,1.027Zm-0.717-1.482,0.735,1.419,0.81-.507Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-201.093-86.008l1.011-.629,1.407-.093Zm1.019-.585-0.718.449,1.728-.516Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-191.386-82.476l-0.6-1.145h1.259Zm-0.528-1.1,0.53,1.009,0.58-1.009h-1.11Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-182.69-83.656l0.267-2.821,0.3,2.049Zm0.276-2.45-0.217,2.3,0.459-.628Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-182.169-84.41l-0.281-1.943,1.83,1.774Zm-0.219-1.821,0.257,1.773,1.413-.154Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-181.509-83.345l-1.187-.377,0.55-.752Zm-1.115-.4,1.021,0.324-0.548-.972Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-181.56-83.343l-0.625-1.108,1.564-.171Zm-0.554-1.072,0.559,0.991,0.839-1.143Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-183.671-81.15l-0.019,0-1.085-.306,2.179-2.377Zm-1.023-.334,1,0.281,0.971-2.427Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-183.746-81.079l1.072-2.681,1.158,0.366-0.025.026Zm1.1-2.627-0.974,2.434,2.025-2.1Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-205.229-97.912l-3.237-5.1,2.561,2.083v0.008Zm-3.054-4.889,2.956,4.652-0.617-2.75Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-205.938-100.894l-2.531-2.059,0.7,0.191h0l1.858,1.831Zm-2.341-1.961,2.011,1.635-1.525-1.5Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-203.409-99.307l-0.416-2.273h2.91Zm-0.363-2.229,0.392,2.143,2.352-2.143h-2.744Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-200.944-101.536h-2.916l2.583-2.055Zm-2.79-.044H-201l-0.313-1.93Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-201.3-98.974l-2.141-.362,2.5-2.279-0.008.06Zm-2.044-.39,2.006,0.339L-201-101.5Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.709-96.769l-1.523-.546v-0.012l-0.085-1.222Zm-1.481-.578,1.331,0.477-1.406-1.557Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.786-96.788L-192.8-96.8l-1.565-1.733,2.6,0.9Zm-1.432-1.651,1.436,1.59,0.923-.77Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-191.8-97.6l-2.6-.906,3-.142Zm-2.375-.873,2.349,0.817,0.358-.945Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-190.909-96.46l-0.93-1.163,0.407-1.076Zm-0.88-1.171L-191-96.64l-0.446-1.908Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-190.076-96.358l-0.9-.173v-0.014l-0.517-2.213Zm-0.86-.211,0.773,0.15-1.222-2.069Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-194.315-98.465v-1.562l0.032,0.015,2.932,1.411Zm0.044-1.5v1.45l2.744-.13Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-194.271-98.427l-1.249-1.364,1.249-.227v1.591Zm-1.163-1.335,1.119,1.221v-1.425Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-195.479-99.758h-0.006l-1.2-.46,2.555,0.216Zm-0.9-.392,0.9,0.346,1.015-.185Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-200.109-105.894l0.061-2.5,0.392,1.267v0.007Zm0.1-2.233-0.047,1.966,0.355-.971Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-198.409-105.949l-1.7-.057,0.426-1.164Zm-1.641-.1,1.527,0.051-1.145-1.1Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-199.676-107.051l-0.382-1.233h0.705ZM-200-108.24l0.318,1.029,0.27-1.029H-200Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-198.409-105.887l-1.294-1.237,0.316-1.208Zm-1.245-1.251,1.129,1.079-0.853-2.133Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-200.795-98.566l0.445-2.47,0.172,1.322v0.007Zm0.438-2.182-0.346,1.917,0.479-.891Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-199.152-98.338l-1.631-.346,0.579-1.076Zm-1.564-.377,1.46,0.31-0.942-1.273Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-200.216-99.667l-0.161-1.239,1.854-.673Zm-0.113-1.21,0.144,1.108,1.514-1.709Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-199.191-98.318l-1.038-1.4,0.012-.014,1.657-1.871Zm-0.981-1.4,0.957,1.292,0.582-3.029Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-205.295-98l-0.69-3.08,1.468,2.965Zm-0.567-2.732,0.6,2.682,0.676-.1Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-204.672-97.591l-0.665-.453,0.816-.121Zm-0.547-.426,0.52,0.355,0.118-.449Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-201.271-96.771l-3.441-.84,0.147-.557,0.023,0.009,3.285,1.346Zm-3.387-.872,3.112,0.76-2.99-1.226Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-198.256-102.89l-1.865-3.161,1.676,0.056v0.02ZM-200.042-106l1.731,2.933-0.176-2.881Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.5-91.144l-0.834-2.52,0.549-2.962Zm-0.788-2.523,0.727,2.2-0.249-4.782Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-194.988-86.214l-1.807-1.307v-0.014l0.886-3.572Zm-1.757-1.325,1.693,1.224-0.859-4.585Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.552-91.277v-0.028l-0.243-4.673,0.047,0.091,2.072,4.01-0.025.008Zm-0.19-4.5,0.231,4.44,1.77-.567Z" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-197.346-114.633a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-197.776-116.882a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-196.909-117.027a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-193.365-116.1a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-196.07-114.527a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-207.957-107a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295V-107a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295V-107" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-203.468-101.579a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-205.634-100.894a0.306,0.306,0,0,1-.3.305,0.306,0.306,0,0,1-.3-0.305,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-198.916-98.393a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-199.447-92.408a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-201.7-86.369a0.21,0.21,0,0,1-.21.21h0a0.21,0.21,0,0,1-.209-0.21,0.21,0.21,0,0,1,.209-0.21,0.21,0.21,0,0,1,.21.21h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-201.388-87.6a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295V-87.6a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-201.609-91.584a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.459-96.834a0.306,0.306,0,0,1-.3.305,0.306,0.306,0,0,1-.3-0.305,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-196.886-94.6a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295V-94.6a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-194.746-86.287a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-193.341-85.063a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-191.052-84.742a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-180.382-84.587a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-188.709-88.971a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-190.42-91.84a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-200.6-86.064a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-194.009-98.465a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-212.933-111.647a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-215.871-109.009a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-220.55-111.9a0.306,0.306,0,0,1-.3.305,0.306,0.306,0,0,1-.3-0.305,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-200.4-116.354a0.306,0.306,0,0,1-.3.305,0.306,0.306,0,0,1-.3-0.305,0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-206.7-118.32a0.3,0.3,0,0,1-.295.305H-207a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3H-207a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-191.184-118.52a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.084-117.8a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-194.817-118.211a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-196.491-114.893a0.189,0.189,0,0,1-.189.189,0.19,0.19,0,0,1-.19-0.189,0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-201.609-109.576a0.189,0.189,0,0,1-.189.189,0.19,0.19,0,0,1-.19-0.189,0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-204.427-108.143a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-204.646-104.136a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-205.6-103.142a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-208.218-102.927a0.189,0.189,0,0,1-.189.189,0.19,0.19,0,0,1-.19-0.189,0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-206.273-102.275a0.189,0.189,0,0,1-.189.189,0.19,0.19,0,0,1-.19-0.189,0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-203.209-99.357a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-204.487-97.66a0.189,0.189,0,0,1-.189.189,0.19,0.19,0,0,1-.19-0.189,0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-204.369-98.127a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-200.155-100.935a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-195.245-99.758a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-196.441-100.157a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-190.8-96.579a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-189.947-96.39a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.531-95.788a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-194.029-97.355a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.19,0.19,0,0,1,.19-0.189,0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-196.49-95.41a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-198.047-94.566a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.19,0.19,0,0,1,.19-0.189,0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.174-84.492a0.189,0.189,0,0,1-.189.189,0.19,0.19,0,0,1-.19-0.189,0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-190.609-83.6a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-190.036-84.948a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-182.254-86.375a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-182.465-83.7a0.189,0.189,0,0,1-.189.189,0.19,0.19,0,0,1-.19-0.189,0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-181.4-83.4a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-184.531-81.5a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.19,0.19,0,0,1,.19-0.189,0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-191.8-83.6a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.341-91.279a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-191.2-82.546a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.609-96.46a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-200.565-98.655a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-199.086-93.827a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-198.65-86.681a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-194.43-95.857a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-201.137-103.591a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.19,0.19,0,0,1,.19-0.189,0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-199.163-108.237a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.19,0.19,0,0,1,.19-0.189,0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-199.917-108.284a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-194.12-114.568a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.19,0.19,0,0,1,.19-0.189,0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-199.009-112.339a0.189,0.189,0,0,1-.189.189,0.19,0.19,0,0,1-.19-0.189,0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-203.478-109.5a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-206.728-104.267a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.19,0.19,0,0,1,.19-0.189,0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-212.851-107.027a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-219.435-109.576a0.189,0.189,0,0,1-.189.189,0.19,0.19,0,0,1-.19-0.189,0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-203.254-106.795a0.259,0.259,0,0,1-.258.26h0a0.26,0.26,0,0,1-.26-0.26,0.261,0.261,0,0,1,.26-0.26,0.26,0.26,0,0,1,.26.26" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-205.054-101.086a0.26,0.26,0,0,1-.26.26,0.261,0.261,0,0,1-.26-0.26,0.26,0.26,0,0,1,.26-0.26,0.259,0.259,0,0,1,.26.258v0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-201.066-96.789a0.259,0.259,0,0,1-.258.26h0a0.26,0.26,0,0,1-.26-0.26,0.261,0.261,0,0,1,.26-0.26,0.26,0.26,0,0,1,.26.26" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-205-98.006a0.259,0.259,0,0,1-.258.26h0a0.26,0.26,0,0,1-.26-0.26,0.261,0.261,0,0,1,.26-0.26,0.26,0.26,0,0,1,.26.26" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-199.8-106.013a0.26,0.26,0,0,1-.26.26,0.261,0.261,0,0,1-.26-0.26,0.261,0.261,0,0,1,.26-0.26,0.26,0.26,0,0,1,.26.26" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-198.183-105.985a0.26,0.26,0,0,1-.26.26,0.261,0.261,0,0,1-.26-0.26,0.26,0.26,0,0,1,.26-0.26,0.259,0.259,0,0,1,.26.258v0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-201.066-98.974a0.259,0.259,0,0,1-.258.26h0a0.26,0.26,0,0,1-.26-0.26,0.261,0.261,0,0,1,.26-0.26,0.26,0.26,0,0,1,.26.26" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-198-102.965a0.259,0.259,0,0,1-.258.26h0a0.26,0.26,0,0,1-.26-0.26,0.261,0.261,0,0,1,.26-0.26,0.26,0.26,0,0,1,.26.26" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-198.327-101.51a0.26,0.26,0,0,1-.26.26,0.261,0.261,0,0,1-.26-0.26,0.261,0.261,0,0,1,.26-0.26,0.26,0.26,0,0,1,.26.26" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-194.055-100.008a0.259,0.259,0,0,1-.258.26h0a0.26,0.26,0,0,1-.26-0.26,0.261,0.261,0,0,1,.26-0.26,0.26,0.26,0,0,1,.26.26" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-191.178-98.647a0.26,0.26,0,0,1-.26.26,0.261,0.261,0,0,1-.26-0.26,0.261,0.261,0,0,1,.26-0.26,0.26,0.26,0,0,1,.26.26" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-210.217-109.4a0.259,0.259,0,0,1-.258.26h0a0.26,0.26,0,0,1-.26-0.26,0.261,0.261,0,0,1,.26-0.26,0.26,0.26,0,0,1,.26.26" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-206.4-105.275a0.26,0.26,0,0,1-.26.26,0.261,0.261,0,0,1-.26-0.26,0.261,0.261,0,0,1,.26-0.26,0.26,0.26,0,0,1,.26.26" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-213.609-103.621a0.259,0.259,0,0,1-.258.26h0a0.26,0.26,0,0,1-.26-0.26,0.261,0.261,0,0,1,.26-0.26,0.26,0.26,0,0,1,.26.26" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.9-115.827a0.189,0.189,0,0,1-.189.189,0.19,0.19,0,0,1-.19-0.189,0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-191.855-119.44a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.19,0.19,0,0,1,.19-0.189,0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-193.039-117.967a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.951-117.827a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.709-119.219a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.189,0.189,0,0,1,.189-0.189h0a0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.652-119.6a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.19-0.188h0a0.19,0.19,0,0,1,.19-0.189,0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-187.426-121.627a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-188.225-123.027a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-185.115-125.1a0.181,0.181,0,0,1-.181.181,0.181,0.181,0,0,1-.181-0.181,0.181,0.181,0,0,1,.181-0.181,0.181,0.181,0,0,1,.181.181" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-176.283-127.654a0.181,0.181,0,0,1-.181.181,0.181,0.181,0,0,1-.181-0.181,0.181,0.181,0,0,1,.181-0.181,0.181,0.181,0,0,1,.181.181" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-179.325-127.6a0.181,0.181,0,0,1-.181.181,0.181,0.181,0,0,1-.181-0.181,0.181,0.181,0,0,1,.181-0.181,0.181,0.181,0,0,1,.181.181" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-185.209-125.427a0.181,0.181,0,0,1-.181.181,0.181,0.181,0,0,1-.181-0.181,0.181,0.181,0,0,1,.181-0.181,0.181,0.181,0,0,1,.181.181" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-182-125.619a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-179.094-128.7a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-179.709-126.327a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-191.982-124.889a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-188.609-124.91a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-233.881-133.3a0.216,0.216,0,0,1-.216.216h0a0.217,0.217,0,0,1-.217-0.216,0.216,0.216,0,0,1,.216-0.216h0a0.216,0.216,0,0,1,.216.216h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-235.887-133.034a0.216,0.216,0,0,1-.216.216h0a0.216,0.216,0,0,1-.217-0.215h0a0.217,0.217,0,0,1,.217-0.216,0.216,0.216,0,0,1,.216.216h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-232.933-133.381a0.216,0.216,0,0,1-.216.216h0a0.216,0.216,0,0,1-.217-0.215h0a0.217,0.217,0,0,1,.217-0.216,0.216,0.216,0,0,1,.216.216h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-231.754-132.077a0.216,0.216,0,0,1-.216.216h0a0.217,0.217,0,0,1-.217-0.216,0.216,0.216,0,0,1,.216-0.216h0a0.216,0.216,0,0,1,.216.216h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-216.716-132.082a0.174,0.174,0,0,1-.174.174,0.174,0.174,0,0,1-.173-0.174,0.174,0.174,0,0,1,.173-0.174,0.174,0.174,0,0,1,.174.174" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-215.146-130.427a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-212.428-130.9a0.158,0.158,0,0,1-.158.158h0a0.157,0.157,0,0,1-.157-0.157h0a0.157,0.157,0,0,1,.156-0.158h0a0.158,0.158,0,0,1,.158.158h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-207.263-132.415a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-202.163-131.927a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-216.492-128.8a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-240.535-122.907a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-239.736-118.494a0.306,0.306,0,0,1-.3.305,0.306,0.306,0,0,1-.3-0.305,0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-233.345-117.046a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-236.478-118.785a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-239.809-114.98a0.257,0.257,0,0,1-.257.257h0a0.257,0.257,0,0,1-.257-0.257h0a0.257,0.257,0,0,1,.257-0.257h0a0.257,0.257,0,0,1,.257.257h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-243.984-104.3a0.257,0.257,0,0,1-.257.257h0a0.257,0.257,0,0,1-.257-0.257h0a0.257,0.257,0,0,1,.257-0.257h0a0.257,0.257,0,0,1,.257.257h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-237.8-102.942a0.257,0.257,0,0,1-.257.257h0a0.257,0.257,0,0,1-.257-0.257h0a0.257,0.257,0,0,1,.257-0.257h0a0.257,0.257,0,0,1,.257.257h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-236.421-99.8a0.257,0.257,0,0,1-.257.257h0a0.257,0.257,0,0,1-.257-0.257h0a0.257,0.257,0,0,1,.257-0.257h0a0.257,0.257,0,0,1,.257.257h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-235.116-97.834a0.257,0.257,0,0,1-.257.257h0a0.257,0.257,0,0,1-.257-0.257h0a0.257,0.257,0,0,1,.257-0.257h0a0.257,0.257,0,0,1,.257.257h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-234.709-91.061a0.257,0.257,0,0,1-.257.257h0a0.257,0.257,0,0,1-.257-0.257h0a0.257,0.257,0,0,1,.257-0.257h0a0.257,0.257,0,0,1,.257.257h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-234.323-88.989a0.257,0.257,0,0,1-.257.257h0a0.257,0.257,0,0,1-.257-0.257h0a0.257,0.257,0,0,1,.257-0.257h0a0.257,0.257,0,0,1,.257.257h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-229.825-87.042a0.257,0.257,0,0,1-.257.257h0a0.257,0.257,0,0,1-.257-0.257h0a0.257,0.257,0,0,1,.257-0.257h0a0.257,0.257,0,0,1,.257.257h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-242.652-113.272a0.257,0.257,0,0,1-.257.257h0a0.257,0.257,0,0,1-.257-0.257h0a0.257,0.257,0,0,1,.257-0.255,0.257,0.257,0,0,1,.257.257h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-241.448-114.933a0.182,0.182,0,0,1-.182.182h0a0.182,0.182,0,0,1-.183-0.181h0a0.182,0.182,0,0,1,.182-0.182h0a0.182,0.182,0,0,1,.182.182h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-245.084-108.876a0.182,0.182,0,0,1-.182.182h0a0.181,0.181,0,0,1-.182-0.18v0a0.181,0.181,0,0,1,.18-0.182h0a0.182,0.182,0,0,1,.182.182h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-245.154-106.5a0.182,0.182,0,0,1-.182.182h0a0.181,0.181,0,0,1-.182-0.18v0a0.181,0.181,0,0,1,.18-0.182h0a0.182,0.182,0,0,1,.182.182h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-238.942-102.872a0.182,0.182,0,0,1-.182.182h0a0.181,0.181,0,0,1-.182-0.18v0a0.181,0.181,0,0,1,.18-0.182h0a0.182,0.182,0,0,1,.182.182h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-237.638-102.042a0.181,0.181,0,0,1-.18.182h0a0.181,0.181,0,0,1-.182-0.18v0a0.181,0.181,0,0,1,.18-0.182h0a0.181,0.181,0,0,1,.182.18v0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-236.016-101.274a0.182,0.182,0,0,1-.182.182h0a0.182,0.182,0,0,1-.183-0.181h0a0.182,0.182,0,0,1,.182-0.182h0a0.182,0.182,0,0,1,.182.182h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-234.877-95.427a0.182,0.182,0,0,1-.182.182h0a0.182,0.182,0,0,1-.183-0.181h0a0.182,0.182,0,0,1,.182-0.182h0a0.182,0.182,0,0,1,.182.182h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.972-92.306a0.182,0.182,0,0,1-.182.182h0a0.181,0.181,0,0,1-.182-0.18v0a0.181,0.181,0,0,1,.18-0.182h0a0.182,0.182,0,0,1,.182.182h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-225.266-91.844a0.182,0.182,0,0,1-.182.182h0a0.182,0.182,0,0,1-.183-0.181h0a0.182,0.182,0,0,1,.182-0.182h0a0.182,0.182,0,0,1,.182.182h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-235.636-92.72a0.182,0.182,0,0,1-.182.182h0a0.181,0.181,0,0,1-.182-0.18v0a0.181,0.181,0,0,1,.18-0.182h0a0.182,0.182,0,0,1,.182.182h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-233.423-86.793a0.182,0.182,0,0,1-.182.182h0a0.181,0.181,0,0,1-.182-0.18v0a0.181,0.181,0,0,1,.18-0.182h0a0.182,0.182,0,0,1,.182.182h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-231.727-117.3a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-232.589-112.794a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.354-115.571a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-217.245-110.963a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-220.565-111.107a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.392-112.074a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-224.647-110.173a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-222.337-105.211a0.222,0.222,0,0,1-.222.222h0a0.222,0.222,0,0,1-.223-0.221h0a0.222,0.222,0,0,1,.222-0.222h0a0.222,0.222,0,0,1,.222.222h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-224.916-105.615a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-223.8-101.793a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-225.933-96.049a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-222.909-95.092a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-228.409-90.708a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-224.209-93.785a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-224.271-89.845a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-232.809-85.979a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-235.324-93.716a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-230.13-98.541a0.306,0.306,0,0,1-.3.305,0.306,0.306,0,0,1-.3-0.305,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.795-107.864a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.766-110.081a0.306,0.306,0,0,1-.3.305,0.306,0.306,0,0,1-.3-0.305,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-223.4-111.769a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-232.99-117.84a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-237.98-115.494a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-244.023-111.241a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-244.447-107.455a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-232.393-113.827a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-234.225-113.193a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-236.693-109.143a0.3,0.3,0,0,1-.295.305H-237a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-228.8-113.113a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-242.148-122.907a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-240.945-121.075a0.246,0.246,0,0,1-.246.246h0a0.247,0.247,0,0,1-.247-0.246,0.247,0.247,0,0,1,.247-0.246,0.246,0.246,0,0,1,.246.246h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-241.467-121.909a0.246,0.246,0,0,1-.246.246h0a0.247,0.247,0,0,1-.247-0.246,0.247,0.247,0,0,1,.247-0.246,0.246,0.246,0,0,1,.246.246h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-240.509-120.506a0.246,0.246,0,0,1-.246.246h0a0.246,0.246,0,0,1-.246-0.246h0a0.246,0.246,0,0,1,.246-0.246h0a0.246,0.246,0,0,1,.246.246h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-241.416-115.413a0.246,0.246,0,0,1-.246.246h0a0.247,0.247,0,0,1-.247-0.246,0.247,0.247,0,0,1,.247-0.246,0.246,0.246,0,0,1,.246.246h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-242.674-121.727a0.246,0.246,0,0,1-.246.246h0a0.247,0.247,0,0,1-.247-0.246,0.247,0.247,0,0,1,.247-0.246,0.246,0.246,0,0,1,.246.246h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-239.694-123.327a0.246,0.246,0,0,1-.246.246h0a0.247,0.247,0,0,1-.247-0.246,0.247,0.247,0,0,1,.247-0.246,0.246,0.246,0,0,1,.246.246h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-237.56-125.989a0.246,0.246,0,0,1-.246.246h0a0.247,0.247,0,0,1-.247-0.246,0.247,0.247,0,0,1,.247-0.246,0.246,0.246,0,0,1,.246.246h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-232.839-126.591a0.246,0.246,0,0,1-.246.246h0a0.247,0.247,0,0,1-.247-0.246,0.247,0.247,0,0,1,.247-0.246,0.246,0.246,0,0,1,.246.246h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-225.887-127.89a0.246,0.246,0,0,1-.246.246h0a0.246,0.246,0,0,1-.246-0.246h0a0.246,0.246,0,0,1,.246-0.246h0a0.246,0.246,0,0,1,.246.246h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-213.627-119.173a0.246,0.246,0,0,1-.246.246h0a0.246,0.246,0,0,1-.246-0.246h0a0.246,0.246,0,0,1,.246-0.246h0a0.246,0.246,0,0,1,.246.246h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-219.314-129.327a0.246,0.246,0,0,1-.246.246h0a0.246,0.246,0,0,1-.246-0.246h0a0.246,0.246,0,0,1,.246-0.246h0a0.246,0.246,0,0,1,.246.246h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-214.28-129.986a0.246,0.246,0,0,1-.246.246h0a0.247,0.247,0,0,1-.247-0.246,0.247,0.247,0,0,1,.247-0.246,0.246,0.246,0,0,1,.246.246h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-208.894-132.927a0.246,0.246,0,0,1-.246.246h0a0.247,0.247,0,0,1-.247-0.246,0.247,0.247,0,0,1,.247-0.246,0.246,0.246,0,0,1,.246.246h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-208.609-133.674a0.246,0.246,0,0,1-.246.246h0a0.246,0.246,0,0,1-.246-0.246h0a0.246,0.246,0,0,1,.246-0.246h0a0.246,0.246,0,0,1,.246.246h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-206.6-133.541a0.132,0.132,0,0,1-.132.132h0a0.133,0.133,0,0,1-.133-0.132,0.133,0.133,0,0,1,.133-0.132,0.132,0.132,0,0,1,.132.132h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-204.741-132.577a0.246,0.246,0,0,1-.246.246h0a0.247,0.247,0,0,1-.247-0.246,0.247,0.247,0,0,1,.247-0.246,0.246,0.246,0,0,1,.246.246h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-197.764-130.751a0.158,0.158,0,0,1-.158.158h0a0.158,0.158,0,0,1-.158-0.158h0a0.158,0.158,0,0,1,.158-0.158h0a0.158,0.158,0,0,1,.158.158h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-221.665-130.9a0.246,0.246,0,0,1-.246.246h0a0.247,0.247,0,0,1-.247-0.246,0.247,0.247,0,0,1,.247-0.246,0.246,0.246,0,0,1,.246.246h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-217.035-130.275a0.16,0.16,0,0,1-.16.16h0a0.16,0.16,0,0,1-.16-0.16h0a0.16,0.16,0,0,1,.16-0.16h0a0.16,0.16,0,0,1,.16.16h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-221.888-129.408a0.246,0.246,0,0,1-.246.246h0a0.247,0.247,0,0,1-.247-0.246,0.247,0.247,0,0,1,.247-0.246,0.246,0.246,0,0,1,.246.246h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-235.471-122.668a0.246,0.246,0,0,1-.246.246h0a0.246,0.246,0,0,1-.246-0.246h0a0.246,0.246,0,0,1,.246-0.246h0a0.246,0.246,0,0,1,.246.246h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-234.131-123.709a0.246,0.246,0,0,1-.246.246h0a0.247,0.247,0,0,1-.247-0.246,0.247,0.247,0,0,1,.247-0.246,0.246,0.246,0,0,1,.246.246h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-232.619-122.872a0.246,0.246,0,0,1-.246.246h0a0.247,0.247,0,0,1-.247-0.246,0.247,0.247,0,0,1,.247-0.246,0.246,0.246,0,0,1,.246.246h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-231.517-124.827a0.246,0.246,0,0,1-.246.246h0a0.246,0.246,0,0,1-.246-0.246h0a0.246,0.246,0,0,1,.246-0.246h0a0.246,0.246,0,0,1,.246.246h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-241.836-123.187a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-241.192-123.006a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-239.758-119.427a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-234-116.235a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-235.751-114.961a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-236.315-115.737a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-236.047-114.157a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-232.624-116.235a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-230.729-117.265a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.967-113.355a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-231.6-115.447a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-230.384-115.727a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-228.263-111.769a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-226.093-109.285a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-226.82-105.755a0.239,0.239,0,0,1-.239.239,0.239,0.239,0,0,1-.239-0.239,0.239,0.239,0,0,1,.239-0.239,0.239,0.239,0,0,1,.239.239" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-221.921-107.217a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-225.034-104.638a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-226.618-97.912a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-225.909-94.253a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-223.209-92.651a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-223.672-90.94a0.162,0.162,0,0,1-.162.162h0A0.162,0.162,0,0,1-224-90.939h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-225.1-90.4a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-230.319-113.272a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-239.758-116.018a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-242.328-115.856a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-238.866-121.933a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-236.331-124a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-235.146-123.168a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-236.732-124.327a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-235.588-119.049a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-241.192-124.778a0.162,0.162,0,0,1-.162.162h0a0.162,0.162,0,0,1-.163-0.161h0a0.162,0.162,0,0,1,.162-0.162h0a0.162,0.162,0,0,1,.162.162h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-235.14-127.988a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-232.009-125.591a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-230.245-129.557a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-256.916-120.059a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.683-120.081a0.19,0.19,0,0,1-.19.19h0a0.191,0.191,0,0,1-.191-0.19,0.191,0.191,0,0,1,.191-0.19,0.19,0.19,0,0,1,.19.19h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.237-119.29a0.19,0.19,0,0,1-.19.19h0a0.191,0.191,0,0,1-.191-0.19,0.191,0.191,0,0,1,.191-0.19,0.19,0.19,0,0,1,.19.19h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.785-120.462a0.19,0.19,0,0,1-.19.19h0a0.19,0.19,0,0,1-.19-0.19h0a0.19,0.19,0,0,1,.19-0.19h0a0.19,0.19,0,0,1,.19.19h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.179-122.981a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-294.309-128.69a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-294.789-127.277a0.248,0.248,0,0,1-.247.247,0.247,0.247,0,0,1-.247-0.247h0a0.246,0.246,0,0,1,.245-0.247h0a0.247,0.247,0,0,1,.247.247h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-290.267-124.161a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.3v-0.005a0.3,0.3,0,0,1,.3-0.3h0a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-294.121-125.551a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-280.3-130.888a0.224,0.224,0,0,1-.224.224,0.224,0.224,0,0,1-.224-0.224,0.224,0.224,0,0,1,.224-0.224,0.224,0.224,0,0,1,.224.224" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-280.694-129.991a0.3,0.3,0,0,1-.295.3H-281a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3H-281a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-283.379-128.839a0.239,0.239,0,0,1-.239.239,0.239,0.239,0,0,1-.239-0.239,0.239,0.239,0,0,1,.239-0.239,0.239,0.239,0,0,1,.239.239" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-281.609-129.144a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-277.529-126.239a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-274.162-130.679a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-278.581-130.888a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-275.577-128.672a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-277.734-129.614a0.4,0.4,0,0,1-.4.4,0.4,0.4,0,0,1-.4-0.4,0.4,0.4,0,0,1,.4-0.4,0.4,0.4,0,0,1,.4.4" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-278.633-123.308a0.4,0.4,0,0,1-.4.4,0.4,0.4,0,0,1-.4-0.4,0.4,0.4,0,0,1,.4-0.4,0.4,0.4,0,0,1,.4.4" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-280.168-118.327a0.4,0.4,0,0,1-.4.4,0.4,0.4,0,0,1-.4-0.4,0.4,0.4,0,0,1,.4-0.4,0.4,0.4,0,0,1,.4.4" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-277.2-113.593a0.4,0.4,0,0,1-.4.4,0.4,0.4,0,0,1-.4-0.4,0.4,0.4,0,0,1,.4-0.4,0.4,0.4,0,0,1,.4.4" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-271.216-111.991a0.4,0.4,0,0,1-.4.4,0.4,0.4,0,0,1-.4-0.4,0.4,0.4,0,0,1,.4-0.4,0.4,0.4,0,0,1,.4.4" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-268.385-105.763a0.4,0.4,0,0,1-.4.4,0.4,0.4,0,0,1-.4-0.4,0.4,0.4,0,0,1,.4-0.4,0.4,0.4,0,0,1,.4.4" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-276.509-119.507a0.4,0.4,0,0,1-.4.4,0.4,0.4,0,0,1-.4-0.4,0.4,0.4,0,0,1,.4-0.4,0.4,0.4,0,0,1,.4.4" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-272.074-130.827a0.209,0.209,0,0,1-.209.209,0.209,0.209,0,0,1-.208-0.209,0.209,0.209,0,0,1,.208-0.209,0.209,0.209,0,0,1,.209.209" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-272.04-129.653a0.223,0.223,0,0,1-.223.223h0a0.223,0.223,0,0,1-.224-0.222h0a0.223,0.223,0,0,1,.223-0.223h0a0.223,0.223,0,0,1,.223.223h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-284.619-128.533a0.306,0.306,0,0,1-.3.3,0.306,0.306,0,0,1-.3-0.3,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-288.247-129.062a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.3v-0.005a0.3,0.3,0,0,1,.3-0.3h0a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-291.3-124.488a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-294.121-122.406a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-288.609-125.353a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.3v-0.005a0.3,0.3,0,0,1,.3-0.3h0a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-282.593-122.838a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.188-0.189,0.189,0.189,0,0,1,.188-0.189,0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-281.491-121.48a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-280.068-116.18a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-277.651-112.712a0.252,0.252,0,0,1-.252.252h0a0.252,0.252,0,0,1-.252-0.252,0.253,0.253,0,0,1,.252-0.253,0.253,0.253,0,0,1,.253.253h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-278.948-114.556a0.252,0.252,0,0,1-.252.252h0a0.252,0.252,0,0,1-.252-0.252,0.253,0.253,0,0,1,.252-0.253,0.253,0.253,0,0,1,.253.253h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-274.922-110.552a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-271.309-109.357a0.215,0.215,0,0,1-.215.215,0.216,0.216,0,0,1-.216-0.215,0.216,0.216,0,0,1,.216-0.215,0.215,0.215,0,0,1,.215.215" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.709-108a0.189,0.189,0,0,1-.189.189,0.189,0.189,0,0,1-.188-0.189,0.189,0.189,0,0,1,.188-0.189,0.189,0.189,0,0,1,.189.189" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-270.74-106.927a0.306,0.306,0,0,1-.3.305,0.306,0.306,0,0,1-.3-0.305,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-272.554-107.289a0.3,0.3,0,0,1-.295.305h-0.01a0.306,0.306,0,0,1-.3-0.305,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-274.062-108.1a0.193,0.193,0,0,1-.193.193,0.193,0.193,0,0,1-.194-0.192h0a0.193,0.193,0,0,1,.193-0.193h0a0.193,0.193,0,0,1,.193.193" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.72-133.947a0.223,0.223,0,0,1-.223.223h0a0.223,0.223,0,0,1-.223-0.223h0a0.223,0.223,0,0,1,.223-0.223h0a0.223,0.223,0,0,1,.223.223h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-270.925-133.216a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-270.9-133.579a0.177,0.177,0,0,1-.177.177h0a0.177,0.177,0,0,1-.177-0.177h0a0.177,0.177,0,0,1,.177-0.177h0a0.177,0.177,0,0,1,.177.177h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.473-134.253a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.015-134.087a0.237,0.237,0,0,1-.237.237,0.237,0.237,0,0,1-.236-0.237,0.237,0.237,0,0,1,.236-0.237,0.237,0.237,0,0,1,.237.237" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.424-132.41a0.2,0.2,0,0,1-.2.2,0.2,0.2,0,0,1-.2-0.2,0.2,0.2,0,0,1,.2-0.2,0.2,0.2,0,0,1,.2.2" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.277-132.4a0.237,0.237,0,0,1-.236.237,0.237,0.237,0,0,1-.237-0.237,0.236,0.236,0,0,1,.241-0.231h0a0.236,0.236,0,0,1,.236.236" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.209-130.078a0.237,0.237,0,0,1-.236.237,0.237,0.237,0,0,1-.237-0.237,0.237,0.237,0,0,1,.237-0.237,0.237,0.237,0,0,1,.236.237" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.4-134.232a0.237,0.237,0,0,1-.236.237,0.237,0.237,0,0,1-.237-0.237,0.236,0.236,0,0,1,.236-0.236h0a0.236,0.236,0,0,1,.236.236" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-255.609-134.2a0.236,0.236,0,0,1-.236.236h0a0.236,0.236,0,0,1-.236-0.236,0.237,0.237,0,0,1,.236-0.237,0.237,0.237,0,0,1,.237.237" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.393-131.6a0.259,0.259,0,0,1-.259.259h0a0.259,0.259,0,0,1-.258-0.259,0.259,0.259,0,0,1,.258-0.259,0.259,0.259,0,0,1,.259.259h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.249-132.627a0.259,0.259,0,0,1-.259.259h0a0.259,0.259,0,0,1-.258-0.259,0.259,0.259,0,0,1,.258-0.259,0.259,0.259,0,0,1,.259.259h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.38-134.469a0.157,0.157,0,0,1-.157.157,0.157,0.157,0,0,1-.157-0.157,0.157,0.157,0,0,1,.157-0.157,0.157,0.157,0,0,1,.157.157" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.528-133.143a0.259,0.259,0,0,1-.259.259h0a0.259,0.259,0,0,1-.258-0.259,0.259,0.259,0,0,1,.258-0.259,0.259,0.259,0,0,1,.259.259h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.066-133.143a0.259,0.259,0,0,1-.259.259h0a0.259,0.259,0,0,1-.258-0.259,0.259,0.259,0,0,1,.258-0.259,0.259,0.259,0,0,1,.259.259h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.168-131.769a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-251.92-132.985a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-252.066-131.727a0.306,0.306,0,0,1-.3.3,0.306,0.306,0,0,1-.3-0.3,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-249.028-133.482a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-245.954-134.076a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-245.809-132.895a0.264,0.264,0,0,1-.264.264h0a0.264,0.264,0,0,1-.263-0.264,0.264,0.264,0,0,1,.263-0.264,0.264,0.264,0,0,1,.264.264h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-245.774-130.989a0.264,0.264,0,0,1-.264.264h0a0.264,0.264,0,0,1-.263-0.264,0.264,0.264,0,0,1,.263-0.264,0.264,0.264,0,0,1,.264.264h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-247.714-130.333a0.264,0.264,0,0,1-.264.264h0a0.264,0.264,0,0,1-.263-0.264,0.264,0.264,0,0,1,.263-0.264,0.264,0.264,0,0,1,.264.264h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-246.2-132.475a0.209,0.209,0,0,1-.209.209,0.209,0.209,0,0,1-.208-0.209,0.209,0.209,0,0,1,.208-0.209,0.209,0.209,0,0,1,.209.209" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-246.609-130.542a0.209,0.209,0,0,1-.209.209,0.209,0.209,0,0,1-.208-0.209,0.209,0.209,0,0,1,.208-0.209,0.209,0.209,0,0,1,.209.209" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-247.592-130.751a0.209,0.209,0,0,1-.209.209,0.209,0.209,0,0,1-.208-0.209,0.209,0.209,0,0,1,.208-0.209,0.209,0.209,0,0,1,.209.209" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-246.354-129.693a0.21,0.21,0,0,1-.209.209,0.209,0.209,0,0,1-.208-0.209,0.209,0.209,0,0,1,.208-0.209,0.21,0.21,0,0,1,.209.209" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-246.63-133.692a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-249.638-128.362a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-252.487-127.048a0.234,0.234,0,0,1-.234.234,0.235,0.235,0,0,1-.235-0.234,0.234,0.234,0,0,1,.234-0.234h0a0.234,0.234,0,0,1,.234.234" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.548-125.994a0.234,0.234,0,0,1-.234.234,0.235,0.235,0,0,1-.235-0.234,0.234,0.234,0,0,1,.234-0.234h0a0.234,0.234,0,0,1,.234.234" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-255.088-125.311a0.234,0.234,0,0,1-.234.234,0.234,0.234,0,0,1-.235-0.233h0a0.235,0.235,0,0,1,.235-0.234,0.234,0.234,0,0,1,.234.234" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-253.722-124.94a0.234,0.234,0,0,1-.234.234,0.234,0.234,0,0,1-.235-0.233h0a0.235,0.235,0,0,1,.235-0.234,0.234,0.234,0,0,1,.234.234" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-256.381-126.722a0.234,0.234,0,0,1-.234.234,0.234,0.234,0,0,1-.235-0.233h0a0.235,0.235,0,0,1,.235-0.234,0.234,0.234,0,0,1,.234.234" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-256.981-127.666a0.234,0.234,0,0,1-.234.234,0.235,0.235,0,0,1-.235-0.234,0.234,0.234,0,0,1,.234-0.234h0a0.234,0.234,0,0,1,.234.234" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-255.666-129.027a0.306,0.306,0,0,1-.3.3,0.306,0.306,0,0,1-.3-0.3,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-256.675-128.527a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-242.925-133.9a0.416,0.416,0,0,1-.416.416h0a0.417,0.417,0,0,1-.417-0.416,0.417,0.417,0,0,1,.417-0.416,0.416,0.416,0,0,1,.416.416h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-256.4-133.927a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.588-133.476a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-246.935-129.053a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-251.07-127.473a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-255.521-128.2a0.2,0.2,0,0,1-.2.2,0.2,0.2,0,0,1-.2-0.2,0.2,0.2,0,0,1,.2-0.2,0.2,0.2,0,0,1,.2.2" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-256.075-129.749a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-245.341-127.5a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-246.341-126.722a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-244.768-126.654a0.226,0.226,0,0,1-.225.226,0.226,0.226,0,0,1-.226-0.226,0.226,0.226,0,0,1,.226-0.225,0.225,0.225,0,0,1,.225.225h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-247.185-127.392a0.174,0.174,0,0,1-.174.174,0.174,0.174,0,0,1-.174-0.174,0.174,0.174,0,0,1,.174-0.174,0.174,0.174,0,0,1,.174.174" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-243.741-127.193a0.174,0.174,0,0,1-.174.174,0.174,0.174,0,0,1-.174-0.174,0.174,0.174,0,0,1,.174-0.174,0.174,0.174,0,0,1,.174.174" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-245.571-126.427a0.174,0.174,0,0,1-.174.174,0.174,0.174,0,0,1-.174-0.174,0.174,0.174,0,0,1,.174-0.174,0.174,0.174,0,0,1,.174.174" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-246.563-127.581a0.174,0.174,0,0,1-.174.174,0.174,0.174,0,0,1-.174-0.174,0.174,0.174,0,0,1,.174-0.174,0.174,0.174,0,0,1,.174.174" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-244.482-127.678a0.174,0.174,0,0,1-.174.174,0.174,0.174,0,0,1-.174-0.174,0.174,0.174,0,0,1,.174-0.174,0.174,0.174,0,0,1,.174.174" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-244.981-127.407a0.174,0.174,0,0,1-.174.174,0.174,0.174,0,0,1-.174-0.174,0.174,0.174,0,0,1,.174-0.174,0.174,0.174,0,0,1,.174.174" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-247.151-127.138a0.12,0.12,0,0,1-.12.12,0.12,0.12,0,0,1-.12-0.12,0.12,0.12,0,0,1,.12-0.12,0.12,0.12,0,0,1,.12.12" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-246.221-127.169a0.12,0.12,0,0,1-.12.12,0.12,0.12,0,0,1-.12-0.12,0.12,0.12,0,0,1,.12-0.12,0.12,0.12,0,0,1,.12.12" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.685-132.012a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-268.481-131.9a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-270.477-131.127a0.306,0.306,0,0,1-.3.3,0.306,0.306,0,0,1-.3-0.3,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-272.5-129.088a0.471,0.471,0,0,1-.471.471,0.471,0.471,0,0,1-.471-0.471,0.471,0.471,0,0,1,.471-0.471,0.471,0.471,0,0,1,.471.471" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-270.232-125.749a0.471,0.471,0,0,1-.471.471,0.471,0.471,0,0,1-.472-0.47h0a0.471,0.471,0,0,1,.471-0.471h0a0.471,0.471,0,0,1,.471.471" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-279.03-128.875a0.471,0.471,0,0,1-.471.471,0.471,0.471,0,0,1-.471-0.471,0.471,0.471,0,0,1,.471-0.471,0.471,0.471,0,0,1,.471.471" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-277.894-127.308a0.471,0.471,0,0,1-.471.471,0.471,0.471,0,0,1-.472-0.47h0a0.471,0.471,0,0,1,.471-0.471h0a0.471,0.471,0,0,1,.471.471" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-282.277-126.912a0.471,0.471,0,0,1-.471.471,0.471,0.471,0,0,1-.471-0.471,0.471,0.471,0,0,1,.471-0.471,0.471,0.471,0,0,1,.471.471" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-285.935-125a0.471,0.471,0,0,1-.471.471,0.471,0.471,0,0,1-.471-0.471,0.471,0.471,0,0,1,.471-0.471,0.471,0.471,0,0,1,.471.471" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-292.839-127.657a0.471,0.471,0,0,1-.471.471,0.471,0.471,0,0,1-.472-0.47h0a0.471,0.471,0,0,1,.471-0.471h0a0.471,0.471,0,0,1,.471.471" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-292.587-124.914a0.471,0.471,0,0,1-.471.471,0.471,0.471,0,0,1-.472-0.47h0a0.471,0.471,0,0,1,.471-0.471h0a0.471,0.471,0,0,1,.471.471" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266-130.888a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.07-128.759a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266.277-128.921a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-267.8-130.72a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.751-127.85a0.306,0.306,0,0,1-.3.3,0.306,0.306,0,0,1-.3-0.3,0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.3-124.772a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-264.328-126.722a0.221,0.221,0,0,1-.22.222h0a0.222,0.222,0,0,1-.223-0.221h0a0.222,0.222,0,0,1,.222-0.222h0a0.221,0.221,0,0,1,.222.22v0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-264.449-130.207a0.222,0.222,0,0,1-.222.222h0a0.222,0.222,0,0,1-.223-0.221h0a0.222,0.222,0,0,1,.222-0.222h0a0.222,0.222,0,0,1,.222.222h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.6-126.381a0.222,0.222,0,0,1-.222.222h0a0.222,0.222,0,0,1-.223-0.221h0a0.222,0.222,0,0,1,.222-0.222h0a0.222,0.222,0,0,1,.222.222h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266.864-126.668a0.222,0.222,0,0,1-.222.222h0a0.222,0.222,0,0,1-.223-0.221h0a0.222,0.222,0,0,1,.222-0.222h0a0.222,0.222,0,0,1,.222.222h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.409-129.289a0.222,0.222,0,0,1-.222.222h0a0.222,0.222,0,0,1-.223-0.221h0a0.222,0.222,0,0,1,.222-0.222h0a0.222,0.222,0,0,1,.222.222h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.549-128.5a0.222,0.222,0,0,1-.222.222h0a0.222,0.222,0,0,1-.223-0.221h0a0.222,0.222,0,0,1,.222-0.222h0a0.222,0.222,0,0,1,.222.222h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.758-126.2a0.222,0.222,0,0,1-.222.222h0a0.222,0.222,0,0,1-.223-0.221h0a0.222,0.222,0,0,1,.222-0.222h0a0.222,0.222,0,0,1,.222.222h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.825-125.3a0.222,0.222,0,0,1-.222.222h0a0.222,0.222,0,0,1-.223-0.221h0a0.222,0.222,0,0,1,.222-0.222h0a0.222,0.222,0,0,1,.222.222h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.058-121.027a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261-119.043a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.295-114.163a0.3,0.3,0,0,1-.295.305h-0.01a0.306,0.306,0,0,1-.3-0.305,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.937-117.3a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-264.868-119.806a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.88-110.635a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.189-108.893a0.137,0.137,0,0,1-.137.137,0.137,0.137,0,0,1-.137-0.137,0.137,0.137,0,0,1,.137-0.137,0.137,0.137,0,0,1,.137.137" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-267.8-106.684a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-271.067-108.4a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.535-105.874a0.217,0.217,0,0,1-.217.217,0.217,0.217,0,0,1-.216-0.217,0.217,0.217,0,0,1,.216-0.217,0.217,0.217,0,0,1,.217.217" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266.73-106.313a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-274.009-109.5a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-275.589-109.592a0.214,0.214,0,0,1-.214.214h0a0.213,0.213,0,0,1-.213-0.213h0a0.213,0.213,0,0,1,.212-0.214h0a0.214,0.214,0,0,1,.214.214h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-267.534-104.527a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-264.652-103.143a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.921-99.644a0.183,0.183,0,0,1-.183.183,0.182,0.182,0,0,1-.182-0.182h0a0.182,0.182,0,0,1,.181-0.183h0a0.183,0.183,0,0,1,.183.183" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-264.452-96.748a0.183,0.183,0,0,1-.183.183,0.182,0.182,0,0,1-.182-0.182h0a0.182,0.182,0,0,1,.181-0.183h0a0.183,0.183,0,0,1,.183.183" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.373-93.714a0.183,0.183,0,0,1-.183.183,0.182,0.182,0,0,1-.182-0.182h0a0.182,0.182,0,0,1,.181-0.183h0a0.183,0.183,0,0,1,.183.183" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266.039-103.283a0.183,0.183,0,0,1-.183.183,0.182,0.182,0,0,1-.182-0.182h0a0.182,0.182,0,0,1,.181-0.183h0a0.183,0.183,0,0,1,.183.183" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266.242-103.915a0.183,0.183,0,0,1-.183.183,0.183,0.183,0,0,1-.182-0.183,0.183,0.183,0,0,1,.182-0.183,0.183,0.183,0,0,1,.183.183" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.348-104.127a0.183,0.183,0,0,1-.183.183,0.182,0.182,0,0,1-.182-0.182h0a0.182,0.182,0,0,1,.181-0.183h0a0.183,0.183,0,0,1,.183.183" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.245-104.527a0.183,0.183,0,0,1-.183.183,0.183,0.183,0,0,1-.181-0.183,0.183,0.183,0,0,1,.182-0.183,0.183,0.183,0,0,1,.183.183" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-267.191-104.882a0.183,0.183,0,0,1-.183.183,0.182,0.182,0,0,1-.182-0.182h0a0.182,0.182,0,0,1,.181-0.183h0a0.183,0.183,0,0,1,.183.183" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-264.109-104.962a0.183,0.183,0,0,1-.183.183,0.182,0.182,0,0,1-.182-0.182h0a0.182,0.182,0,0,1,.181-0.183h0a0.183,0.183,0,0,1,.183.183" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.3-105.515a0.183,0.183,0,0,1-.183.183,0.182,0.182,0,0,1-.182-0.182h0a0.182,0.182,0,0,1,.181-0.183h0a0.183,0.183,0,0,1,.183.183" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.825-105.065a0.183,0.183,0,0,1-.183.183,0.183,0.183,0,0,1-.182-0.183,0.183,0.183,0,0,1,.182-0.183,0.183,0.183,0,0,1,.183.183" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.366-102.207a0.183,0.183,0,0,1-.183.183,0.182,0.182,0,0,1-.182-0.182h0a0.182,0.182,0,0,1,.181-0.183h0a0.183,0.183,0,0,1,.183.183" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.5-102.742a0.183,0.183,0,0,1-.183.183,0.182,0.182,0,0,1-.182-0.182h0a0.182,0.182,0,0,1,.181-0.183h0a0.183,0.183,0,0,1,.183.183" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.459-99.916a0.183,0.183,0,0,1-.183.183,0.182,0.182,0,0,1-.182-0.182h0a0.182,0.182,0,0,1,.181-0.183h0a0.183,0.183,0,0,1,.183.183" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-253.932-99.354a0.183,0.183,0,0,1-.183.183,0.182,0.182,0,0,1-.182-0.182h0a0.182,0.182,0,0,1,.181-0.183h0a0.183,0.183,0,0,1,.183.183" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-250.824-98.327a0.183,0.183,0,0,1-.183.183,0.182,0.182,0,0,1-.182-0.182h0a0.182,0.182,0,0,1,.181-0.183h0a0.183,0.183,0,0,1,.183.183" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-251.949-95.315a0.183,0.183,0,0,1-.183.183,0.182,0.182,0,0,1-.182-0.182h0a0.182,0.182,0,0,1,.181-0.183h0a0.183,0.183,0,0,1,.183.183" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-264.554-103.648a0.183,0.183,0,0,1-.183.183,0.182,0.182,0,0,1-.182-0.182h0a0.182,0.182,0,0,1,.181-0.183h0a0.183,0.183,0,0,1,.183.183" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-264.729-102.02a0.183,0.183,0,0,1-.183.183,0.182,0.182,0,0,1-.182-0.182h0a0.182,0.182,0,0,1,.181-0.183h0a0.183,0.183,0,0,1,.183.183" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266.067-98.579a0.183,0.183,0,0,1-.183.183,0.182,0.182,0,0,1-.182-0.182h0a0.182,0.182,0,0,1,.181-0.183h0a0.183,0.183,0,0,1,.183.183" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.733-104.727a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.322-103.092a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.642-102.254a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.728-100.739a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-250.744-96.548a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-255.6-93.182a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-255.12-90.179a0.306,0.306,0,0,1-.3.305,0.306,0.306,0,0,1-.3-0.305,0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-255.7-88.178a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-252.227-99.427a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-252.986-90.966a0.178,0.178,0,0,1-.178.178,0.178,0.178,0,0,1-.177-0.178,0.178,0.178,0,0,1,.177-0.178,0.178,0.178,0,0,1,.178.178" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.039-90.9a0.178,0.178,0,0,1-.178.178,0.178,0.178,0,0,1-.177-0.178,0.178,0.178,0,0,1,.177-0.178,0.178,0.178,0,0,1,.178.178" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-255.12-89.079a0.178,0.178,0,0,1-.178.178,0.178,0.178,0,0,1-.177-0.178,0.178,0.178,0,0,1,.177-0.178,0.178,0.178,0,0,1,.178.178" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-252.131-93.359a0.178,0.178,0,0,1-.178.178,0.178,0.178,0,0,1-.177-0.178,0.178,0.178,0,0,1,.177-0.178,0.178,0.178,0,0,1,.178.178" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-256.081-100.385a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.7-98.11a0.3,0.3,0,0,1-.295.305H-259a0.306,0.306,0,0,1-.3-0.305,0.306,0.306,0,0,1,.3-0.3,0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.732-100.36a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.359-95.708a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.474-97.539a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-264.209-94.355a0.306,0.306,0,0,1-.3.305,0.306,0.306,0,0,1-.3-0.305,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.324-90.863a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.853-79.678a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.25-81.2a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295V-81.2a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295V-81.2" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.409-77.793a0.254,0.254,0,0,1-.254.254,0.254,0.254,0,0,1-.254-0.254,0.254,0.254,0,0,1,.254-0.254,0.254,0.254,0,0,1,.254.254" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.909-80.811a0.254,0.254,0,0,1-.254.254,0.254,0.254,0,0,1-.254-0.254,0.254,0.254,0,0,1,.254-0.254,0.254,0.254,0,0,1,.254.254" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.785-82.973a0.254,0.254,0,0,1-.254.254,0.254,0.254,0,0,1-.254-0.254,0.254,0.254,0,0,1,.254-0.254,0.254,0.254,0,0,1,.254.254" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.909-81.974a0.254,0.254,0,0,1-.254.254,0.254,0.254,0,0,1-.254-0.254,0.254,0.254,0,0,1,.254-0.254,0.254,0.254,0,0,1,.254.254" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.495-77.58a0.254,0.254,0,0,1-.254.254A0.254,0.254,0,0,1-263-77.58a0.254,0.254,0,0,1,.254-0.254,0.254,0.254,0,0,1,.254.254" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.563-78.17a0.192,0.192,0,0,1-.192.192h0a0.192,0.192,0,0,1-.192-0.192h0a0.192,0.192,0,0,1,.192-0.192h0a0.192,0.192,0,0,1,.192.192h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-256.79-86.162a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.7-86.714a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3H-263a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.609-85.779a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.609-83.532a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.658-83.638a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.115-87.827a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.709-89.533a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.379-84.791a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.114-82.414a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.256-84.547a0.2,0.2,0,0,1-.2.2,0.2,0.2,0,0,1-.2-0.2,0.2,0.2,0,0,1,.2-0.195,0.2,0.2,0,0,1,.2.195" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.665-84.352a0.2,0.2,0,0,1-.2.2,0.2,0.2,0,0,1-.2-0.2,0.2,0.2,0,0,1,.2-0.195,0.2,0.2,0,0,1,.2.195" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.884-85.381a0.2,0.2,0,0,1-.2.2,0.2,0.2,0,0,1-.2-0.2,0.2,0.2,0,0,1,.2-0.195,0.2,0.2,0,0,1,.2.195" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.32-92.91a0.306,0.306,0,0,1-.3.305,0.306,0.306,0,0,1-.3-0.305,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.556-95.066a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.19-93.04a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-264.059-79.721a0.306,0.306,0,0,1-.3.305,0.306,0.306,0,0,1-.3-0.305,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-292.709-129.2a0.192,0.192,0,0,1-.192.192h0a0.192,0.192,0,0,1-.192-0.192h0a0.192,0.192,0,0,1,.192-0.192h0a0.192,0.192,0,0,1,.192.192h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-267.309-114.707a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.27-112.638a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266.3-113.145a0.227,0.227,0,0,1-.227.227h0a0.227,0.227,0,0,1-.227-0.227h0a0.227,0.227,0,0,1,.227-0.227h0a0.227,0.227,0,0,1,.227.227h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-264.6-116.235a0.227,0.227,0,0,1-.227.227h0a0.226,0.226,0,0,1-.227-0.225v0a0.227,0.227,0,0,1,.227-0.227h0a0.227,0.227,0,0,1,.227.227h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266.861-112.343a0.227,0.227,0,0,1-.227.227h0a0.227,0.227,0,0,1-.227-0.227h0a0.227,0.227,0,0,1,.227-0.227h0a0.227,0.227,0,0,1,.227.227h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.678-110.982a0.227,0.227,0,0,1-.227.227h0a0.227,0.227,0,0,1-.227-0.227h0a0.227,0.227,0,0,1,.227-0.227h0a0.227,0.227,0,0,1,.227.227h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-267.809-123.136a0.306,0.306,0,0,1-.3.3,0.306,0.306,0,0,1-.3-0.3,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.687-126.136a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-264.983-126.008a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.066-126.5a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.867-123.613a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-267.746-123.527a0.174,0.174,0,0,1-.174.174,0.173,0.173,0,0,1-.173-0.173h0a0.173,0.173,0,0,1,.172-0.174h0a0.174,0.174,0,0,1,.174.174" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-195.765-118.236a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-196.07-117.781a0.455,0.455,0,0,1-.454-0.455,0.454,0.454,0,0,1,.454-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-203.209-105.227a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-203.509-104.776a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-199.349-107.108a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-199.654-106.654a0.455,0.455,0,0,1-.455-0.454,0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-200.716-101.579a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-201.021-101.127a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-199.873-99.7a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295V-99.7a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-200.178-99.248a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-195.637-90.945a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-195.942-90.491a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-183.38-81.193a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-183.685-80.739a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-202.061-90.854a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-202.366-90.4a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-199.788-86.646a0.3,0.3,0,0,1-.295.305h-0.01a0.306,0.306,0,0,1-.3-0.305,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-200.093-86.191a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.455h0m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-194.269-93.976a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-194.574-93.521a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-191.535-97.66a0.306,0.306,0,0,1-.3.305,0.306,0.306,0,0,1-.3-0.305,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-191.841-97.205a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.455h0m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.968-93.671a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-193.273-93.217a0.455,0.455,0,0,1-.455-0.454,0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-196.257-87.471a0.48,0.48,0,0,1-.48.48,0.48,0.48,0,0,1-.48-0.48,0.48,0.48,0,0,1,.48-0.48,0.48,0.48,0,0,1,.48.48" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-196.737-86.757a0.715,0.715,0,0,1-.715-0.715,0.716,0.716,0,0,1,.715-0.715,0.716,0.716,0,0,1,.715.715,0.715,0.715,0,0,1-.715.715m0-1.4a0.685,0.685,0,0,0-.684.684,0.685,0.685,0,0,0,.684.685,0.685,0.685,0,0,0,.684-0.685,0.685,0.685,0,0,0-.684-0.684" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-189.291-86.854a0.48,0.48,0,0,1-.48.48,0.48,0.48,0,0,1-.48-0.48,0.48,0.48,0,0,1,.48-0.48,0.48,0.48,0,0,1,.48.48" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-189.771-86.139a0.715,0.715,0,0,1-.715-0.715,0.716,0.716,0,0,1,.715-0.715,0.716,0.716,0,0,1,.715.715,0.715,0.715,0,0,1-.715.715m0-1.4a0.685,0.685,0,0,0-.684.684,0.685,0.685,0,0,0,.684.685,0.685,0.685,0,0,0,.684-0.685,0.685,0.685,0,0,0-.684-0.684" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-206.048-106.143a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-206.353-105.689a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-208.988-109.827a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-209.294-109.378a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-217.177-114.72a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-217.482-114.266a0.455,0.455,0,0,1-.455-0.454,0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-212.925-105.383a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-213.23-104.927a0.455,0.455,0,0,1-.455-0.454,0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-199.717-121.639a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-200.022-121.185a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-210.509-122.307a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-210.819-121.852a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.537-121.853a0.306,0.306,0,0,1-.3.3,0.306,0.306,0,0,1-.3-0.3,0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.842-121.4a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.455h0m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-187.77-123.968a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-188.075-123.513a0.455,0.455,0,0,1-.454-0.455,0.454,0.454,0,0,1,.454-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-185.663-123.144a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-185.968-122.69a0.455,0.455,0,0,1-.454-0.455,0.454,0.454,0,0,1,.454-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-182.735-129.31a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-183.04-128.855a0.455,0.455,0,0,1-.454-0.455,0.454,0.454,0,0,1,.454-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-189.032-129.781a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-189.337-129.327a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-180.759-127.234a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-181.064-126.779a0.455,0.455,0,0,1-.454-0.455,0.454,0.454,0,0,1,.454-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-234.1-131.627a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-234.4-131.168a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-230.394-133.106a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-230.7-132.651a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-237.5-121.879a0.3,0.3,0,0,1-.295.3h-0.01a0.306,0.306,0,0,1-.3-0.3,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-237.809-121.427a0.455,0.455,0,0,1-.454-0.455,0.454,0.454,0,0,1,.454-0.454,0.455,0.455,0,0,1,.455.454,0.455,0.455,0,0,1-.455.455h0m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-235.734-125.207a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-236.039-124.752a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-242.347-118.54a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-242.652-118.085a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-229.109-125.232a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-229.409-124.777a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-223.7-124.049a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-224.009-123.595a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-219.478-119.027a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-219.783-118.568a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.455h0m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-215.089-128.538a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-215.394-128.083a0.455,0.455,0,0,1-.454-0.455,0.454,0.454,0,0,1,.454-0.454,0.455,0.455,0,0,1,.455.454,0.455,0.455,0,0,1-.455.455h0m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-211.584-129.1a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-211.889-128.647a0.455,0.455,0,0,1-.454-0.455,0.454,0.454,0,0,1,.454-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.222-130.932a0.3,0.3,0,0,1-.295.3h-0.01a0.306,0.306,0,0,1-.3-0.3,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.527-130.477a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.455h0m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-204.484-130.6a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-204.789-130.142a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-205.764-132.227a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-206.069-131.777a0.455,0.455,0,0,1-.455-0.454,0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-195.344-129.543a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-195.649-129.088a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-238.285-117.6a0.306,0.306,0,0,1-.3.305,0.306,0.306,0,0,1-.3-0.305,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-238.59-117.145a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.455h0m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-225.286-100.06a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-225.591-99.606a0.455,0.455,0,0,1-.454-0.455,0.454,0.454,0,0,1,.454-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-228.517-89.39a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-228.822-88.935a0.455,0.455,0,0,1-.454-0.455,0.454,0.454,0,0,1,.454-0.454,0.455,0.455,0,0,1,.455.454,0.455,0.455,0,0,1-.455.455h0m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-231.369-86.252a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-231.674-85.8a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-226.309-93.445a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-226.618-92.99a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-233.787-88.271a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-234.093-87.817a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.651-122.251a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.956-121.8a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.027-121.178a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.332-120.727a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266.386-122.774a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266.691-122.319a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.438-124.245a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.743-123.791a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-270.659-115.06a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-270.964-114.605a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-271.886-110.145a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-272.191-109.69a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.1-107.9a0.306,0.306,0,0,1-.3.305,0.306,0.306,0,0,1-.3-0.305,0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.4-107.444a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.455h0m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-273.658-123.537a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-273.963-123.082a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.471-121.782a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.776-121.327a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.758-122.965a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.063-122.511a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.446-120.111a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.751-119.657a0.455,0.455,0,0,1-.455-0.454,0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-278.354-117.473a0.306,0.306,0,0,1-.3.305,0.306,0.306,0,0,1-.3-0.305,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-278.659-117.018a0.455,0.455,0,0,1-.454-0.455,0.454,0.454,0,0,1,.454-0.454,0.455,0.455,0,0,1,.455.454,0.455,0.455,0,0,1-.455.455h0m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-275.976-112.175a0.417,0.417,0,0,1-.417.417h0a0.417,0.417,0,0,1-.417-0.417h0a0.417,0.417,0,0,1,.417-0.417h0a0.417,0.417,0,0,1,.417.417h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-276.393-111.554a0.622,0.622,0,0,1-.621-0.621,0.622,0.622,0,0,1,.621-0.622,0.622,0.622,0,0,1,.621.622,0.622,0.622,0,0,1-.621.621m0-1.216a0.6,0.6,0,0,0-.6.6h0a0.6,0.6,0,0,0,.6.594,0.6,0.6,0,0,0,.6-0.594,0.6,0.6,0,0,0-.6-0.6" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.115-132.627a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.42-132.176a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-269.72-130.8a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-270.025-130.346a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-273.55-128.236a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-273.855-127.781a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-283.933-124.438a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-284.238-123.983a0.455,0.455,0,0,1-.454-0.455,0.454,0.454,0,0,1,.454-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-276.151-130.187a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-276.456-129.727a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-273.481-126.033a0.482,0.482,0,0,1-.482.482,0.482,0.482,0,0,1-.482-0.482,0.482,0.482,0,0,1,.482-0.482,0.482,0.482,0,0,1,.482.482" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-273.963-125.315a0.72,0.72,0,0,1-.718-0.719,0.719,0.719,0,0,1,.718-0.718,0.719,0.719,0,0,1,.718.718,0.72,0.72,0,0,1-.718.719m0-1.406a0.688,0.688,0,0,0-.687.687,0.688,0.688,0,0,0,.687.688,0.688,0.688,0,0,0,.687-0.688,0.688,0.688,0,0,0-.687-0.687" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-273.045-120.388a0.482,0.482,0,0,1-.482.482,0.482,0.482,0,0,1-.482-0.482,0.482,0.482,0,0,1,.482-0.482,0.482,0.482,0,0,1,.482.482" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-273.527-119.669a0.72,0.72,0,0,1-.718-0.719,0.719,0.719,0,0,1,.718-0.718,0.719,0.719,0,0,1,.718.718,0.72,0.72,0,0,1-.718.719m0-1.406a0.688,0.688,0,0,0-.687.687,0.688,0.688,0,0,0,.687.688,0.688,0.688,0,0,0,.687-0.688,0.688,0.688,0,0,0-.687-0.687" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.387-133.167a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.692-132.712a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266.843-134.284a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-267.148-133.827a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.191-131.777a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.5-131.322a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-252.871-129.443a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-253.176-128.989a0.455,0.455,0,0,1-.454-0.455,0.454,0.454,0,0,1,.454-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-253.154-125.962a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-253.459-125.508a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-251.468-134.687a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-251.773-134.233a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-248.6-134.687a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-248.909-134.233a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-246.594-131.907a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-246.9-131.452a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.567-130.86a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.872-130.405a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-292.7-126.427a0.306,0.306,0,0,1-.3.3,0.306,0.306,0,0,1-.3-0.3,0.3,0.3,0,0,1,.3-0.3H-293a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-293.009-125.972a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.826-125.42a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.131-124.965a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-266.732-127.627a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-267.037-127.177a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.692-124.381a0.306,0.306,0,0,1-.3.3,0.306,0.306,0,0,1-.3-0.3,0.3,0.3,0,0,1,.3-0.3H-262a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262-123.927a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-224.152-91.245a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-224.457-90.791a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-238.338-111.938a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-238.643-111.483a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.455h0m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-236.577-102.182a0.306,0.306,0,0,1-.3.305,0.306,0.306,0,0,1-.3-0.305,0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-236.882-101.727a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.455h0m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-241.986-102.637a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-242.291-102.182a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-273.252-111.667a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-273.557-111.212a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.758-105.65a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.063-105.195a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.455h0m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-268.109-108.982a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-268.415-108.527a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.455h0m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.943-104.144a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.248-103.689a0.455,0.455,0,0,1-.454-0.455,0.454,0.454,0,0,1,.454-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.869-104.268a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-263.174-103.813a0.455,0.455,0,0,1-.454-0.455,0.454,0.454,0,0,1,.454-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-253.753-93.382a0.431,0.431,0,0,1-.43.43,0.431,0.431,0,0,1-.43-0.43,0.431,0.431,0,0,1,.43-0.43,0.431,0.431,0,0,1,.43.43" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.183-92.741a0.642,0.642,0,0,1-.641-0.641,0.641,0.641,0,0,1,.641-0.64,0.641,0.641,0,0,1,.64.64,0.641,0.641,0,0,1-.64.641m0-1.254a0.614,0.614,0,0,0-.613.613,0.614,0.614,0,0,0,.613.613,0.614,0.614,0,0,0,.613-0.613,0.614,0.614,0,0,0-.613-0.613" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.328-90.027a0.43,0.43,0,0,1-.43.43,0.431,0.431,0,0,1-.43-0.43,0.431,0.431,0,0,1,.43-0.43,0.43,0.43,0,0,1,.43.43" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.758-89.385a0.642,0.642,0,0,1-.641-0.641,0.641,0.641,0,0,1,.641-0.64,0.641,0.641,0,0,1,.64.64,0.641,0.641,0,0,1-.64.641m0-1.254a0.614,0.614,0,0,0-.613.613,0.614,0.614,0,0,0,.613.613,0.614,0.614,0,0,0,.613-0.613,0.614,0.614,0,0,0-.613-0.613" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.162-101.107a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.467-100.652a0.455,0.455,0,0,1-.454-0.455,0.454,0.454,0,0,1,.454-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-264.857-99.227a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-265.162-98.771a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.372-86.046a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.677-85.591a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.759-78.39a0.245,0.245,0,0,1-.245.245,0.246,0.246,0,0,1-.246-0.245,0.245,0.245,0,0,1,.245-0.245h0a0.245,0.245,0,0,1,.245.245" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.009-78.027a0.366,0.366,0,0,1-.366-0.366,0.366,0.366,0,0,1,.366-0.366,0.366,0.366,0,0,1,.366.366,0.366,0.366,0,0,1-.366.366m0-.716a0.35,0.35,0,0,0-.35.35h0a0.35,0.35,0,0,0,.35.35h0a0.35,0.35,0,0,0,.35-0.35h0a0.35,0.35,0,0,0-.35-0.35h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.609-81.259a0.245,0.245,0,0,1-.245.245,0.245,0.245,0,0,1-.246-0.244h0a0.246,0.246,0,0,1,.246-0.245,0.245,0.245,0,0,1,.245.245" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-261.853-80.893a0.366,0.366,0,0,1-.366-0.366,0.366,0.366,0,0,1,.366-0.366,0.366,0.366,0,0,1,.366.366,0.366,0.366,0,0,1-.366.366m0-.716a0.35,0.35,0,0,0-.35.35h0a0.35,0.35,0,0,0,.35.35h0a0.35,0.35,0,0,0,.35-0.35h0a0.35,0.35,0,0,0-.35-0.35h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.47-89.207a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.775-88.752a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-256.043-102.389a0.306,0.306,0,0,1-.3.305,0.306,0.306,0,0,1-.3-0.305,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-256.348-101.935a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-198.984-117.35a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-199.289-116.895a0.455,0.455,0,0,1-.454-0.455,0.454,0.454,0,0,1,.454-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-199.289-116.7a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-184.769-126.355a0.306,0.306,0,0,1-.3.3,0.306,0.306,0,0,1-.3-0.3,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-185.074-125.9a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-185.074-125.706a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-198.015-122.158a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-198.32-121.7a0.455,0.455,0,0,1-.454-0.455,0.454,0.454,0,0,1,.454-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-198.32-121.508a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.649,0.65,0.65,0,0,1,.649.649,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-232.933-132.651a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-233.238-132.2a0.455,0.455,0,0,1-.454-0.455,0.454,0.454,0,0,1,.454-0.454,0.455,0.455,0,0,1,.455.454,0.455,0.455,0,0,1-.455.455h0m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-233.238-132a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-234.8-119.515a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-235.1-119.06a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-235.1-118.866a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-224.519-127.841a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-224.824-127.387a0.455,0.455,0,0,1-.455-0.454,0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-224.824-127.192a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.649,0.65,0.65,0,0,1,.649.649,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-220-130.727a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-220.309-130.274a0.455,0.455,0,0,1-.454-0.455,0.454,0.454,0,0,1,.454-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-220.309-130.08a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.5-123.35a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.8-122.9a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.8-122.7a0.65,0.65,0,0,1-.649-0.649A0.65,0.65,0,0,1-227.8-124a0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-224.766-115.266a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-225.071-114.811a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.454,0.455,0.455,0,0,1,.455.454,0.455,0.455,0,0,1-.455.455h0m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-225.071-114.617a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-220.128-124.627a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-220.433-124.168a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-220.433-123.973a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-223-113.113a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-223.3-112.659a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-223.3-112.464a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-237.855-118.54a0.306,0.306,0,0,1-.3.305,0.306,0.306,0,0,1-.3-0.305,0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-238.16-118.085a0.455,0.455,0,0,1-.454-0.455A0.455,0.455,0,0,1-238.16-119a0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.455h0m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-238.16-117.891a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-212.4-114.943a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-212.7-114.489a0.455,0.455,0,0,1-.454-0.455,0.454,0.454,0,0,1,.454-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-212.7-114.294a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-205.5-112.027a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-205.809-111.572a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-205.809-111.378a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.649,0.65,0.65,0,0,1,.649.649,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-207.175-107.5a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-207.48-107.046a0.455,0.455,0,0,1-.455-0.454,0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-207.48-106.851a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-197.664-91.5a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295V-91.5a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295V-91.5" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-197.969-91.048a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-197.969-90.854a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-201.832-89.063a0.306,0.306,0,0,1-.3.305,0.306,0.306,0,0,1-.3-0.305,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-202.137-88.608a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.455h0m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-202.137-88.413a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-215.353-109.667a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-215.658-109.213a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-215.658-109.017a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-211.279-125.153a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-211.584-124.7a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-211.584-124.5a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-194.509-123.1a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-194.817-122.642a0.455,0.455,0,0,1-.454-0.455,0.454,0.454,0,0,1,.454-0.454,0.455,0.455,0,0,1,.455.454,0.455,0.455,0,0,1-.455.455h0m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-194.817-122.448a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-185.1-129a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295V-129a0.3,0.3,0,0,1,.3-0.3h0.005a0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-185.409-128.55a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-185.409-128.356a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.649,0.65,0.65,0,0,1,.649.649,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-186.209-125.538a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-186.509-125.083a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.455h0m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-186.509-124.889a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.649,0.65,0.65,0,0,1,.649.649,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-239.1-106.262a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-239.409-105.807a0.455,0.455,0,0,1-.454-0.455,0.454,0.454,0,0,1,.454-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-239.409-105.612a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.315-101.4a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.62-100.942a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-227.62-100.747a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.649,0.65,0.65,0,0,1,.649.649,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-233.345-99.5a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295V-99.5a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-233.65-99.044a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-233.65-98.849A0.65,0.65,0,0,1-234.3-99.5a0.65,0.65,0,0,1,.649-0.649A0.65,0.65,0,0,1-233-99.5a0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-181.855-84.437a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-182.16-83.982a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-182.16-83.788a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.649,0.65,0.65,0,0,1,.649.649,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-231.609-93.7a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295V-93.7a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295V-93.7" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-231.909-93.248a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-231.909-93.054a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.649,0.65,0.65,0,0,1,.649.649,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-223.809-93.227a0.306,0.306,0,0,1-.3.305,0.306,0.306,0,0,1-.3-0.305,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-224.109-92.776a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.455h0m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-224.109-92.582a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-275.1-115.7a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-275.4-115.249a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-275.4-115.054a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-276.609-128.218a0.306,0.306,0,0,1-.3.3,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-276.909-127.763a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-276.909-127.569a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.087-132.884a0.306,0.306,0,0,1-.3.3,0.306,0.306,0,0,1-.3-0.3,0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.393-132.427a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.455h0m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.393-132.235a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.133-129.227a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.438-128.772a0.455,0.455,0,0,1-.455-0.454,0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.438-128.577a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-268.1-129.2a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-268.4-128.744a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-268.4-128.549a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-270.445-128.744a0.3,0.3,0,0,1-.295.3h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-270.75-128.29a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-270.75-128.095a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.637-100.371a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.943-99.917a0.454,0.454,0,0,1-.454-0.454,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.455.455h0a0.455,0.455,0,0,1-.455.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.943-99.722a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.264-94.972a0.362,0.362,0,0,1-.362.362,0.362,0.362,0,0,1-.362-0.362,0.362,0.362,0,0,1,.362-0.362,0.362,0.362,0,0,1,.362.362" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.626-94.433a0.54,0.54,0,0,1-.539-0.539,0.54,0.54,0,0,1,.539-0.539,0.54,0.54,0,0,1,.539.539,0.54,0.54,0,0,1-.539.539m0-1.055a0.516,0.516,0,0,0-.516.516h0a0.516,0.516,0,0,0,.516.516h0a0.516,0.516,0,0,0,.516-0.516h0a0.516,0.516,0,0,0-.516-0.516h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-260.626-94.2a0.77,0.77,0,0,1-.77-0.77h0a0.77,0.77,0,0,1,.77-0.77h0a0.77,0.77,0,0,1,.77.77h0a0.77,0.77,0,0,1-.77.77h0m0-1.517a0.748,0.748,0,0,0-.747.747,0.748,0.748,0,0,0,.747.747,0.748,0.748,0,0,0,.747-0.747,0.748,0.748,0,0,0-.747-0.747" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-255.563-98.491a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-255.868-98.037a0.455,0.455,0,0,1-.455-0.454,0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-255.868-97.842a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.9-93.814a0.306,0.306,0,0,1-.3.305,0.306,0.306,0,0,1-.3-0.305,0.306,0.306,0,0,1,.3-0.3,0.306,0.306,0,0,1,.3.3" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.209-93.359a0.455,0.455,0,0,1-.454-0.455,0.455,0.455,0,0,1,.454-0.455,0.455,0.455,0,0,1,.454.455,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-258.209-93.164a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.459-96.927a0.3,0.3,0,0,1-.295.305h-0.01a0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.764-96.476a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-254.764-96.282a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.563-83.736a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.868-83.282a0.455,0.455,0,0,1-.455-0.454,0.455,0.455,0,0,1,.455-0.455h0a0.455,0.455,0,0,1,.454.455,0.454,0.454,0,0,1-.454.454m0-.889a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.868-83.087a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.309-87.585a0.306,0.306,0,0,1-.3.305,0.3,0.3,0,0,1-.3-0.295v-0.01a0.3,0.3,0,0,1,.3-0.3h0.005a0.3,0.3,0,0,1,.3.295v0.005" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.609-87.127a0.455,0.455,0,0,1-.455-0.455h0a0.455,0.455,0,0,1,.455-0.454,0.454,0.454,0,0,1,.454.454,0.455,0.455,0,0,1-.454.455m0-.89a0.435,0.435,0,0,0-.435.435,0.435,0.435,0,0,0,.435.435,0.435,0.435,0,0,0,.435-0.435,0.435,0.435,0,0,0-.435-0.435" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-259.609-86.936a0.65,0.65,0,0,1-.649-0.649,0.65,0.65,0,0,1,.649-0.65,0.65,0.65,0,0,1,.649.65,0.65,0.65,0,0,1-.649.649m0-1.279a0.631,0.631,0,0,0-.63.63,0.631,0.631,0,0,0,.63.63,0.631,0.631,0,0,0,.63-0.63,0.631,0.631,0,0,0-.63-0.63" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.062-127.77a0.484,0.484,0,0,1-.484.484,0.484,0.484,0,0,1-.484-0.484,0.484,0.484,0,0,1,.484-0.484,0.484,0.484,0,0,1,.484.484" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.546-127.049a0.721,0.721,0,0,1-.721-0.721,0.722,0.722,0,0,1,.721-0.721,0.723,0.723,0,0,1,.721.721,0.722,0.722,0,0,1-.721.721m0-1.411a0.691,0.691,0,0,0-.69.69,0.69,0.69,0,0,0,.69.69h0a0.691,0.691,0,0,0,.69-0.69,0.691,0.691,0,0,0-.69-0.69" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.546-126.74a1.031,1.031,0,0,1-1.03-1.03,1.031,1.031,0,0,1,1.03-1.03,1.031,1.031,0,0,1,1.03,1.03,1.031,1.031,0,0,1-1.03,1.03m0-2.029a1,1,0,0,0-1,1,1,1,0,0,0,1,1,1,1,0,0,0,1-1,1,1,0,0,0-1-1" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-232.209-106.983a0.484,0.484,0,0,1-.484.484,0.484,0.484,0,0,1-.484-0.484,0.484,0.484,0,0,1,.484-0.484,0.484,0.484,0,0,1,.484.484" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-232.693-106.262a0.721,0.721,0,0,1-.721-0.721,0.721,0.721,0,0,1,.721-0.721,0.722,0.722,0,0,1,.721.721,0.722,0.722,0,0,1-.721.721m0-1.411a0.691,0.691,0,0,0-.69.69,0.69,0.69,0,0,0,.69.69h0a0.691,0.691,0,0,0,.69-0.69,0.691,0.691,0,0,0-.69-0.69" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-232.693-105.953a1.031,1.031,0,0,1-1.03-1.03,1.031,1.031,0,0,1,1.03-1.03,1.031,1.031,0,0,1,1.03,1.03,1.031,1.031,0,0,1-1.03,1.03m0-2.029a1,1,0,0,0-1,1,1,1,0,0,0,1,1,1,1,0,0,0,1-1,1,1,0,0,0-1-1" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-230.161-121.112a0.484,0.484,0,0,1-.484.484,0.484,0.484,0,0,1-.484-0.484,0.484,0.484,0,0,1,.484-0.484,0.484,0.484,0,0,1,.484.484" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-230.645-120.391a0.721,0.721,0,0,1-.721-0.721,0.722,0.722,0,0,1,.721-0.721,0.722,0.722,0,0,1,.721.721,0.721,0.721,0,0,1-.721.721m0-1.411a0.691,0.691,0,0,0-.69.69,0.69,0.69,0,0,0,.69.69h0a0.691,0.691,0,0,0,.69-0.69,0.691,0.691,0,0,0-.69-0.69" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-230.645-120.082a1.031,1.031,0,0,1-1.03-1.03,1.031,1.031,0,0,1,1.03-1.03,1.031,1.031,0,0,1,1.03,1.03,1.031,1.031,0,0,1-1.03,1.03m0-2.029a1,1,0,0,0-1,1,1,1,0,0,0,1,1,1,1,0,0,0,1-1,1,1,0,0,0-1-1" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.166-88.945a0.484,0.484,0,0,1-.484.484,0.484,0.484,0,0,1-.484-0.484,0.484,0.484,0,0,1,.484-0.484,0.484,0.484,0,0,1,.484.484" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.65-88.227a0.721,0.721,0,0,1-.721-0.721,0.721,0.721,0,0,1,.721-0.721,0.721,0.721,0,0,1,.721.721,0.721,0.721,0,0,1-.721.721m0-1.411a0.691,0.691,0,0,0-.69.69,0.691,0.691,0,0,0,.69.69,0.691,0.691,0,0,0,.69-0.69,0.691,0.691,0,0,0-.69-0.69" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-192.65-87.915a1.031,1.031,0,0,1-1.03-1.03,1.031,1.031,0,0,1,1.03-1.03,1.031,1.031,0,0,1,1.03,1.03,1.031,1.031,0,0,1-1.03,1.03m0-2.029a1,1,0,0,0-1,1,1,1,0,0,0,1,1,1,1,0,0,0,1-1,1,1,0,0,0-1-1" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-200.809-127.19a0.484,0.484,0,0,1-.484.484,0.484,0.484,0,0,1-.484-0.484,0.484,0.484,0,0,1,.484-0.484,0.484,0.484,0,0,1,.484.484" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-201.289-126.469a0.721,0.721,0,0,1-.721-0.721,0.721,0.721,0,0,1,.721-0.721,0.721,0.721,0,0,1,.721.721,0.721,0.721,0,0,1-.721.721m0-1.411a0.691,0.691,0,0,0-.69.69,0.69,0.69,0,0,0,.69.69h0a0.691,0.691,0,0,0,.69-0.69,0.691,0.691,0,0,0-.69-0.69" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-201.289-126.16a1.031,1.031,0,0,1-1.03-1.03,1.031,1.031,0,0,1,1.03-1.03,1.031,1.031,0,0,1,1.03,1.03,1.031,1.031,0,0,1-1.03,1.03m0-2.029a1,1,0,0,0-1,1,1,1,0,0,0,1,1,1,1,0,0,0,1-1,1,1,0,0,0-1-1" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-249.043-130.877a0.484,0.484,0,0,1-.484.484,0.484,0.484,0,0,1-.484-0.484,0.484,0.484,0,0,1,.484-0.484,0.484,0.484,0,0,1,.484.484" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-249.527-130.156a0.722,0.722,0,0,1-.721-0.721,0.723,0.723,0,0,1,.721-0.721,0.722,0.722,0,0,1,.721.721,0.721,0.721,0,0,1-.721.721m0-1.411a0.691,0.691,0,0,0-.69.69,0.691,0.691,0,0,0,.69.69,0.69,0.69,0,0,0,.69-0.69h0a0.691,0.691,0,0,0-.69-0.69" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-249.527-129.847a1.031,1.031,0,0,1-1.03-1.03,1.031,1.031,0,0,1,1.03-1.03,1.031,1.031,0,0,1,1.03,1.03,1.031,1.031,0,0,1-1.03,1.03m0-2.029a1,1,0,0,0-1,1,1,1,0,0,0,1,1,1,1,0,0,0,1-1,1,1,0,0,0-1-1" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-267.8-118.675a0.484,0.484,0,0,1-.484.484,0.484,0.484,0,0,1-.484-0.484,0.484,0.484,0,0,1,.484-0.484,0.484,0.484,0,0,1,.484.484" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-268.286-117.954a0.721,0.721,0,0,1-.721-0.721,0.722,0.722,0,0,1,.721-0.721,0.722,0.722,0,0,1,.721.721,0.721,0.721,0,0,1-.721.721m0-1.411a0.691,0.691,0,0,0-.69.69,0.69,0.69,0,0,0,.69.69h0a0.691,0.691,0,0,0,.69-0.69,0.691,0.691,0,0,0-.69-0.69" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-268.286-117.645a1.031,1.031,0,0,1-1.03-1.03,1.031,1.031,0,0,1,1.03-1.03,1.031,1.031,0,0,1,1.03,1.03,1.031,1.031,0,0,1-1.03,1.03m0-2.029a1,1,0,0,0-1,1,1,1,0,0,0,1,1,1,1,0,0,0,1-1,1,1,0,0,0-1-1" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-279.659-120.388a0.484,0.484,0,0,1-.484.484,0.484,0.484,0,0,1-.484-0.484,0.484,0.484,0,0,1,.484-0.484,0.484,0.484,0,0,1,.484.484" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-280.143-119.667a0.721,0.721,0,0,1-.721-0.721,0.722,0.722,0,0,1,.721-0.721,0.722,0.722,0,0,1,.721.721,0.721,0.721,0,0,1-.721.721m0-1.411a0.691,0.691,0,0,0-.69.69,0.69,0.69,0,0,0,.69.69h0a0.69,0.69,0,0,0,.69-0.69h0a0.691,0.691,0,0,0-.69-0.69" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-280.143-119.358a1.031,1.031,0,0,1-1.03-1.03,1.031,1.031,0,0,1,1.03-1.03,1.031,1.031,0,0,1,1.03,1.03,1.031,1.031,0,0,1-1.03,1.03m0-2.029a1,1,0,0,0-1,1,1,1,0,0,0,1,1,1,1,0,0,0,1-1,1,1,0,0,0-1-1" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.231-97.775a0.445,0.445,0,0,1-.445.445h0a0.444,0.444,0,0,1-.444-0.444h0a0.444,0.444,0,0,1,.443-0.445h0a0.445,0.445,0,0,1,.445.445h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.676-97.112a0.664,0.664,0,0,1-.663-0.663,0.664,0.664,0,0,1,.663-0.662,0.664,0.664,0,0,1,.663.662,0.664,0.664,0,0,1-.663.663m0-1.3a0.635,0.635,0,0,0-.634.634,0.634,0.634,0,0,0,.634.634h0a0.634,0.634,0,0,0,.634-0.634h0a0.635,0.635,0,0,0-.634-0.634" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-262.676-96.827a0.947,0.947,0,0,1-.946-0.947,0.947,0.947,0,0,1,.946-0.946,0.947,0.947,0,0,1,.946.946,0.947,0.947,0,0,1-.946.947m0-1.865a0.919,0.919,0,0,0-.918.918,0.919,0.919,0,0,0,.918.918,0.919,0.919,0,0,0,.918-0.918,0.919,0.919,0,0,0-.918-0.918" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-256.86-89.427a0.445,0.445,0,0,1-.445.445h0a0.444,0.444,0,0,1-.444-0.444h0a0.444,0.444,0,0,1,.443-0.445h0a0.445,0.445,0,0,1,.445.445h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.309-88.767a0.664,0.664,0,0,1-.663-0.663,0.664,0.664,0,0,1,.663-0.662,0.664,0.664,0,0,1,.663.662,0.664,0.664,0,0,1-.663.663m0-1.3a0.634,0.634,0,0,0-.634.634h0a0.634,0.634,0,0,0,.634.634h0a0.634,0.634,0,0,0,.634-0.634h0a0.634,0.634,0,0,0-.634-0.634h0" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-257.309-88.483a0.947,0.947,0,0,1-.946-0.947,0.947,0.947,0,0,1,.946-0.946,0.947,0.947,0,0,1,.947.946,0.948,0.948,0,0,1-.947.947m0-1.865a0.919,0.919,0,0,0-.918.918,0.919,0.919,0,0,0,.918.918,0.919,0.919,0,0,0,.918-0.918,0.919,0.919,0,0,0-.918-0.918" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-288.253-127.486a0.484,0.484,0,0,1-.484.484,0.484,0.484,0,0,1-.484-0.484,0.484,0.484,0,0,1,.484-0.484,0.484,0.484,0,0,1,.484.484" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-288.737-126.765a0.721,0.721,0,0,1-.721-0.721,0.722,0.722,0,0,1,.721-0.721,0.722,0.722,0,0,1,.721.721,0.721,0.721,0,0,1-.721.721m0-1.411a0.691,0.691,0,0,0-.69.69,0.691,0.691,0,0,0,.69.69,0.69,0.69,0,0,0,.69-0.69h0a0.691,0.691,0,0,0-.69-0.69" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-288.737-126.456a1.031,1.031,0,0,1-1.03-1.03,1.031,1.031,0,0,1,1.03-1.03,1.031,1.031,0,0,1,1.03,1.03,1.031,1.031,0,0,1-1.03,1.03m0-2.029a1,1,0,0,0-1,1,1,1,0,0,0,1,1,1,1,0,0,0,1-1,1,1,0,0,0-1-1" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-281.924-124.59a0.484,0.484,0,0,1-.484.484,0.484,0.484,0,0,1-.484-0.484,0.484,0.484,0,0,1,.484-0.484,0.484,0.484,0,0,1,.484.484" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-282.409-123.869a0.721,0.721,0,0,1-.721-0.721,0.722,0.722,0,0,1,.721-0.721,0.722,0.722,0,0,1,.721.721,0.721,0.721,0,0,1-.721.721m0-1.411a0.691,0.691,0,0,0-.69.69,0.691,0.691,0,0,0,.69.69,0.691,0.691,0,0,0,.69-0.69,0.691,0.691,0,0,0-.69-0.69" transform="translate(295.283 135.142)"/>
                        <path class="cls-3" d="M-282.409-123.56a1.031,1.031,0,0,1-1.03-1.03,1.031,1.031,0,0,1,1.03-1.03,1.031,1.031,0,0,1,1.03,1.03,1.031,1.031,0,0,1-1.03,1.03m0-2.029a1,1,0,0,0-1,1,1,1,0,0,0,1,1,1,1,0,0,0,1-1,1,1,0,0,0-1-1" transform="translate(295.283 135.142)"/>
                      </g>
                    </g>
                  </g>
                  <g id="star-1_static">
                    <g id="star">
                      <circle id="center" class="cls-4" cx="15.533" cy="19.748" r="0.548"/>
                    </g>
                  </g>
                  <g id="star-2_static">
                    <g id="star-2-2">
                      <circle id="center-2" class="cls-4" cx="16.745" cy="21.432" r="0.499"/>
                    </g>
                  </g>
                  <g id="star-3_static">
                    <g id="star-3-2">
                      <circle id="center-3" class="cls-4" cx="19.295" cy="21.347" r="0.499"/>
                    </g>
                  </g>
                  <g id="star-4_static">
                    <g id="star-4-2">
                      <circle id="center-4" class="cls-4" cx="23.657" cy="23.141" r="0.499"/>
                    </g>
                  </g>
                  <g id="star-4-3_static">
                    <g id="star-5">
                      <circle id="center-5" class="cls-4" cx="28.034" cy="30.869" r="0.499"/>
                    </g>
                  </g>
                  <g id="star-5-2_static">
                    <g id="star-6">
                      <circle id="center-6" class="cls-4" cx="31.831" cy="17.945" r="0.499"/>
                    </g>
                  </g>
                  <g id="star-6-2_static">
                    <g id="star-7">
                      <circle id="center-7" class="cls-4" cx="55.666" cy="12.789" r="0.499"/>
                    </g>
                  </g>
                  <!--***[ NO MODIFICAR ]*** -->
                  <g id="star-1" class="star-1">
                    <g id="star">
                      <circle id="center" class="cls-4 center" cx="15.533" cy="19.748" r="0.731"/>
                      <circle id="first-line" class="cls-4 first-line" cx="15.533" cy="19.748" r="1" style="fill: rgba(0, 0, 0, 0);stroke: #fcee21;stroke-width: 0.05px;"></circle>
                      <circle id="last-line" class="cls-4 last-line" cx="15.533" cy="19.748" r="1.35" style="fill: rgba(0, 0, 0, 0);stroke: #fcee21;stroke-width: 0.05px;"></circle>
                    </g>
                  </g>
                  <g id="star-2" class="star-2">
                    <g id="star-2-2" data-name="star">
                      <circle id="center" class="cls-4 center" cx="16.745" cy="21.432" r="0.731"/>
                      <circle id="first-line" class="cls-4 first-line" cx="16.745" cy="21.432" r="1" style="fill: rgba(0, 0, 0, 0);stroke: #fcee21;stroke-width: 0.05px;"></circle>
                      <circle id="last-line" class="cls-4 last-line" cx="16.745" cy="21.432" r="1.35" style="fill: rgba(0, 0, 0, 0);stroke: #fcee21;stroke-width: 0.05px;"></circle>
                    </g>
                  </g>
                  <g id="star-3" class="star-3">
                    <g id="star-3-2" data-name="star">
                      <circle id="center" class="cls-4 center" cx="19.295" cy="21.347" r="0.731"/>
                      <circle id="first-line" class="cls-4 first-line" cx="19.295" cy="21.347" r="1" style="fill: rgba(0, 0, 0, 0);stroke: #fcee21;stroke-width: 0.05px;"></circle>
                      <circle id="last-line" class="cls-4 last-line" cx="19.295" cy="21.347" r="1.35" style="fill: rgba(0, 0, 0, 0);stroke: #fcee21;stroke-width: 0.05px;"></circle>
                    </g>
                  </g>
                  <g id="star-4" class="star-4">
                    <g id="star-4-2" data-name="star">
                      <circle id="center" class="cls-4 center" cx="23.657" cy="23.141" r="0.731"/>
                      <circle id="first-line" class="cls-4 first-line" cx="23.657" cy="23.141" r="1" style="fill: rgba(0, 0, 0, 0);stroke: #fcee21;stroke-width: 0.05px;"></circle>
                      <circle id="last-line" class="cls-4 last-line" cx="23.657" cy="23.141" r="1.35" style="fill: rgba(0, 0, 0, 0);stroke: #fcee21;stroke-width: 0.05px;"></circle>
                    </g>
                  </g>
                  <g id="star-5" data-name="star-4" class="star-5">
                    <g id="star-5" data-name="star">
                      <circle id="center" class="cls-4 center" cx="28.034" cy="30.869" r="0.731"/>
                      <circle id="first-line" class="cls-4 first-line" cx="28.034" cy="30.869" r="1" style="fill: rgba(0, 0, 0, 0);stroke: #fcee21;stroke-width: 0.05px;"></circle>
                      <circle id="last-line" class="cls-4 last-line" cx="28.034" cy="30.869" r="1.35" style="fill: rgba(0, 0, 0, 0);stroke: #fcee21;stroke-width: 0.05px;"></circle>
                    </g>
                  </g>
                  <g id="star-6" data-name="star-5" class="star-6">
                    <g id="star-6" data-name="star">
                      <circle id="center" class="cls-4 center" cx="31.831" cy="17.945" r="0.731"/>
                      <circle id="first-line" class="cls-4 first-line" cx="31.831" cy="17.945" r="1" style="fill: rgba(0, 0, 0, 0);stroke: #fcee21;stroke-width: 0.05px;"></circle>
                      <circle id="last-line" class="cls-4 last-line" cx="31.831" cy="17.945" r="1.35" style="fill: rgba(0, 0, 0, 0);stroke: #fcee21;stroke-width: 0.05px;"></circle>
                    </g>
                  </g>
                  <g id="star-7" data-name="star-6" class="star-7">
                    <g id="star-7" data-name="star">
                      <circle id="center" class="cls-4 center" cx="55.666" cy="12.789" r="0.731"/>
                      <circle id="first-line" class="cls-4 first-line" cx="55.666" cy="12.789" r="1" style="fill: rgba(0, 0, 0, 0);stroke: #fcee21;stroke-width: 0.05px;"></circle>
                      <circle id="last-line" class="cls-4 last-line" cx="55.666" cy="12.789" r="1.35" style="fill: rgba(0, 0, 0, 0);stroke: #fcee21;stroke-width: 0.05px;"></circle>
                    </g>
                  </g>
                </g>
                <!--***[ NO MODIFICAR ]*** -->
              </svg>
            </div>
            <ul class="content-agencies">
              <li data-agency=".star-1" ><img src="img/duncan-channon.png"></li>
              <li data-agency=".star-2,.star-6,.star-7" ><img src="img/freedman.png"></li>
              <li data-agency=".star-4" ><img src="img/third-rail-creative.png"></li>
              <li data-agency=".star-3" ><img src="img/nomadic.svg"></li>
              <li data-agency=".star-2,.star-6"  class="xs"><img src="img/26.2.svg"></li>
              <li data-agency=".star-5"  class="sm"><img src="img/havas-tribu.svg"></li>
              <li data-agency=".star-5" ><img src="img/quimica.png"></li>
              <li data-agency=".star-3" ><img src="img/optimad.png"></li>
            </ul>
        </section>
        <section class="section our-brands">
          <div class="content">
            <h2>Let us fluff our feathers</h2>
            <div class="content-feathers">
              <div class="brand"><img src="img/nike_logo.png"></div>
              <div class="brand"><img src="img/clashofclans_logo.png"></div>
              <div class="brand"><img src="img/easports_logo.png"></div>
              <div class="brand"><img src="img/gatorade.png"></div>
              <div class="brand"><img src="img/wholefoods_logo.png"></div>
              <div class="brand"><img src="img/supercell_logo.png"></div>
              <div class="brand"><img src="img/lego_logo.png"></div>
              <div class="brand"><img src="img/mcdonalds.png"></div>
              <div class="brand"><img src="img/twd_logo.png"></div>
              <div class="brand"><img src="img/hbo_logo.png"></div>
              <div class="brand"><img src="img/disney_logo.png"></div>
              <div class="brand"><img src="img/josecuervo_logo.png"></div>
            </div>
          </div>
        </section>
        <section class="section rich-media-experts">
          <div class="content">
            <h2 class="title--double-line">
               We are Rich Media Experts
               <span> This is what we do</span>
            </h2>
            <!-- <div class="btn-portfolio">
              <a href="http://richmediaexperts.com/portfolio">VIEW OUR WORK</a>
            </div> -->
            <div class="culunm left">
              <div class="box ads">
                <h3>HTML5 Ads</h3>
                <ul>
                  <li>Resposive Banners</li>
                  <li>Video Banners</li>
                  <li>Expandables/pushdown etc.</li>
                  <li>High Impact Campaigns</li>
                  <li>Standard Banners</li>
                  <li>Static Banners</li>
                </ul>
              </div>
              <div class="box creative">
                <h3>Design and Creative</h3>
                <ul>
                  <li>Idea /concept creation</li>
                  <li>Story Board Generation</li>
                  <li>Concept adaptation to digital format</li>
                  <li>Design and resizing</li>
                </ul>
              </div>
            </div>
            <div class="culunm center video-colum">
               <video  autoplay loop controls="false" src="video/video.mp4" class="video"></video>
               <img src="img/computer-2.png">
            </div>
            <div class="culunm right">
              <div class="box transcreation">
                <h3>Transcreation</h3>
                <ul>
                  <li>Idea /concept creation</li>
                  <li>Story Board Generation</li>
                  <li>Concept adaptation to digital format</li>
                  <li>Design and resizing</li>
                </ul>
              </div>
              <div class="box ad-server">
                <h3>Ad Server and QA/QC</h3>
                <ul>
                  <li>Banner upload</li>
                  <li>Campaign creation and set up</li>
                  <li>Campaign testing and approval</li>
                  <li>Tag generation</li>
                  <li>Campaign report generation</li>
                </ul>
              </div>
            </div>
          </div>
        </section>

        <footer class="section footer">
          <div class="owl-carousel owl-bg">
            <div>
              <img src="img/footer-bg.png" alt="" class="hidden-sm-down">
              <img src="img/footer-bg-xs.png" alt="" class="hidden-sm-up">
            </div>
            <div>
              <img src="img/img1.jpg" alt="" class="hidden-sm-down">
              <img src="img/img1-xs.png" alt="" class="hidden-sm-up">
            </div>
            <div>
              <img src="img/img3.jpg" alt="" class="hidden-sm-down">
              <img src="img/img3-xs.jpg" alt="" class="hidden-sm-up">
            </div>
          </div>
          <div class="clock hidden-md-down">
            <img src="img/hand.png" alt="">
            <div class="time">
              <p>
                <span id="mstHour"><?php echo $mstHour; ?></span>
                <span class="time-animation"> :</span>
                <span id="mstMinutes"><?php echo $mstMinutes; ?></span>
              </p>
              <p>MST</p>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-6 push-lg-6">
                  <div class="content">
                    <div class="box right card">
                      <div class="container">
                        <div class="row">
                          <div class="col-sm-12">
                            <h3>Contact Information</h3>
                          </div>
                          <!-- <div class="col-sm-6">
                            <p>You can find me here:</p>
                            <p>San José - Costa Rica</p>
                          </div> -->
                          <div class="col-sm-12">
                            <p class="phone">Phone:</p>
                            <p>In the U.S.A.: <a href="tel:+7733828683">(773) 382-8683</a></p>
                            <p>In Costa Rica: <a href="tel:+50625241692">(506) 2524-1692</a></p>
                          </div>
                          <div class="col-sm-12">
                            <p class="mail">Email:
                              <a href="mailto:hello@richmediaexperts.com" class="mail">hello@richmediaexperts.com</a>
                            </p>
                          </div>
                          <hr>

                        </div>
                        <div class="row">
                        <div class="col-sm-12">
                            <h3>Get In Touch</h3>
                            <p>Let us know what you need and we'll get back to you, guaranteed.</p>
                        </div>
                        <div class="col-sm-12">
                          <form method="POST" action="#Contact" id="form">
                                <input type="text" placeholder="Your Name" class="name" name="name">
                                <input type="mail" placeholder="Your Email Address" class="mail" name="email">
                                <textarea placeholder="Type in your message..." class="textarea" name="message"></textarea>
                                <?php
                                   if($sent){
                                      echo '<p class="succes-message">Your inquiry has been successfully sent. Thank You.</p>';
                                   }
                                ?>
                                <div class="captcha__container">
                                  <div class="g-recaptcha" data-sitekey="6LeKhiAUAAAAACbf9eumUo9q5JHuxONvP15tjxeh"></div>
                                </div>
                                <button class="send">Send</button>
                          </form>
                        </div>
                        </div>
                      </div>
                      <div>

                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-lg-6 pull-lg-6 world-times float-xs-left hidden-md-down">
                <div class="row">
                  <div class="time-container">
                    <div class="other-time">
                      <p>
                        <span id="pstHour"><?php echo $pstHour; ?></span>
                        <span class="time-animation"> :</span>
                        <span id="pstMinutes"><?php echo $pstMinutes; ?></span>
                      </p>
                      <p>PST</p>
                    </div>
                  </div>
                  <div class="time-container">
                    <div class="other-time">
                      <p>
                        <span id="cstHour"><?php echo $cstHour; ?></span>
                        <span class="time-animation"> :</span>
                        <span id="cstMinutes"><?php echo $cstMinutes; ?></span>
                      </p>
                      <p>CST</p>
                    </div>
                  </div>
                  <div class="time-container">
                    <div class="other-time">
                      <p>
                        <span id="estHour"><?php echo $estHour; ?></span>
                        <span class="time-animation"> :</span>
                        <span id="estMinutes"><?php echo $estMinutes; ?></span>
                      </p>
                      <p>EST</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="box left">
              <img src="img/map-contact.png">
            </div> -->

          </div>
        </footer>
      </div>
      <div class="hamburguer_menu">
        <a href="javascript:void(0)" class="button_menu open_menu">
          <span></span>
          <span></span>
          <span></span>
        </a>
        <nav>
          <ul>
             <li><a href="#Manifesto" class="close-menu">Manifesto</a></li>
             <li><a href="#About Us" class="close-menu">About Us</a></li>
             <li><a href="#Work" class="close-menu">Work</a></li>
             <li><a href="#Testimonials" class="close-menu">Testimonials</a></li>
             <li><a href="#Expertise" class="close-menu">Expertise</a></li>
             <li><a href="#Clients" class="close-menu">Clients</a></li>
             <li><a href="#Brands" class="close-menu">Brands</a></li>
             <li><a href="#Services" class="close-menu">Services</a></li>
             <li><a href="#Contact" class="close-menu">Contact</a></li>
          </ul>
        </nav>
      </div>
      <div class="nav_aside hidden-sm-down">
        <ul class="site__nav"></ul>
      </div>
      <a href="#0" class="cd-top">Top</a>
      <script src="js/jquery.2.2.1.min.js"></script>
      <script src=""></script>
      <script type="text/javascript" src="js/scrolloverflow.js"></script>
      <script src="js/jquery.fullpage.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenMax.min.js"></script>
      <script src="js/wow.min.js"></script><!-- Animation -->
      <script src="js/owl.carousel.min.js"></script><!-- Animation -->
      <script src="js/main.js?v=1.4"></script>
      <script src="js/plugins.js?v=0.7"></script>

      <script>
          $(document).ready(function(){
              setInterval(function(){
                  $.getJSON('<?php echo $hourURL ?>', function(response) {
                        $("#cstHour").text(response.CST.hour);
                        $("#cstMinutes").text(response.CST.minutes);

                        $("#pstHour").text(response.PST.hour);
                        $("#pstMinutes").text(response.PST.minutes);

                        $("#estHour").text(response.EST.hour);
                        $("#estMinutes").text(response.EST.minutes);

                        $("#mstHour").text(response.MST.hour);
                        $("#mstMinutes").text(response.MST.minutes);
                  });
              }, 1000);
          });
      </script>

   </body>
</html>
