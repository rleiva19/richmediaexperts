<?php
/*
 * STANDARD HOURS
 */

$datetimeCST  = new DateTime("now", new DateTimeZone('America/Chicago'));
$cstHour = $datetimeCST->format("H");
$cstMinutes = $datetimeCST->format("i");

$datetimePST  = new DateTime("now", new DateTimeZone('America/Los_Angeles'));
$pstHour = $datetimePST->format("H");
$pstMinutes = $datetimePST->format("i");

$datetimeEST  = new DateTime("now", new DateTimeZone('America/New_York'));
$estHour = $datetimeEST->format("H");
$estMinutes = $datetimeEST->format("i");

$datetimeMST  = new DateTime("now", new DateTimeZone('America/Denver'));
$mstHour = $datetimeMST->format("H");
$mstMinutes = $datetimeMST->format("i");

$hours = array(
    'CST' => array(
        'hour' => $cstHour,
        'minutes' => $cstMinutes
    ),
    'PST' => array(
        'hour' => $pstHour,
        'minutes' => $pstMinutes
    ),
    'EST' => array(
        'hour' => $estHour,
        'minutes' => $estMinutes
    ),
    'MST' => array(
        'hour' => $mstHour,
        'minutes' => $mstMinutes
    )
);

echo json_encode($hours);
